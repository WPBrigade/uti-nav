<div class="header">
   <div class="header-top-section">
      <div class="header_container">
         <a href="/" class="mobile-logo"><img src="/ResourcePackages/Foundation/assets/dist/images/logo.png"> <img src="/ResourcePackages/Foundation/assets/dist/images/mobile-logo.png"></a>
         <div class="links">
            <a href="#/" class="enroll"><span>Enroll Now</span></a>
            <a href="/request-info/" class="req-info"><span>Request Info</span></a>
            <a href="#/" class="chat"><span>Chat Now</span></a>
            <a href="tel:18008347308">Call <span class="mhide">1-800-834-7308</span></a>
            <span class="fa fa-bars menu-btn">Menu</span>
         </div> <!--  .links -->
      </div> <!--  .header_container -->
   </div>
   <div class="header-bottom-section">
      <div class="header_container">
            <div class="back-overlay"></div>
         <a href="/" class="logo"><img src="/ResourcePackages/Foundation/assets/dist/images/logo.png" alt=""></a>
         <div class="header-right-section">
            <form method="post" action="/search" class="searchform">
                  <div class="search-input">
                        <input type="search" placeholder="Search by topic">
                        <button type="submit"><i class="fa fa-search"></i></button>
                  </div>
                  <button type="button" class="close-form-btn"><i class="fa fa-times"></i></button>
            </form>
            <div class="separator-line"></div>
            <a href="#" class="searchicon"><i class="fa fa-search"></i><span>Search</span></a>
            <ul class="main_navigation">
               <li><a href="/" data-text="Home" style="display: none;"><span>Home</span></a></li>
               <li>
                  <a href="/schools" data-text="Schools"><span>Schools</span></a>
                  <ul class="sub_menu">
                     <li><a href="/schools/universal-technical-institute">Universal Technical Institute</a></li>
                     <li><a href="/schools/motorcycle-mechanics-institute">Motorcycle Mechanics Institute</a></li>
                     <li><a href="/schools/marine-mechanics-institute">Marine Mechanics Institute</a></li>
                     <li><a href="/schools/nascar-technical-institute">NASCAR Technical Institute</a></li>
                  </ul>
               </li>
               <li class="mega-dd ">
                  <a href="/programss" data-text="Programs"><span>Programs</span></a>
                  <div class="mega-menu sub_menu">
                     <div class="col-4">
                        <ul class="menu-section">
                           <li>
                              <a href="/programs/automotivee">Automotive</a>
                              <ul>
                                 <li><a href="/programs/automotive/specialized-trainingg">Specialized Training</a></li>
                                 <li><a href="/programs/automotive/specialized-training/bmw-step">BMW</a></li>
                                 <li><a href="/programs/automotive/specialized-training/ford-fact">Ford</a></li>
                                 <li><a href="/programs/automotive/specialized-training/gm-tech">General Motors</a></li>
                                 <li><a href="/programs/automotive/specialized-training/infiniti-itta">Infiniti</a></li>
                                 <li><a href="/programs/automotive/specialized-training/mercedes-benz-drive">Mercedes-Benz</a></li>
                                 <li><a href="/programs/automotive/specialized-training/mopar-tec">Mopar</a></li>
                                 <li><a href="/programs/automotive/specialized-training/nascar">NASCAR</a></li>
                                 <li><a href="/programs/automotive/specialized-training/nissan-natt">Nissan</a></li>
                                 <li><a href="/programs/automotive/specialized-training/porsche-ptap">Porsche</a></li>
                                 <li><a href="/programs/automotive/specialized-training/toyota-tpat">Toyota</a></li>
                                 <li><a href="/programs/automotive/specialized-training/volvo-safe">Volvo</a></li>
                              </ul>
                           </li>
                        </ul>
                     </div> <!--  .col-4 -->
                     <div class="col-4">
                        <ul class="menu-section">
                           <li>
                              <a href="/programs/diesel">Diesel</a>
                              <ul>
                                 <li><a href="/programs/diesel/specialized-training">Specialized Training</a></li>
                                 <li><a href="/programs/diesel/specialized-training/cummins-diesel-engines">Cummins Diesel Engines</a></li>
                                 <li><a href="/programs/diesel/specialized-training/cummins-power-generation">Cummins Power Generation</a></li>
                                 <li><a href="/programs/diesel/specialized-training/daimler-trucks-finish-first">Daimler Trucks</a></li>
                                 <li><a href="/programs/diesel/specialized-training/international-trucks-itep">International Trucks</a></li>
                                 <li><a href="/programs/diesel/specialized-training/peterbilt-pti">Peterbilt</a></li>
                              </ul>
                           </li>
                           <li>
                              <a href="/programs/marine">Marine</a>
                              <ul>
                                 <li><a href="/programs/marine/specialized-training">Specialized Training</a></li>
                                 <li><a href="/programs/marine/specialized-training/honda-marine">Honda Marine</a></li>
                                 <li><a href="/programs/marine/specialized-training/mercury-marine">Mercury Marine</a></li>
                                 <li><a href="/programs/marine/specialized-training/suzuki-marine">Suzuki Marine</a></li>
                                 <li><a href="/programs/marine/specialized-training/volvo-penta">Volvo Penta</a></li>
                                 <li><a href="/programs/marine/specialized-training/yamaha-marine">Yamaha Marine</a></li>
                              </ul>
                           </li>
                        </ul>
                     </div> <!--  .col-4 -->
                     <div class="col-4">
                        <ul class="menu-section">
                           <li><a href="/programs/motorcycle">Motorcycle</a>
                              <ul>
                                 <li><a href="/programs/motorcycle/specialized-training">Specialized Training</a></li>
                                 <li><a href="/programs/motorcycle/specialized-training/bmw-motorrad">BMW Motorrad</a></li>
                                 <li><a href="/programs/motorcycle/specialized-training/harley-davidson">Harley-Davidson</a></li>
                                 <li><a href="/programs/motorcycle/specialized-training/honda-hontech">Honda</a></li>
                                 <li><a href="/programs/motorcycle/specialized-training/kawasaki-k-tech">Kawasaki</a></li>
                                 <li><a href="/programs/motorcycle/specialized-training/suzuki-fast">Suzuki</a></li>
                                 <li><a href="/programs/motorcycle/specialized-training/yamaha-yamapro">Yamaha</a></li>
                              </ul>
                           </li>
                           <li><a href="/programs/cnc-machining">CNC Machining</a></li>
                           <li><a href="/programs/collision">Collision Repair</a></li>
                           <li><a href="/programs/welding">Welding</a></li>
                        </ul>
                     </div> <!--  .col-4 -->
                     <div class="col-4 catalog-menu">
                           <a href="/programs/catalogs"><img src="/ResourcePackages/Foundation/assets/dist/images/photo.jpg" alt=""></a>
                           <h3><a href="/programs/catalogs">Catalogs</a></h3>
                           <p>Download our catalogs and learn about programs, courses, tuition, fees, admissions and much more.</p>
                     </div> <!--  .col-4 -->
                  </div> <!--  .mega-menu -->
               </li>
               <li>
                  <a href="/why-utii" data-text="Why UTI"><span>Why UTI</span></a>
                  <div class="two-collumn sub_menu">
                     <div class="menu-section-wrapper">
                        <ul class="menu-section">
                           <li><a href="/why-uti/value-investment">Value Of Your Investment</a></li>
                           <li>
                              <a href="/why-uti/industry-relationships">Industry Relationships</a>
                              <ul>
                                 <li><a href="/why-uti/industry-relationships/original-equipment-manufacturers">Original Equipment Manufacturers</a></li>
                                 <li><a href="/why-uti/industry-relationships/employers">Employers</a></li>
                                 <li><a href="/why-uti/industry-relationships/aftermarket">Aftermarket</a></li>
                              </ul>
                           </li>
                           <li><a href="/why-uti/jobs-career-opportunities">Jobs &amp; Career Opportunities</a></li>
                           <li><a href="/why-uti/equipment-technology">Equipment &amp; Technology</a></li>
                           <li><a href="/why-uti/employer-stories">Employer Stories</a></li>
                           <li><a href="/why-uti/graduate-stories">Graduate Stories</a></li>
                           <li><a href="/why-uti/instructor-stories">Instructor Stories</a></li>
                        </ul>
                     </div>
                     <div class="catalog-menu">
                        <a href="/why-uti/graduate-stories"><img src="/ResourcePackages/Foundation/assets/dist/images/mog.jpg" alt=""></a>
                        <h3><a href="/why-uti/graduate-stories">Meet Our Graduates</a></h3>
                        <p>Find out what some of our graduates are doing today in pursuing their successful careers.</p>
                     </div> <!--  .catalog-menu -->
                  </div>
               </li>
               <li>
                  <a href="/support-services" data-text="Support Services"><span>Support Services</span></a>
                  <div class="two-collumn sub_menu">
                     <div class="menu-section-wrapper">
                        <ul class="menu-section">
                           <li><a href="/support-services/employment-assistance">Employment Assistance</a></li>
                           <li><a href="/support-services/housing-assistance">Housing Assistance</a></li>
                           <li><a href="/support-services/military-veteran-services">Military &amp; Veterans Services</a></li>
                           <li><a href="/support-services/disability-services">Disability Services</a></li>
                           <li><a href="/support-services/voter-registration">Voter Registration</a></li>
                        </ul>
                     </div>
                     <div class="catalog-menu">
                        <a href="/support-services/military-veteran-services"><img src="/ResourcePackages/Foundation/assets/dist/images/mscholar.jpg" alt=""></a>
                        <h3><a href="/support-services/military-veteran-services">Military Scholarships</a></h3>
                        <p>Learn more about how we assist our veterans from VA funding to exclusive scholarships.</p>
                     </div> <!--  .catalog-menu -->
                  </div>
               </li>
               <li class="location-dd ">
                  <a href="/locations" data-text="Locations"><span>Locations</span></a>
                  <div class="location-menu sub_menu">
                     <div class="three-collumn">
                        <div class="menu-section-wrapper">
                           <ul class="menu-section">
                              <li>
                                 <a href="/locations/arizona">Arizona</a>
                                 <ul>
                                    <li><a href="/locations/arizona/avondale">Avondale, AZ</a></li>
                                    <li><a href="/locations/arizona/phoenix">Phoenix, AZ</a></li>
                                 </ul>
                              </li>
                              <li>
                                 <a href="/locations/california">California</a>
                                 <ul>
                                    <li><a href="/locations/california/long-beach">Long Beach, CA</a></li>
                                    <li><a href="/locations/california/rancho-cucamonga">Rancho Cucamonga, CA</a></li>
                                    <li><a href="/locations/california/sacramento">Sacramento, CA</a></li>
                                 </ul>
                              </li>
                              <li>
                                 <a href="/locations/florida">Florida</a>
                                 <ul>
                                    <li><a href="/locations/florida/orlando-uti">Orlando, FL  |  UTI</a></li>
                                    <li><a href="/locations/florida/orlando-mmi">Orlando, FL  |  MMI</a></li>
                                    <li><a href="/locations/florida/orlando-mmi-marine">Orlando, FL  |  Marine</a></li>
                                 </ul>
                              </li>
                              <li>
                                 <a href="/locations/illinois">Illinois</a>
                                 <ul>
                                    <li><a href="/locations/illinois/lisle">Lisle, IL</a></li>
                                 </ul>
                              </li>
                           </ul>
                        </div>
                        <div class="menu-section-wrapper">
                           <ul class="menu-section">
                              <li>
                                 <a href="/locations/massachusetts">Massachusetts</a>
                                 <ul>
                                    <li><a href="/locations/massachusetts/norwood">Norwood, MA</a></li>
                                 </ul>
                              </li>
                              <li>
                                 <a href="/locations/new-jersey">New Jersey</a>
                                 <ul>
                                    <li><a href="/locations/new-jersey/bloomfield">Bloomfield, NJ</a></li>
                                 </ul>
                              </li>
                              <li>
                                 <a href="/locations/north-carolina">North Carolina</a>
                                 <ul>
                                    <li><a href="/locations/north-carolina/mooresville">Mooresville, NC</a></li>
                                 </ul>
                              </li>
                              <li>
                                 <a href="/locations/pennsylvania">Pennsylvania</a>
                                 <ul>
                                    <li><a href="/locations/pennsylvania/exton">Exton, PA</a></li>
                                 </ul>
                              </li>
                              <li>
                                 <a href="/locations/texas">Texas</a>
                                 <ul>
                                    <li><a href="/locations/texas/dallas-fort-worth">Dallas, TX</a></li>
                                    <li><a href="/locations/texas/houston">Houston, TX</a></li>
                                 </ul>
                              </li>
                           </ul>
                        </div>
                        <div class="catalog-menu">
                           <a href="/locations/arizona/avondale"><img src="/ResourcePackages/Foundation/assets/dist/images/avondale.jpg" alt=""></a>
                           <h3><a href="/locations/arizona/avondale">Avondale, AZ  |  UTI</a></h3>
                           <p>State-of-the-art, 248,000 sq.ft. Avondale campus will provide you with hands-on experience with everything from undercar maintenance to advanced diagnosis. Learn more here.</p>
                        </div> <!--  .catalog-menu -->
                     </div>
                  </div> <!--  .location-menu -->
               </li>
               <li>
                  <a href="/admissionss" data-text="Admissions"><span>Admissions</span></a>
                  <div class="sm-menu sub_menu">
                     <ul class="menu-section">
                        <li><a href="/admissions/tuition">Tuition</a></li>
                        <li>
                           <a href="/admissions/admissions-requirements">Requirements</a>
                           <ul>
                              <li><a href="/admissions/admissions-requirements/international-students">International Students</a></li>
                              <li><a href="/admissions/admissions-requirements/felony-appeals">Felony Appeals</a></li>
                              <li><a href="/admissions/admissions-requirements/ged-assistance">GED Assistance</a></li>
                           </ul>
                        </li>
                        <li><a href="/admissions/refer-a-friend">Refer a Friend</a></li>
                     </ul>
                     <div class="catalog-menu">
                        <a href="/admissions/admissions-requirements/ged-assistance"><img src="/ResourcePackages/Foundation/assets/dist/images/ged.jpg" alt=""></a>
                        <h3><a href="/admissions/admissions-requirements/ged-assistance">GED Resources</a></h3>
                        <p>UTI welcomes General Education Diploma students. Find out more in our resources.</p>
                     </div> <!--  .catalog-menu -->
                  </div> <!--  .sm-menu -->
               </li>
               <li>
                  <a href="/financial-aidd" data-text="Financial Aid"><span>Financial Aid</span></a>
                  <ul class="sub_menu">
                     <li><a href="/financial-aid/scholarships-grants">Scholarships &amp; Grants</a></li>
                     <li><a href="/financial-aid/hiring-incentives">Hiring Incentives</a></li>
                  </ul>
               </li>
            </ul>
         </div> <!--  .header-right-section -->
      </div> <!--  .header_container -->
   </div>
</div>
