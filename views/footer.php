  <div id="Contentplaceholder1_TCC38B385002_Col00" class="sf_colsIn container" data-sf-element="Container" data-placeholder-label="Container"></div>
         <div id="Contentplaceholder1_TCC38B385003_Col00" class="sf_colsIn container" data-sf-element="Container" data-placeholder-label="Container"></div>
         <div class="main-footer widget  to-bottom-right-alt background-gradient-blue" >
            <div class="row align-center-middle hide-for-large">
               <div class="small-5 medium-3 columns">
                  <a class="logo" href="/schools/universal-technical-institute">
                  <img data-src="https://utiedu.azureedge.net/images/default-source/global/logos/footer-logos-only/uti_rev_125x60.png?sfvrsn=dfdeb775_6" alt="UTI_Rev_125x60" />
                  </a>
               </div>
               <div class="small-5 medium-3 columns">
                  <a class="logo" href="/schools/motorcycle-mechanics-institute">
                  <img data-src="https://utiedu.azureedge.net/images/default-source/global/logos/footer-logos-only/mmi_moto_rev_125x60.png?sfvrsn=7a4612b6_10" alt="MMI_Moto_Rev_125x60" />
                  </a>
               </div>
               <div class="small-5 medium-3 columns">
                  <a class="logo" href="/schools/marine-mechanics-institute">
                  <img data-src="https://utiedu.azureedge.net/images/default-source/global/logos/footer-logos-only/mmi_marine_rev_125x60.png?sfvrsn=866b017c_8" alt="MMI_Marine_Rev_125x60" />
                  </a>
               </div>
               <div class="small-5 medium-3 columns">
                  <a class="logo" href="/schools/nascar-technical-institute">
                  <img data-src="https://utiedu.azureedge.net/images/default-source/global/logos/footer-logos-only/nascar_tech_rev_125x60.png?sfvrsn=5cbc10f9_6" alt="NASCAR_Tech_Rev_125x60" />
                  </a>
               </div>
            </div>
            <div class="row align-justify">
               <div class="small-6 medium-4 large-2 columns footer-column">
                  <strong>Getting Started</strong>
                  <ul>
                     <li>
                        <a href="/schools">Schools</a>
                     </li>
                     <li>
                        <a href="/programs">Core Programs</a>
                     </li>
                     <li>
                        <a href="/why-uti/industry-partnerships/original-equipment-manufacturers">Specialized Training</a>
                     </li>
                     <li>
                        <a href="/locations/">Locations</a>
                     </li>
                     <li>
                        <a href="/admissions/tuition/">Tuition</a>
                     </li>
                     <li>
                        <a href="/why-uti">Why UTI</a>
                     </li>
                  </ul>
               </div>
               <div class="small-6 medium-4 large-2 columns footer-column">
                  <strong>Becoming a Student</strong>
                  <ul>
                     <li>
                        <a href="/admissions">Admissions</a>
                     </li>
                     <li>
                        <a href="/programs/catalogs">Catalogs</a>
                     </li>
                     <li>
                        <a href="/financial-aid">Financial Aid</a>
                     </li>
                     <li>
                        <a href="/financial-aid/scholarships-grants">Scholarships &amp; Grants</a>
                     </li>
                     <li>
                        <a href="/support-services/military-veteran-services/">Military Services</a>
                     </li>
                     <li>
                        <a href="/support-services/disability-services">Disability Services</a>
                     </li>
                  </ul>
               </div>
               <div class="small-6 medium-4 large-2 columns footer-column">
                  <strong>About Us</strong>
                  <ul>
                     <li>
                        <a href="/about#wwcnyl10iu">Accreditation</a>
                     </li>
                     <li>
                        <a href="/about/">Our Mission &amp; Values</a>
                     </li>
                     <li>
                        <a href="/careers/">Careers</a>
                     </li>
                     <li>
                        <a href="https://uti.investorroom.com/">Investor Relations</a>
                     </li>
                     <li>
                        <a href="https://uti.mediaroom.com/">Newsroom</a>
                     </li>
                  </ul>
               </div>
               <div class="small-6 medium-4 large-2 columns footer-column">
                  <strong>Resources</strong>
                  <ul>
                     <li>
                        <a href="/support-services/housing-assistance">Housing Assistance</a>
                     </li>
                     <li>
                        <a href="/why-uti/jobs-career-opportunities">Job Opportunities</a>
                     </li>
                     <li>
                        <a href="">Parent Information</a>
                     </li>
                     <li>
                        <a href="/support-services/voter-registration">Voter Registration</a>
                     </li>
                     <li>
                        <a href="/1098-t">1098-T</a>
                     </li>
                  </ul>
               </div>
               <div class="small-6 medium-4 large-2 columns footer-column">
                  <strong>Contact</strong>
                  <ul>
                     <li>
                        <a href="tel:8008347308">800-834-7308</a>
                     </li>
                     <li>
                        <a href="/request-info">Request Info</a>
                     </li>
                     <li>
                        <a href="/contact-us">Contact Us</a>
                     </li>
                     <li>
                        <a href="/locations/campus-tours">Campus Tours</a>
                     </li>
                  </ul>
               </div>
               <div class="small-12 medium-4 large-2 columns footer-column show-for-large">
                  <div class="row collapse">
                     <div class="small-6 medium-12 columns">
                        <a class="footer-logo" href="/schools/universal-technical-institute">
                        <img data-src="https://utiedu.azureedge.net/images/default-source/global/logos/footer-logos-only/uti_rev_125x60.png?sfvrsn=dfdeb775_6" alt="UTI_Rev_125x60" />
                        </a>
                     </div>
                     <div class="small-6 medium-12 columns">
                        <a class="footer-logo" href="/schools/motorcycle-mechanics-institute">
                        <img data-src="https://utiedu.azureedge.net/images/default-source/global/logos/footer-logos-only/mmi_moto_rev_125x60.png?sfvrsn=7a4612b6_10" alt="MMI_Moto_Rev_125x60" />
                        </a>
                     </div>
                     <div class="small-6 medium-12 columns">
                        <a class="footer-logo" href="/schools/marine-mechanics-institute">
                        <img data-src="https://utiedu.azureedge.net/images/default-source/global/logos/footer-logos-only/mmi_marine_rev_125x60.png?sfvrsn=866b017c_8" alt="MMI_Marine_Rev_125x60" />
                        </a>
                     </div>
                     <div class="small-6 medium-12 columns">
                        <a class="footer-logo" href="/schools/nascar-technical-institute">
                        <img data-src="https://utiedu.azureedge.net/images/default-source/global/logos/footer-logos-only/nascar_tech_rev_125x60.png?sfvrsn=5cbc10f9_6" alt="NASCAR_Tech_Rev_125x60" />
                        </a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row column" aria-hidden="true">
               <div class="divider" aria-hidden="true"></div>
            </div>
            <div class="main-footer--disclaimer row column" aria-label="disclaimer">
               <p>
               <p>1) UTI cannot guarantee employment or salary.</p>
               <p>2) For important information about the educational debt, earnings and completion rates of students who attended this program, visit our website at <a href="/disclosures"> www.uti.edu/disclosures.</a></p>
               <p>3) Approximately 8,600 of the 9,200 UTI graduates in 2016 were available for employment. At the time of reporting, approximately 7,400 were employed within one year of their graduation date, for a total of 86%. This rate excludes graduates not available for employment because of continuing education, military service, health, incarceration, death or international student status. The rate includes graduates who completed Manufacturer-Specific Advanced Training programs and those employed in positions that were obtained before or during their UTI education, where the primary job duties after graduation align with the educational and training objectives of the program. UTI cannot guarantee employment or salary.</p>
               <p>4) UTI&rsquo;s Automotive Technology program is 51-weeks.  Core program length varies by subject.  For example, Diesel &amp; Industrial Technology is 45-weeks and Automotive/Diesel Technology is 75-weeks.</p>
               </p>
            </div>
            <div class="main-footer--bottom row align-center-middle">
               <div class="small-12 columns">
                  <span aria-label="copyright" class="copyright">&copy; 2017 Universal Technical Institute, Inc.  All rights reserved.</span>
                  <ul class="menu">
                     <li>
                        <a href="/privacy-policy">Privacy Policy</a>
                     </li>
                     <li>
                        <a href="/legal-notice">Legal Notice</a>
                     </li>
                     <li>
                        <a href="/accessibility">Accessibility</a>
                     </li>
                     <li>
                        <a href="/disclosures">Disclosures</a>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script> 
      <script src="/ResourcePackages/Foundation/assets/dist/js/vendor.min.js"></script> 
      <script src="/ResourcePackages/Foundation/assets/dist/js/app.min.js"></script> 
      <script type="text/javascript">
         (function($) {
           $(document).ready(function() {
             $('.info-overlay h3').each(function() {
               $(this).text($(this).text().replace("Ε", "E"));
               $(this).text($(this).text().replace("Μ", "M"));
               $(this).text($(this).text().replace("Χ", "X"));
             });
           });
         })(jQuery);
      </script>
   </body>
</html>