<!DOCTYPE html> 
<html lang="en">
   <head>
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta charset="utf-8" />
      <title>
         Home
      </title>
      <link type="text/css" rel="stylesheet" href="/ResourcePackages/Foundation/assets/dist/css/app.min.css" />
      <link rel="stylesheet" type="text/css" href="//cloud.typography.com/7416432/788802/css/fonts.css" />
      <!-- Google Tag Manager --> <script>
         (function (w, d, s, l, i) {
         w[l] = w[l] || []; w[l].push({
         'gtm.start':
         new Date().getTime(), event: 'gtm.js'
         }); var f = d.getElementsByTagName(s)[0],
         j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
         'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
         })(window, document, 'script', 'dataLayer', 'GTM-5BXBPH');
      </script> <!-- End Google Tag Manager -->  <script type="text/javascript">var sf_appPath='/';</script><script type='application/ld+json'>
         {
         "@context": "http://www.schema.org",
         "@type": "schema:EducationalOrganization",
         "@id": "https://www.uti.edu",
             "name": "Universal Technical Institute, Inc.",
             "alternateName": "UTI",
             "url": "https://www.uti.edu",
             "description": "Headquartered in Scottsdale, Arizona, Universal Technical Institute, Inc. (NYSE: UTI) is the leading provider of post-secondary education for students seeking careers  as automotive, diesel, medium/heavy equipment, collision repair, motorsports, motorcycle and marine technicians; welders; and CNC machining technicians.  With more than 200,000 graduates in its 52-year history, UTI offers undergraduate degree and diploma programs at 12 campus locations across the United States, as well as manufacturer-specific training programs at dedicated training centers.  Through its campus-based school system, UTI provides specialized post-secondary education programs under the banner of several well-known brands, including Universal Technical Institute (UTI), Motorcycle Mechanics Institute and Marine Mechanics Institute (MMI) and NASCAR Technical Institute (NASCAR Tech).",
             "taxID": "860226984",
             "address": {
                 "@type": "PostalAddress",
                 "addressLocality": "Scottsdale",
                 "addressRegion": "AZ",
                 "postalCode": "85254",
                 "streetAddress": "16220 North Scottsdale Road, Suite 100"},
             "logo": "https://www.uti.edu/images/default-source/global/logos/logo_uti_nav.png",
             "telephone": "+1-800-859-7249"
         }
      </script><script type="text/javascript">
         /*window.onload = function() {
           var h = document.getElementsByTagName('header')[0];
           h.className += " home-hero";
         }*/
      </script>
      <meta name="Generator" content="Sitefinity 10.0.6400.0 PU" />
      <link rel="canonical" href="https://www.uti.edu" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <script type="text/javascript">
         (function() {var _rdDeviceWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;var _rdDeviceHeight = (window.innerHeight > 0) ? window.innerHeight : screen.height;var _rdOrientation = (window.width > window.height) ? 'landscape' : 'portrait';})();
      </script>
      <meta name="description" content="UTI is a leading provider of technical training for students seeking entry-level careers as technicians in the transportation industry." />
      <meta name="keywords" content="Universal Technical Institute, UTI, Mechanic School, Automotive school " />
   </head>
   <body role="main">
      <!-- Google Tag Manager (noscript) --> 
      <noscript> <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5BXBPH"
         height="0" width="0" style="display:none;visibility:hidden" title="google tag manager"></iframe> </noscript>
      <!-- End Google Tag Manager (noscript) -->  
      <div class="sfPublicWrapper" id="PublicWrapper">