<?php include('../../views/header.php'); ?>
<?php include('../../views/navigation.php'); ?>
<div id="Contentplaceholder1_TCC38B385001_Col00" class="sf_colsIn container" data-sf-element="Container" data-placeholder-label="Container">
   <header class="widget hero  hero--dark-image background-image  text-center " id="hero79sy9v" style="background-image: url('https://utiedu.azureedge.net/images/default-source/global/global-heros/global_motor_hero.jpg?sfvrsn=a493c99b_4');">
      <div class="overlay" aria-hidden="true"></div>
      <div class="row align-center-middle">
         <div class="small-11 medium-10 large-9 columns">
            <div class="hero-content">
               <h1>
                  ADMISSIONS REQUIREMENTS
               </h1>
               <div class="button-area">
               </div>
            </div>
         </div>
      </div>
      <a href="#" class="scroll-to-next"><i class="fa fa-chevron-down" aria-hidden="true"></i><span class="show-for-sr">Scroll to next section</span></a>
   </header>
</div>
<div id="Contentplaceholder1_TCC38B385002_Col00" class="sf_colsIn container" data-sf-element="Container" data-placeholder-label="Container">
   <section class=" description widget  background-blue-dark" id="descr3stbc8" aria-labelledby="descr3stbc8-header">
      <div class="description-inner row column">
         <div class="row column">
            <div class="widget-header text-center" id="descr3stbc8-header">
               <h2>ELIGIBILITY REQUIREMENTS</h2>
               <p>We want to make sure UTI is the right fit for your goals. Once you decide this is the place to start training, there are some requirements to cover to make sure you are eligible. Review those requirements below then speak with an Admissions Representative to answer your questions.</p>
            </div>
         </div>
         <div class="description-content">
            <div class="no-media text-center">
               <div class="cta-container">
                  <a class="button ttb" href="/request-info">
                  LEARN MORE
                  <i class="fa fa-chevron-right"></i>
                  </a>
               </div>
            </div>
         </div>
      </div>
   </section>
</div>
<div id="Contentplaceholder1_TCC38B385003_Col00" class="sf_colsIn container" data-sf-element="Container" data-placeholder-label="Container">
   <div id="wwcnzfev47" class="description widget  background-white ">
      <div class="description-inner row column">
         <div class="description-content">
            <h2><strong></strong></h2>
            <h2 style="text-align:center;">ADMISSION PROCEDURES AND ENTRANCE REQUIREMENTS</h2>
            <p>The school determines, with reasonable certainty and in advance of class start date, that the applicant has proper&nbsp;qualifications to complete training. Each Enrollment&nbsp;Agreement and other pertinent information submitted by the&nbsp;applicant will be reviewed prior to starting classes.</p>
            <p>All students, upon acceptance of an Enrollment Agreement,&nbsp;are conditionally admitted to UTI. The conditional status remains until the student's documentation is judged acceptable. Allowing adequate lead time (ideally 30 days&nbsp;minimum) for both evaluation of the document(s) submitted&nbsp;and an alert regarding any deficiency prior to any planned&nbsp;relocation to attend school is highly advised.</p>
            <h3>ENTRANCE REQUIREMENTS</h3>
            <p>To comply with the school's entrance requirements prior to starting or re-enrolling, students must supply and UTI must&nbsp;accept one of the following documents:</p>
            <ul style="margin-top:0in;" type="disc">
               <li>Standards-based high school diploma recognized by the&nbsp;student's state (documented      with a copy of the diploma,&nbsp;a transcript provided by the high school      or a DD Form 214&nbsp;showing verification of high school graduation).      Diplomas&nbsp;and transcripts are evaluated upon receipt. UTI      evaluates&nbsp;diplomas for validity and reserves the right not to      accept&nbsp;those deemed invalid. Note: Students who are residents&nbsp;of      the state of Tennessee and attending MMI-Orlando or&nbsp;UTI-Houston, and      all students at NASCAR Tech are required&nbsp;to submit copies of their      official high school transcripts&nbsp;rather than copies of their high      school diplomas to satisfy&nbsp;admissions requirements; or</li>
            </ul>
            <ul style="margin-top:0in;" type="disc">
               <li>State-issued GED or state-authorized equivalent exam.&nbsp;Students who are residents      of the state of Tennessee and&nbsp;attending MMI-Orlando or UTI-Houston,      and all students at&nbsp;NASCAR Tech are required to submit copies of      their official&nbsp;GED transcripts rather than copies of their      certification to&nbsp;satisfy admissions requirements; or</li>
            </ul>
            <ul style="margin-top:0in;" type="disc">
               <li>Evidence of having previously attended a Title IV-eligible&nbsp;program at a      postsecondary institution under the Ability to&nbsp;Benefit (ATB)      provision prior to July 1, 2012 (documented&nbsp;with a copy of the      official ATB test scores and transcript); or</li>
            </ul>
            <ul style="margin-top:0in;" type="disc">
               <li>Successful completion of a degree program at the&nbsp;postsecondary level (associate      degree and beyond proven by submission of an official transcript from the      college); or</li>
            </ul>
            <ul style="margin-top:0in;" type="disc">
               <li>Successful completion of an officially recognized home&nbsp;schooling program. The      home schooling documentation&nbsp;required by UTI for review varies based      on state requirements.&nbsp;If home schooling was completed in a state      that issues&nbsp;a secondary school completion credential, a copy of      the&nbsp;credential is required. If the state has no such      requirements,&nbsp;additional documentation — including a transcript      showing&nbsp;all courses, grades and graduation date, and a notarized      statement — must be submitted for review. The campus&nbsp;Registrar or      designee will review home school documents and&nbsp;notify the applicant      if further documentation is required.</li>
            </ul>
            <h3>SPECIAL NOTES ON CERTIFICATES OF COMPLETION&nbsp;AND SPECIAL EDUCATION DIPLOMAS</h3>
            <p>Students possessing a certificate of high school completion&nbsp;(i.e., completed all courses but did not pass all state&nbsp;standards-based requirements such as testing), or high school&nbsp;diploma or transcript indicating the student earned a special&nbsp;education diploma that did not meet all of the state standards-based requirements must provide a copy of a state-issued GED or state-authorized equivalent exam prior to starting class.</p>
            <h3>SPECIAL NOTES ON UTI RE-ENROLLEES&nbsp;PREVIOUSLY ACCEPTED UNDER ABILITY TO&nbsp;BENEFIT (ATB) PROVISION</h3>
            <ul style="margin-top:0in;" type="disc">
               <li>Students whose prior ATB enrollment resulted in graduation&nbsp;can re-enroll.</li>
            </ul>
            <ul style="margin-top:0in;" type="disc">
               <li>Students whose prior ATB enrollment resulted in&nbsp;withdrawal can re-enroll      provided they meet all other&nbsp;re-enrollment criteria.</li>
            </ul>
            <ul style="margin-top:0in;" type="disc">
               <li>Former ATB students who may have been accepted to begin&nbsp;coursework but never      attended and whose enrollment was therefore canceled must provide a copy      of a state-issued GED&nbsp;or state-authorized equivalent exam prior to      starting class.</li>
            </ul>
            <h3>SPECIAL NOTES ON TRANSFERS FROM OTHER&nbsp;INSTITUTIONS WHO PREVIOUSLY ATTENDED&nbsp;UNDER ATB PROVISION</h3>
            <ul style="margin-top:0in;" type="disc">
               <li>Students whose prior ATB enrollment can be validated with&nbsp;acceptable      documentation demonstrating they had attended a Title IV-eligible program      of study prior to&nbsp;July 1, 2012 may be accepted for enrollment at      UTI.</li>
            </ul>
            <ul style="margin-top:0in;" type="disc">
               <li>A transcript from the previous institution is required to&nbsp;verify      attendance.</li>
            </ul>
            <p>These students may submit a copy of official ATB scores from the&nbsp;previous school or take a new ATB test for UTI admission.</p>
            <h3>FOREIGN EDUCATION</h3>
            <p>Foreign education documents from outside the United States&nbsp;or its territories that cannot immediately be confirmed as valid&nbsp;proof of high school completion by a college official must be submitted for assessment with a third-party evaluation agency&nbsp;at the prospective student's expense.</p>
            <h3>ADDITIONAL ADMISSION PROCEDURES/ENTRANCE REQUIREMENTS</h3>
            <p>In addition to the general Admissions Procedures/ Entrance&nbsp;Requirements provided above, applicants for Cummins Engines&nbsp;and Cummins Power Generation programs must satisfy all of the&nbsp;following requirements:</p>
            <ol style="margin-top:0in;" type="1">
               <li>Be a      UTI or NASCAR Tech graduate, or a UTI or&nbsp;NASCAR Tech student who has      no more than 10&nbsp;courses remaining in his or her current UTI or&nbsp;NASCAR      Tech program;</li>
               <li>Have a valid motor vehicle record (MVR);</li>
               <li>Have no current MVR convictions of driving under the&nbsp;influence of alcohol      and/or drugs; and</li>
               <li>Complete a personal interview with the Advanced Training&nbsp;Manager. (For Cummins      Engines and Cummins Power Generation, applicants instead will meet with      the campus&nbsp;Student Development Advisor.) Only an applicant who      meets&nbsp;requirements 1 through 3 will be eligible for an interview.</li>
            </ol>
            <p>Satisfying all admissions requirements does not guarantee&nbsp;admission to the program. If the number of qualified applicants&nbsp;exceeds the number of available spots, the institution will select&nbsp;the most qualified applicants based upon GPA, attendance and&nbsp;interview responses.</p>
            <h3>NON-MATRICULATING STUDENTS</h3>
            <p>A GED or state-authorized equivalent exam is not&nbsp;required for students who do not enroll in a full, approved program. If a student chooses to later enroll in a full program, all&nbsp;admissions requirements listed above must be satisfied.</p>
         </div>
      </div>
   </div>
   <section style="background-image: url('https://utiedu.azureedge.net/images/default-source/global/background-assets/metal_bg.jpg?sfvrsn=c4a98b1d_4');" class=" description widget  has-screws background-image " id="descr6bqdik" aria-labelledby="descr6bqdik-header">
      <div class="description-inner row column">
         <div class="row column">
            <div class="widget-header text-center" id="descr6bqdik-header">
               <h2>INTERESTED IN ATTENDING UNIVERSAL TECHNICAL INSTITUTE?</h2>
               <p>Reach out to our Admissions team today.</p>
            </div>
         </div>
         <div class="description-content">
            <div class="no-media text-center">
               <div class="cta-container">
                  <a class="button ttb" href="/request-info">
                  Request Info
                  <i class="fa fa-chevron-right"></i>
                  </a>
               </div>
            </div>
         </div>
      </div>
      <div class="screws-top" aria-hidden="true" aria-label="top background screws"></div>
      <div class="screws-bottom" aria-hidden="true" aria-label="bottom background screws"></div>
   </section>
</div>
<?php include('../../views/footer.php'); ?>

