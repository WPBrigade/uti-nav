<?php include('../../views/header.php'); ?>
<?php include('../../views/navigation.php'); ?>
<div id="Contentplaceholder1_TCC38B385001_Col00" class="sf_colsIn container" data-sf-element="Container" data-placeholder-label="Container">
   <header class="widget hero  hero--dark-image background-image  text-center " id="heroskc1pf" style="background-image: url('https://utiedu.azureedge.net/images/default-source/global/global-heros/graduate_profile_hero/graduates_hero.jpg?sfvrsn=5865055a_4');">
      <div class="overlay" aria-hidden="true"></div>
      <div class="row align-center-middle">
         <div class="small-11 medium-10 large-9 columns">
            <div class="hero-content">
               <h1>
                  MEET OUR GRADUATES
               </h1>
               <div class="button-area">
               </div>
            </div>
         </div>
      </div>
      <a href="#" class="scroll-to-next"><i class="fa fa-chevron-down" aria-hidden="true"></i><span class="show-for-sr">Scroll to next section</span></a>
   </header>
   <div style="background-image: url('https://utiedu.azureedge.net/images/default-source/global/background-assets/background-callout-metal.jpg?sfvrsn=94b818dc_0');" class="widget background-image  background-white sf-border" aria-hidden="true"></div>
   <aside id="cllt36m0w7" class="widget  to-bottom-right-alt background-gradient-red callout-container">
      <div class="row column">
         <p>At UTI, we help students prepare for a career in the automotive, diesel, collision repair, motorcycle, marine, CNC machining and welding industries. Find out what some of our graduates are doing today.</p>
      </div>
   </aside>
   <div style="background-image: url('https://utiedu.azureedge.net/images/default-source/global/background-assets/background-callout-metal.jpg?sfvrsn=94b818dc_0');" class="widget background-image  background-white sf-border" aria-hidden="true"></div>
</div>
<div id="Contentplaceholder1_TCC38B385002_Col00" class="sf_colsIn container" data-sf-element="Container" data-placeholder-label="Container"></div>
<div id="Contentplaceholder1_TCC38B385003_Col00" class="sf_colsIn container" data-sf-element="Container" data-placeholder-label="Container">
   <div id="rscsw126ib" class="widget related-story-cards">
      <div class="row small-up-1 medium-up-2 large-up-3 filter-cards--results hide-stories align-left">
         <article class="column">
            <div class="filter-card">
               <div class="sc-video-thumb" style="background-image:url(https://img.youtube.com/vi/YzCXketLBgs/0.jpg)">
                  <a href="#" aria-haspopup="true" class="launch-sc-video" data-open="story-video-YzCXketLBgs" aria-controls="story-video-YzCXketLBgs" tabindex="0">
                  <span class="show-for-sr">Play the video</span>
                  <i class="fa fa-play" aria-hidden="true"></i>
                  </a>
               </div>
               <div class="filter-card--content">
                  <h3>AUSTIN ADAIR</h3>
                  <p>Graduate, UTI Exton</p>
                  <p>Technician, Faulkner Volkswagen</p>
               </div>
            </div>
         </article>
         <article class="column">
            <div class="filter-card">
               <div class="sc-video-thumb" style="background-image:url(https://img.youtube.com/vi/61vCLg0JPqk/0.jpg)">
                  <a href="#" aria-haspopup="true" class="launch-sc-video" data-open="story-video-61vCLg0JPqk" aria-controls="story-video-61vCLg0JPqk" tabindex="0">
                  <span class="show-for-sr">Play the video</span>
                  <i class="fa fa-play" aria-hidden="true"></i>
                  </a>
               </div>
               <div class="filter-card--content">
                  <h3>EJ MALTEZO</h3>
                  <p>Graduate, UTI Avondale</p>
                  <p>Technician, Toyota of Surprise</p>
               </div>
            </div>
         </article>
         <article class="column">
            <div class="filter-card">
               <div class="sc-video-thumb" style="background-image:url(https://img.youtube.com/vi/XxPHH01xh-U/0.jpg)">
                  <a href="#" aria-haspopup="true" class="launch-sc-video" data-open="story-video-XxPHH01xh-U" aria-controls="story-video-XxPHH01xh-U" tabindex="0">
                  <span class="show-for-sr">Play the video</span>
                  <i class="fa fa-play" aria-hidden="true"></i>
                  </a>
               </div>
               <div class="filter-card--content">
                  <h3>DAN WEHRY</h3>
                  <p>Graduate, UTI Norwood</p>
                  <p>Mercedes-Benz of Burlington</p>
               </div>
            </div>
         </article>
         <article class="column">
            <div class="filter-card">
               <div class="sc-video-thumb" style="background-image:url(https://img.youtube.com/vi/DXlPZnCHh3g/0.jpg)">
                  <a href="#" aria-haspopup="true" class="launch-sc-video" data-open="story-video-DXlPZnCHh3g" aria-controls="story-video-DXlPZnCHh3g" tabindex="0">
                  <span class="show-for-sr">Play the video</span>
                  <i class="fa fa-play" aria-hidden="true"></i>
                  </a>
               </div>
               <div class="filter-card--content">
                  <h3>HARRY BOOKHOOP JR.</h3>
                  <p>Graduate, NASCAR Tech</p>
                  <p>Team Penske</p>
               </div>
            </div>
         </article>
         <article class="column">
            <div class="filter-card">
               <div class="sc-video-thumb" style="background-image:url(https://img.youtube.com/vi/BGBXzSLQknY/0.jpg)">
                  <a href="#" aria-haspopup="true" class="launch-sc-video" data-open="story-video-BGBXzSLQknY" aria-controls="story-video-BGBXzSLQknY" tabindex="0">
                  <span class="show-for-sr">Play the video</span>
                  <i class="fa fa-play" aria-hidden="true"></i>
                  </a>
               </div>
               <div class="filter-card--content">
                  <h3>MADISON CONRAD</h3>
                  <p>Graduate, NASCAR Tech</p>
                  <p>Specialist, Roush Yates Engines</p>
               </div>
            </div>
         </article>
         <article class="column">
            <div class="filter-card">
               <div class="sc-video-thumb" style="background-image:url(https://img.youtube.com/vi/ATY9H0UitQo/0.jpg)">
                  <a href="#" aria-haspopup="true" class="launch-sc-video" data-open="story-video-ATY9H0UitQo" aria-controls="story-video-ATY9H0UitQo" tabindex="0">
                  <span class="show-for-sr">Play the video</span>
                  <i class="fa fa-play" aria-hidden="true"></i>
                  </a>
               </div>
               <div class="filter-card--content">
                  <h3>TERRENCE STRAZDON</h3>
                  <p>Graduate, UTI Exton</p>
                  <p>Lexus of Edison</p>
               </div>
            </div>
         </article>
         <article class="column">
            <div class="filter-card">
               <div class="sc-video-thumb" style="background-image:url(https://img.youtube.com/vi/k0i3bFa4vyY/0.jpg)">
                  <a href="#" aria-haspopup="true" class="launch-sc-video" data-open="story-video-k0i3bFa4vyY" aria-controls="story-video-k0i3bFa4vyY" tabindex="0">
                  <span class="show-for-sr">Play the video</span>
                  <i class="fa fa-play" aria-hidden="true"></i>
                  </a>
               </div>
               <div class="filter-card--content">
                  <h3>RYAN WHITE</h3>
                  <p>Graduate, UTI Exton</p>
                  <p>Fred Beans Ford of Boyertown</p>
               </div>
            </div>
         </article>
         <article class="column">
            <div class="filter-card">
               <div class="sc-video-thumb" style="background-image:url(https://img.youtube.com/vi/-WIUQ0gz14U/0.jpg)">
                  <a href="#" aria-haspopup="true" class="launch-sc-video" data-open="story-video--WIUQ0gz14U" aria-controls="story-video--WIUQ0gz14U" tabindex="0">
                  <span class="show-for-sr">Play the video</span>
                  <i class="fa fa-play" aria-hidden="true"></i>
                  </a>
               </div>
               <div class="filter-card--content">
                  <h3>MICHAEL STEINKRAUSS</h3>
                  <p>Graduate, UTI Norwood</p>
                  <p>Mercedes-Benz of Burlington</p>
               </div>
            </div>
         </article>
         <article class="column">
            <div class="filter-card">
               <div class="sc-video-thumb" style="background-image:url(https://img.youtube.com/vi/qamhLNnbFc4/0.jpg)">
                  <a href="#" aria-haspopup="true" class="launch-sc-video" data-open="story-video-qamhLNnbFc4" aria-controls="story-video-qamhLNnbFc4" tabindex="0">
                  <span class="show-for-sr">Play the video</span>
                  <i class="fa fa-play" aria-hidden="true"></i>
                  </a>
               </div>
               <div class="filter-card--content">
                  <h3>BRANDON DAVIS</h3>
                  <p>Graduate, UTI Avondale</p>
                  <p>Freightliner of Arizona</p>
               </div>
            </div>
         </article>
         <article class="column">
            <div class="filter-card">
               <div class="sc-video-thumb" style="background-image:url(https://img.youtube.com/vi/vYu9jcqNEAY/0.jpg)">
                  <a href="#" aria-haspopup="true" class="launch-sc-video" data-open="story-video-vYu9jcqNEAY" aria-controls="story-video-vYu9jcqNEAY" tabindex="0">
                  <span class="show-for-sr">Play the video</span>
                  <i class="fa fa-play" aria-hidden="true"></i>
                  </a>
               </div>
               <div class="filter-card--content">
                  <h3>TRAVIS LAW </h3>
                  <p>Graduate, NASCAR Tech</p>
                  <p>Chief Mechanic, Team Penske </p>
               </div>
            </div>
         </article>
      </div>
   </div>
   <div id="wwcnpqtx4u" class="description widget  background-white less-padding">
      <div class="description-inner row column">
         <div class="description-content"></div>
      </div>
   </div>
</div>
<?php include('../../views/footer.php'); ?>