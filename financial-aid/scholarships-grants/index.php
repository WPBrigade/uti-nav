

<!DOCTYPE html> 
<html lang="en">
   <head>
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta charset="utf-8" />
      <title>
         Automotive Scholarships &amp; Grants | UTI, MMI, &amp; NASCAR Tech
      </title>
      <link type="text/css" rel="stylesheet" href="/ResourcePackages/Foundation/assets/dist/css/app.min.css" />
      <link rel="stylesheet" type="text/css" href="//cloud.typography.com/7416432/788802/css/fonts.css" />
      <!-- Google Tag Manager --> <script>
         (function (w, d, s, l, i) {
         w[l] = w[l] || []; w[l].push({
         'gtm.start':
         new Date().getTime(), event: 'gtm.js'
         }); var f = d.getElementsByTagName(s)[0],
         j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
         'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
         })(window, document, 'script', 'dataLayer', 'GTM-5BXBPH');
      </script> <!-- End Google Tag Manager -->  <script type="text/javascript">var sf_appPath='/';</script>
      <meta name="Generator" content="Sitefinity 10.0.6400.0 PU" />
      <link rel="canonical" href="https://www.uti.edu/financial-aid/scholarships-grants" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <script type="text/javascript">
         (function() {var _rdDeviceWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;var _rdDeviceHeight = (window.innerHeight > 0) ? window.innerHeight : screen.height;var _rdOrientation = (window.width > window.height) ? 'landscape' : 'portrait';})();
      </script>
      <meta name="description" content="There are opportunities available to make training more affordable. Learn more about financial aid options at UTI." />
      <meta name="keywords" content="UTI scholarships, Automotive scholarships, female automotive scholarships, diesel mechanic scholarships, automotive industry scholarships, grants for mechanics tools, automotive technician scholarships, nascar technical institute scholarships" />
   </head>
   <body role="main">
      <!-- Google Tag Manager (noscript) --> 
      <noscript> <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5BXBPH"
         height="0" width="0" style="display:none;visibility:hidden" title="google tag manager"></iframe> </noscript>
      <!-- End Google Tag Manager (noscript) -->  
      <div class="sfPublicWrapper" id="PublicWrapper">
         <?php include('../../views/navigation.php'); ?>
         <div id="Contentplaceholder1_TCC38B385001_Col00" class="sf_colsIn container" data-sf-element="Container" data-placeholder-label="Container">
            <header class="widget hero  hero--dark-image background-image  text-center " id="herooif69u" style="background-image: url('https://utiedu.azureedge.net/images/default-source/global/global-heros/scholarships-and-grants_hero_students-1920px.jpg?sfvrsn=2fc0970_2');">
               <div class="overlay" aria-hidden="true"></div>
               <div class="row align-center-middle">
                  <div class="small-11 medium-10 large-9 columns">
                     <div class="hero-content">
                        <h1>
                           UTI SCHOLARSHIPS & GRANTS
                        </h1>
                        <p>UTI will make more than <b>$15 million</b> in scholarship and grant money available to students in 2018.<sup data-anchor-id="footer-disclaimer">10</sup> Follow the steps below to see if you’re eligible for aid and how to apply.</p>
                        <div class="button-area">
                        </div>
                     </div>
                  </div>
               </div>
               <a href="#" class="scroll-to-next"><i class="fa fa-chevron-down" aria-hidden="true"></i><span class="show-for-sr">Scroll to next section</span></a>
            </header>
         </div>
         <div id="Contentplaceholder1_TCC38B385002_Col00" class="sf_colsIn container" data-sf-element="Container" data-placeholder-label="Container">
            <div style="background-image: url('https://utiedu.azureedge.net/images/default-source/global/background-assets/background-callout-metal.jpg?sfvrsn=94b818dc_0');" class="widget background-image  background-gray-light sf-border" aria-hidden="true"></div>
         </div>
         <div id="Contentplaceholder1_TCC38B385003_Col00" class="sf_colsIn container" data-sf-element="Container" data-placeholder-label="Container">
            <section  class=" description widget  to-bottom-right background-gradient-red" id="descrz6mq1j" aria-labelledby="descrz6mq1j-header">
               <div class="description-inner row column">
                  <div class="row column">
                     <div class="widget-header text-center" id="descrz6mq1j-header">
                        <h2>FOUR STEPS</h2>
                        <p>There are four steps to apply for scholarships and grants at UTI.</br>We’ll show you how to do each one.</p>
                     </div>
                  </div>
                  <div class="description-content">
                     <div class="no-media text-center">
                        <div class="cta-container">
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </div>
         <div class="sticky-anchor"></div>
         <div class="anchor-links" role="navigation">
            <div class="row column">
               <ul class="menu expanded show-for-medium" data-magellan>
                  <li>
                     <a href="#descrfw3epa">Prepare</a>
                  </li>
                  <li>
                     <a href="#accdaealis">Search</a>
                  </li>
                  <li>
                     <a href="#descrb8rs2j">Match</a>
                  </li>
                  <li>
                     <a href="#descrcbs7w6">Apply</a>
                  </li>
               </ul>
               <ul class="vertical menu accordion-menu show-for-small-only" data-accordion-menu>
                  <li>
                     <a href="#" id="anchorTitle">Overview</a>
                     <ul class="menu vertical nested">
                        <li>
                           <a href="#descrfw3epa">Prepare</a>
                        </li>
                        <li>
                           <a href="#accdaealis">Search</a>
                        </li>
                        <li>
                           <a href="#descrb8rs2j">Match</a>
                        </li>
                        <li>
                           <a href="#descrcbs7w6">Apply</a>
                        </li>
                     </ul>
                  </li>
               </ul>
            </div>
         </div>
         <section  class=" description widget  background-white" id="descrfw3epa" aria-labelledby="descrfw3epa-header">
            <div class="description-inner row column">
               <div class="row column">
                  <div class="widget-header text-center" id="descrfw3epa-header">
                     <h2>1. PREPARE YOUR STORY</h2>
                     <p>Make a list of the quick facts that tell your story. Include notes about yourself, like where you come from, where you’re headed and the things you like to do for fun. Here are some sample topics to get you started.</br>
                     <div class="row" style="margin:auto; max-width: 1000px">
                        <div class="small-12 medium-6 columns;" style="text-align:left">
                           <ul>
                              <li>Family Story</br> 
                              <li>Past work experience</br> 
                              <li>Present employment</br> 
                              <li>Future career choice</br> 
                              <li>Club memberships/professional organizations</br> 
                           </ul>
                        </div>
                        <div class="small-12 medium-6 columns;" style="text-align:left">
                           <ul>
                              <li>Military experience</br> 
                              <li>Education history and grade point average (GPA)</br> 
                              <li>Honors and awards</br> 
                              <li>Charity work</br> 
                              <li>Hobbies and interests
                           </ul>
                        </div>
                     </div>
                     </p>
                  </div>
               </div>
               <div class="description-content">
                  <div class="no-media text-center">
                     <div class="cta-container">
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <div style="background-image: url('https://utiedu.azureedge.net/images/default-source/global/background-assets/background-callout-metal.jpg?sfvrsn=94b818dc_0');" class="widget background-image  background-blue-dark sf-border" aria-hidden="true"></div>
         <div  id="wwcnty04f5" class="description widget  background-gray-light ">
            <div class="description-inner row column">
               <div class="description-content">
                  <h3 style="text-align:center;">Questions?</h3>
                  <div style="text-align:center;">If you have any questions or need additional information, you can reach out to the UTI Scholarship Department directly. You can contact us at the following:<br /><i class="fa fa-phone"></i><a href="tel:8008597249"><em></em>800-859-7249<br /><em></em></a><i class="fa fa-envelope-o contact"></i><a href="mailto:scholarships@uti.edu"> Scholarships Department</a></div>
               </div>
            </div>
         </div>
         <div style="background-image: url('https://utiedu.azureedge.net/images/default-source/global/background-assets/background-callout-metal.jpg?sfvrsn=94b818dc_0');" class="widget background-image  background-blue-dark sf-border" aria-hidden="true"></div>
         <section  id="accdaealis" class="widget  background-white accordion-container" aria-labelledby="accdaealis-header">
            <div class="row column">
               <div class="widget-header text-center" id="accdaealis-header">
                  <h2>2. SEARCH FOR RELEVANT SCHOLARSHIPS</h2>
                  <h3>WHERE TO FIND SCHOLARSHIPS</h3>
               </div>
            </div>
            <div class="row align-center">
               <div class="small-12 medium-10 large-8 columns">
                  <ul class="accordion" data-accordion data-allow-all-closed="true">
                     <li class="accordion-item is-active" data-accordion-item>
                        <a href="#" class="accordion-title">
                           <h4>Industry&#39;s Choice Scholarship - Up to $3,500 for Enrolled Students</h4>
                        </a>
                        <div class="accordion-content" data-tab-content>
                           <p>The Industry’s Choice Scholarship Program helps qualified enrolled students to decrease their overall cost of tuition. This program is open to enrolled students at Universal Technical Institute, Motorcycle Mechanics Institute, Marine Mechanics Institute and NASCAR Technical Institute. *
                              <br/><br/>
                              ELIGIBILITY REQUIREMENTS:<br/>
                              >>Must be an enrolled student scheduled to begin classes anytime through September 30, 2018.<br/>
                              >>Have a completed FAFSA at time of application submission<br/>
                              >>Complete the Industry’s Choice Scholarship Application Form<br/><br/>
                              <b><font color="#1779ba"><a href="/docs/default-source/scholarships-grants/scholarship-industrys-choice.pdf">DOWNLOAD APPLICATION</a></font></b> | <b><font color="#1779ba"><a href="https://utitech.wufoo.com/forms/q1c225ay1lzn8tl/">APPLY NOW</a></font></font></b>
                           </p>
                        </div>
                     </li>
                     <li class="accordion-item " data-accordion-item>
                        <a href="#" class="accordion-title">
                           <h4>UTI Institutional Grant - 10% of Tuition for Eligible Students</h4>
                        </a>
                        <div class="accordion-content" data-tab-content>
                           <p>The UTI Institutional Grant helps students with a demonstrated financial need to decrease their overall cost of tuition. The grant will be awarded to all students who meet the eligibility criteria; there is no separate application process. This grant is effective January 2018 and will be in effect for specific start dates.  To see the eligibility details and the list of campus start dates, please click the link below.
                              <br/><br/>
                              <b><font color="#1779ba"><a href="/docs/default-source/scholarships-grants/grant-uti-institutional.pdf">DOWNLOAD GRANT DETAILS</a></font></b>
                           </p>
                        </div>
                     </li>
                     <li class="accordion-item " data-accordion-item>
                        <a href="#" class="accordion-title">
                           <h4>TechForce Foundation</h4>
                        </a>
                        <div class="accordion-content" data-tab-content>
                           <p>TechForce Foundation is a nonprofit 501(c)(3) with the mission to champion students to and through their education and into careers as professional technicians.<br/><br/>
                              <b><a href="https://www.techforcefoundation.org/scholarships/">VISIT WEBSITE</a></b>
                           </p>
                        </div>
                     </li>
                     <li class="accordion-item " data-accordion-item>
                        <a href="#" class="accordion-title">
                           <h4>Imagine America Scholarship - $1,000 for High School Seniors</h4>
                        </a>
                        <div class="accordion-content" data-tab-content>
                           <p>Universal Technical Institute has partnered with Imagine America to offer scholarships to High School Students. To apply, go to Imagine-America.org, select Universal Technical Institute as the school you wish to attend, and complete an online application form. Your application will be forwarded to a guidance counselor or Imagine America contact at your high school who will review your application. If approved, your application will be forwarded to UTI for confirmation. <br/><br/>
                              ELIGIBILITY REQUIREMENTS:<br/> 
                              >>Be enrolled to attend Universal Technical Institute, Motorcycle Mechanics Institute, Marine Mechanics Institute or NASCAR Technical Institute<br/>
                              >>Currently enrolled in high school as a Senior and set to graduate, or recently graduated within the same calendar year<br/><br/>
                              <b><font color="#1779ba"><a href="http://www.imagine-america.org/highschoolscholarships/apply-now">VISIT IMAGINE AMERICA WEBSITE</a></b></font>
                           </p>
                        </div>
                     </li>
                     <li class="accordion-item " data-accordion-item>
                        <a href="#" class="accordion-title">
                           <h4>Imagine America Scholarship - $1,000 for Adult Skills Education</h4>
                        </a>
                        <div class="accordion-content" data-tab-content>
                           <p>Universal Technical Institute has partnered with Imagine America to offer scholarships to Adult Students. To apply, go to Imagine-America.org, select Universal Technical Institute as the school you wish to attend, and complete an online application form.<br/><br/>
                              ELIGIBILITY REQUIREMENTS:<br/> 
                              >>Be enrolled to attend Universal Technical Institute, Motorcycle Mechanics Institute, Marine Mechanics Institute or NASCAR Technical Institute<br/>
                              >>Be 19 years of age or older, and have a high school diploma or GED<br/>
                              >>Must have a financial need<br/>
                              >>Have a completed FAFSA at time of application submission<br/>
                              <br/>
                              <b><font color="#1779ba"><a href="http://www.imagine-america.org/highschoolscholarships/apply-now">VISIT IMAGINE AMERICA WEBSITE</a></b></font>
                           </p>
                        </div>
                     </li>
                     <li class="accordion-item " data-accordion-item>
                        <a href="#" class="accordion-title">
                           <h4>NASCAR Diversity Scholarship - $3,500 for Enrolled Students</h4>
                        </a>
                        <div class="accordion-content" data-tab-content>
                           <p>In an effort to further its Diversity, NASCAR is sponsoring a scholarship program for incoming students exclusively at the NASCAR Technical Institute. <br/><br/>
                              Eligibility Requirements: <br/>
                              >>Enrolled at NASCAR Technical Institute for one of the approved start dates listed on the application<br/>
                              >>Have a completed FAFSA at the time of application submission<br/>
                              >>Complete and submit your application by the appropriate deadline as listed on the application<br/>
                              <br/>
                              <b><font color="#1779ba"><a href="/docs/default-source/scholarships-grants/scholarship-nascar-diversity.pdf">DOWNLOAD APPLICATION</a></font></b>
                           </p>
                        </div>
                     </li>
                     <li class="accordion-item " data-accordion-item>
                        <a href="#" class="accordion-title">
                           <h4>NASCAR &quot;Tech of Tomorrow&quot; Scholarship - $2,500 for Enrolled Students</h4>
                        </a>
                        <div class="accordion-content" data-tab-content>
                           <p>The Tech of Tomorrow, a program sponsored by NASCAR, seeks to assist students with financial need who are looking for great career training with NASCAR Technical Institute, and who truly understand and exhibit the traits of a great team member. <br/><br/>
                              Eligibility Requirements: <br/>
                              >>Enrolled to attend NASCAR Technical Institute for one of the approved start dates listed on the application<br/>
                              >>Have a completed FAFSA at the time of application submission<br/>
                              >>Complete and submit your application by the appropriate deadline as listed on the application<br/><br/>
                              <b><font color="#1779ba"><a href="/docs/default-source/scholarships-grants/scholarship-nascar-technician-of-tomorrow.pdf">DOWNLOAD APPLICATION</a></font></b>
                           </p>
                        </div>
                     </li>
                     <li class="accordion-item " data-accordion-item>
                        <a href="#" class="accordion-title">
                           <h4>UTI National Scholarship Competition - Up to $2,500 for High School Seniors</h4>
                        </a>
                        <div class="accordion-content" data-tab-content>
                           <p>The UTI National Scholarship Competition consists of a written test containing both academic and technical questions designed to test the knowledge of participants interested in the transportation industry. The test is open to high school seniors and is offered in both the fall and spring. UTI has made a total of $475,000 available annually, to be awarded to a total of 380 students who participate in the UTI National Scholarship Competition. Participants can earn a scholarship up to $2,500 for achieving the highest scores on the test.<br/><br/>
                              High schools seniors who have already enrolled, or are interested in enrolling at Universal Technical Institute are eligible to participate.<br/><br/>
                              <b><font color="#1779ba">CONTACT YOUR ADMISSIONS REP FOR THE NEXT TESTING DATE</b></font>
                           </p>
                        </div>
                     </li>
                     <li class="accordion-item " data-accordion-item>
                        <a href="#" class="accordion-title">
                           <h4>UTI Housing Grant  - Up to $1,000 for Enrolled Students</h4>
                        </a>
                        <div class="accordion-content" data-tab-content>
                           <p>Universal Technical Institute has partnered with Collegiate Housing Services to provide a grant that will support enrolled, relocating students who face a hardship starting their education.<br/><br/>
                              ELIGIBILITY REQUIREMENTS: <br/>
                              >>Be enrolled to begin training at an UTI/MMI/NASCAR Technical Institute campus<br/>
                              >>Reflect financial need based on information provided by a completed FAFSA<br/>
                              >>Be relocating more than 50 miles to attend *UTI/MMI/NASCAR Technical Institute<br/>
                              >>Participate in the shared housing option through Collegiate Housing Services<br/>
                              >>Participate in the connect housing option through Collegiate Housing Services - only available to students attending MMI in Arizona or Florida<br/>
                              >>Submit the UTI Housing Grant Application to the UTI Scholarship Department prior to your start date<br/><br/>
                              <b><font color="#1779ba"><a href="/docs/default-source/scholarships-grants/grant-uti-housing.pdf">DOWNLOAD APPLICATION</a></font></b>
                           </p>
                        </div>
                     </li>
                     <li class="accordion-item " data-accordion-item>
                        <a href="#" class="accordion-title">
                           <h4>SkillsUSA Scholarship - From $1,000 to Full Tuition</h4>
                        </a>
                        <div class="accordion-content" data-tab-content>
                           <p>SkillsUSA Championships offer a showcase for the best career and technical students in the nation. This partnership of students, teachers and industry ensures America maintains a skilled workforce. Competitors demonstrating top hands-on skills at the state and national levels can win UTI scholarships ranging from $1,000 to full core technology program tuition.<br/><br/> 
                              ELIGIBILITY REQUIREMENTS: <br/>
                              >>High School students must be individual SkillsUSA members as well as chapter members at their schools. To compete at the national level, you must compete successfully at the state contest and be selected by your state association. <br/>
                              >>Contact your high school SkillsUSA chapter advisor or a state director about how to sign up for a competition in your area.<br/><br/> 
                              HOW TO APPLY: <br/>
                              Ask your high school SkillsUSA chapter advisor about state competitions in Automotive Service Technology, Collision Repair Technology, Collision Refinish Technology, Diesel Equipment Technology, Marine Service Technology and Motorcycle Service Technology. These competitions test your ability to perform jobs and skills as outlined by the National Institute for Automotive Excellence (ASE) and the National Automotive Technicians Education Foundation (NATEF). Each competition consists of a written exam plus hands-on workstations, including simulations, bench and component testing. A complete list of competitions and more comprehensive descriptions are available on the SkillsUSA website at: <br/><br/>
                              <b><font color="#1779ba"><a href="https://www.skillsusa.org/ ">VISIT SKILLS USA WEBSITE</a></font></b>
                              .
                           </p>
                        </div>
                     </li>
                     <li class="accordion-item " data-accordion-item>
                        <a href="#" class="accordion-title">
                           <h4>Arizona Private Schools Association - $1,000 for High School Seniors</h4>
                        </a>
                        <div class="accordion-content" data-tab-content>
                           <p>Universal Technical Institute has partnered with the Arizona Private School Association to offer scholarships to high school students within the state of Arizona.<br/><br/>
                              To apply, go to arizonapsa.org, click on the APSA Scholarships tab and download the APSA AZ High School Scholarship Application.<br/> <br/>
                              Eligibility Requirements:<br/> 
                              >>Must be a high school senior attending a high school located within the state of Arizona<br/>
                              >>Enrolled to attend the Universal Technical Institute campus located in Avondale, AZ or Motorcycle Mechanics Institute campus located in Phoenix, AZ<br/><br/> 
                              <b><font color="#1779ba"><a href="http://arizonapsa.org/ ">VISIT APSA WEBSITE</a></font></b>
                           </p>
                        </div>
                     </li>
                     <li class="accordion-item " data-accordion-item>
                        <a href="#" class="accordion-title">
                           <h4>Lisle Community School District 202 Scholarship - $5,000</h4>
                        </a>
                        <div class="accordion-content" data-tab-content>
                           <p>Universal Technical Institute has partnered with the Lisle Community School District 202 to create a scholarship program specific to students from the Lisle Community School District 202 who seek to enroll at Universal Technical Institute located in Lisle, IL.<br/><br/>
                              Eligibility Requirements:<br/>
                              >>Be enrolled to start Universal Technical Institute located in Lisle, IL<br/>
                              >>Proof that applicant is currently enrolled and set to graduate, or has previously graduated from a high school located within the Lisle Community School District 202<br/>
                              >>Submit the Lisle Community School District 202 Scholarship Application to the UTI Scholarship Department prior to the deadline listed on the application.<br/><br/>
                              <b><font color="#1779ba"><a href="/docs/default-source/scholarships-grants/scholarship-lisle-community-school-district-202.pdf">DOWNLOAD APPLICATION</a></font></b>
                           </p>
                        </div>
                     </li>
                     <li class="accordion-item " data-accordion-item>
                        <a href="#" class="accordion-title">
                           <h4>Naperville Community Unit School District 203 - $5,000</h4>
                        </a>
                        <div class="accordion-content" data-tab-content>
                           <p>Universal Technical Institute has partnered with the Naperville Community Unit School District 203 to create a scholarship program specific to students from the Naperville Community School District who seek to enroll at Universal Technical Institute located in Lisle, IL.<br/><br/>
                              Eligibility Requirements:<br/> 
                              >>Be enrolled to start Universal Technical Institute located in Lisle, IL<br/>
                              >>Proof that applicant is currently enrolled and set to graduate, or has previously graduated from a high school located within the Naperville Community Unit School District 203<br/>
                              >>Submit the Naperville Community Unit School District 203 Scholarship Application to the UTI Scholarship Department prior to the deadline listed on the application.<br/><br/>
                              <b><font color="#1779ba"><a href="/docs/default-source/scholarships-grants/scholarship-naperville-community-unit-school-district-203.pdf">DOWNLOAD APPLICATION</a></font></b>
                           </p>
                        </div>
                     </li>
                     <li class="accordion-item " data-accordion-item>
                        <a href="#" class="accordion-title">
                           <h4>Phoenix Union High School District Scholarship - Up to $6,000</h4>
                        </a>
                        <div class="accordion-content" data-tab-content>
                           <p>Universal Technical Institute has partnered with the Phoenix Union High School District to create a scholarship program specific to students from the Phoenix Union High School District who seek to enroll at Universal Technical Institute, NASCAR Technical Institute, or Motorcycle and Marine Mechanics Institute.<br/><br/>
                              Eligibility Requirements:<br/>
                              >>Be enrolled to start UTI, MMI or NASCAR Technical Institute<br/> 
                              >>Proof that applicant is currently enrolled and set to graduate, or previously graduated from a high school located within the Phoenix Union High School District during or after the 2014 academic year<br/>
                              >>Provide a signature of endorsement from an instructor, guidance counselor or employer as instructed on the application<br/>
                              >>Submit the Phoenix Union High School District Scholarship Application to the UTI Scholarship Department prior to the deadline listed on the application.<br/><br/>
                              <b><font color="#1779ba"><a href="/docs/default-source/scholarships-grants/scholarship-phoenix-union-high-school-district.pdf">DOWNLOAD APPLICATION</a></font></b>
                           </p>
                        </div>
                     </li>
                     <li class="accordion-item " data-accordion-item>
                        <a href="#" class="accordion-title">
                           <h4>Village of Lisle Scholarship - $5,000</h4>
                        </a>
                        <div class="accordion-content" data-tab-content>
                           <p>Universal Technical Institute has partnered with the Village of Lisle Community to create a scholarship program specific to students who are residents of the Village of Lisle Community who seek to enroll at Universal Technical Institute located in Lisle, IL.<br/><br/>
                              Eligibility Requirements:<br/>
                              >>Be enrolled to start Universal Technical Institute located in Lisle, IL<br/>
                              >>Be a resident of the Village of Lisle Community<br/>
                              >>Submit the Village of Lisle Scholarship Application to the UTI Scholarship Department prior to the deadline listed on the application<br/><br/>
                              <b><font color="#1779ba"><a href="/docs/default-source/scholarships-grants/scholarship-village-of-lisle.pdf">DOWNLOAD APPLICATION</a></font></b>
                           </p>
                        </div>
                     </li>
                     <li class="accordion-item " data-accordion-item>
                        <a href="#" class="accordion-title">
                           <h4>Osceola County School District - Up to $5,000</h4>
                        </a>
                        <div class="accordion-content" data-tab-content>
                           <p>Universal Technical Institute has partnered with the Osceola County School District to create a scholarship program specific to students from the Osceola County School District who seek to enroll at Universal Technical Institute, NASCAR Technical Institute, or Motorcycle and Marine Mechanics Institute.<br/><br/>
                              Eligibility Requirements:<br/> 
                              >>Be enrolled to start UTI, MMI or NASCAR Technical Institute<br/>
                              >>Proof that applicant is currently enrolled and set to graduate from a high school located within the Osceola County School District during the 2017-2018 academic year<br/>
                              >>Provide a signature of endorsement from an instructor, guidance counselor or employer as instructed on the application<br/>
                              >>Submit the Osceola County School District Scholarship Application to the UTI Scholarship Department prior to the deadline listed on the application<br/><br/>
                              <b><font color="#1779ba"><a href="https://uti-staging.laneterralever.com/docs/default-source/scholarships-grants/scholarship-osceola-county-school-district.pdf">DOWNLOAD APPLICATION</a></font></b>
                           </p>
                        </div>
                     </li>
                     <li class="accordion-item " data-accordion-item>
                        <a href="#" class="accordion-title">
                           <h4>Work Ethic Scholarship Program – mikeroweWORKS Foundation</h4>
                        </a>
                        <div class="accordion-content" data-tab-content>
                           <p>Universal Technical Institute has partnered with the mikeroweWORKS Foundation to offer scholarships to eligible students who are enrolled to attend Universal Technical Institute, NASCAR Technical Institute, or Motorcycle and Marine Mechanics Institute.<br/><br/> 
                              <b><font color="#1779ba"><a href="http://profoundlydisconnected.com/">APPLY AT mikeroweWORKS FOUNDATION</a></font></b>
                           </p>
                        </div>
                     </li>
                     <li class="accordion-item " data-accordion-item>
                        <a href="#" class="accordion-title">
                           <h4>Houston Independent School District (HISD) Scholarship - $2,500</h4>
                        </a>
                        <div class="accordion-content" data-tab-content>
                           <p>Universal Technical Institute has partnered with the Houston Independent School District to create a scholarship program specific to students from the Houston Independent School District who seek to enroll at Universal Technical Institute, NASCAR Technical Institute, or Motorcycle and Marine Mechanics Institute.<br/><br/> 
                              Eligibility Requirements:<br/> 
                              >>Be enrolled to start UTI, MMI or NASCAR Technical Institute<br/>
                              >>Proof that applicant is currently enrolled and set to graduate, or previously graduated from a high school located within the Houston Independent School District during or after the 2014 academic year<br/>
                              >>Provide a signature of endorsement from an instructor, guidance counselor or employer as instructed on the application<br/>
                              >>Submit the Houston Independent School District Scholarship Application to the UTI Scholarship Department prior to the deadline listed on the application<br/><br/>
                              <b><font color="#1779ba"><a href="/docs/default-source/scholarships-grants/scholarship-houston-integrated-school-district.pdf">DOWNLOAD</a></font></b>
                           </p>
                        </div>
                     </li>
                     <li class="accordion-item " data-accordion-item>
                        <a href="#" class="accordion-title">
                           <h4>UTI Hurricane Harvey Relief Scholarship Program - $1,000</h4>
                        </a>
                        <div class="accordion-content" data-tab-content>
                           <p>In an effort to provide tuition assistance to students directly impacted by Hurricane Harvey, Universal Technical Institute is sponsoring a scholarship program for incoming students who seek to enroll at Universal Technical Institute (UTI), NASCAR Technical Institute (NASCAR Tech), or Motorcycle and Marine Mechanics Institute (MMI). <br/><br/>
                              Eligibility Requirements: <br/>
                              >> Be enrolled to start UTI (with the exception of the UTI, PA campus), MMI, or NASCAR Technical Institute<br/>
                              >> Have been a resident of the state of Texas on August 25, 2017<br/>
                              >>Submit the completed application to the Scholarship Department prior to starting classes at UTI/MMI/NASCAR Tech<br/><br/>
                              <b><font color="#1779ba"><a href="/docs/default-source/scholarships-grants/scholarship-uti-hurricane-harvey-relief.pdf">DOWNLOAD APPLICATION</a></font></b>
                           </p>
                        </div>
                     </li>
                     <li class="accordion-item " data-accordion-item>
                        <a href="#" class="accordion-title">
                           <h4>UTI Texas Career Scholarship - $1,000</h4>
                        </a>
                        <div class="accordion-content" data-tab-content>
                           <p>The UTI Texas Career Scholarship Program helps to provide tuition assistance to students located within the state of Texas, who seek to enroll at Universal Technical Institute, NASCAR Technical Institute, or Motorcycle and Marine Mechanics Institute.<br/><br/>
                              Eligibility Requirements:<br/> 
                              >>Be enrolled to start UTI, MMI, or NASCAR Technical Institute<br/>
                              >>Show proof that they are currently a high school senior, enrolled and set to graduate (or recently graduated) from a high school located within the state of Texas during the 2017-2018 academic year<br/>
                              >>Submit the UTI Texas Career Scholarship Application to the UTI Scholarship Department prior to the deadline listed on the application<br/><br/>
                              <b><font color="#1779ba"><a href="/docs/default-source/scholarships-grants/scholarship-texas-career.pdf">DOWNLOAD APPLICATION</a></font></b>
                           </p>
                        </div>
                     </li>
                     <li class="accordion-item " data-accordion-item>
                        <a href="#" class="accordion-title">
                           <h4>Salute to Service Scholarship</h4>
                        </a>
                        <div class="accordion-content" data-tab-content>
                           <p>Prospective students who meet all other admissions requirements and who can show verification of an honorable discharge from the US Armed Forces via a valid DD214 may apply for a Salute to Service Scholarship. This program is not available to students enrolling at the Exton, PA campus.ii. Students eligible for this program work directly with the VA team. <br/></p>
                        </div>
                     </li>
                     <li class="accordion-item " data-accordion-item>
                        <a href="#" class="accordion-title">
                           <h4>Public Scholarships for UTI Students</h4>
                        </a>
                        <div class="accordion-content" data-tab-content>
                           <p><b><font color="#1779ba"><a href="http://www.automotivehalloffame.org/">Automotive Hall of Fame</a></font></b>
                              <br/>>><b><font color="#1779ba"><a href="http://www.afsa.org/essay-contest">American Foreign Service Association</a></font></b>
                              <br/>>><b><font color="#1779ba"><a href="https://bkmclamorefoundation.org/">Burger King McLamore Foundation</a></font></b>
                              <br/>>><b><font color="#1779ba"><a href="http://www.syf.org/ ">Simon Youth Foundation Community Scholarship Program</a></font></b>
                              <br/>>><b><font color="#1779ba"><a href="https://www.ffa.org/participate/grants-and-scholarships">National Future Farmers of America Scholarship Program</a></font></b>
                              <br/>>><b><font color="#1779ba"><a href="https://www.rmhc.org/rmhc-us-scholarships">Ronald McDonald House Charities Scholarship Program</a></font></b>
                              <br/>>><b><font color="#1779ba"><a href="http://thebladeguru.com/scholarship/ ">Blade Guru Scholarship</a></font></b>
                              <br/>>><b><font color="#1779ba"><a href="http://www.automotivescholarships.com/ ">GAAS (Global Automotive Aftermarket Symposium)</a></font></b>
                              <br/>>><b><font color="#1779ba"><a href="https://www.cjponyparts.com/cj-pony-parts-scholarship-video-contest ">CJ Pony Scholarship</a></font></b>
                              <br/>>><b><font color="#1779ba"><a href="/docs/default-source/scholarships-grants/scholarship-rotary-vocational.pdf">Rotary Vocational Scholarship</a></font></b>
                              <br/>>><b><font color="#1779ba"><a href="http://www.affordablecolleges.com/resources/scholarships/ ">AffordableColleges.com</a></font></b>
                              <br/>>><b><font color="#1779ba"><a href="/docs/default-source/scholarships-grants/scholarship-information-pamphlet.pdf ">Scholarship Information Pamphlet</a></font></b>
                           </p>
                        </div>
                     </li>
                  </ul>
               </div>
            </div>
         </section>
         <div style="background-image: url('https://utiedu.azureedge.net/images/default-source/global/background-assets/background-callout-metal.jpg?sfvrsn=94b818dc_0');" class="widget background-image  background-yellow sf-border" aria-hidden="true"></div>
         <div  id="wwcnty04f5" class="description widget  background-gray-light ">
            <div class="description-inner row column">
               <div class="description-content">
                  <h3 style="text-align:center;">Questions?</h3>
                  <div style="text-align:center;">If you have any questions or need additional information, you can reach out to the UTI Scholarship Department directly. You can contact us at the following:<br /><i class="fa fa-phone"></i><a href="tel:8008597249"><em></em>800-859-7249<br /><em></em></a><i class="fa fa-envelope-o contact"></i><a href="mailto:scholarships@uti.edu"> Scholarships Department</a></div>
               </div>
            </div>
         </div>
         <div style="background-image: url('https://utiedu.azureedge.net/images/default-source/global/background-assets/background-callout-metal.jpg?sfvrsn=94b818dc_0');" class="widget background-image  background-yellow sf-border" aria-hidden="true"></div>
         <section  class=" description widget  background-white" id="descrb8rs2j" aria-labelledby="descrb8rs2j-header">
            <div class="description-inner row column">
               <div class="row column">
                  <div class="widget-header text-center" id="descrb8rs2j-header">
                     <h2>3. FIND YOUR MATCH</h2>
                     <h3>FOCUS YOUR EFFORT</h3>
                     <p>
                     <div align="left">
                        How you compare and rank these scholarships is up to you. Evaluate whether or not you qualify for scholarships based on an honest inventory of each scholarship’s eligibility requirements.<br/><br/>
                        <h3>HERE ARE SOME SUGGESTIONS TO CONSIDER:</h3>
                        <b>Eligibility Requirements</b><br/>
                        Applicants who fail to meet all eligibility requirements are often immediately discarded and not considered further. If you don’t meet one or more of a scholarship’s requirements, skip it and apply your time toward other scholarship opportunities.<br/><br/>
                        <b>Application Deadline</b><br/>
                        Do you have a realistic amount of time to gather your application materials, many of which can include recommendations, a written exam or essay? Give your references ample time to write you a thoughtful recommendation and give yourself enough time to write a well-thought-out essay then have it reviewed by a trusted advisor or teacher.<br/><br/>
                        <b>Application Requirements</b><br/>
                        Look at what each scholarship requires in order to apply and make a checklist. Are you able to gather recommendations? Are you willing to sit down and write an essay response to a scholarship question? Does the application require a copy of your high school transcript? Make sure you have these items ready before deciding to apply for a scholarship.
                     </div>
                     </p>
                  </div>
               </div>
               <div class="description-content">
                  <div class="no-media text-center">
                     <div class="cta-container">
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <div style="background-image: url('https://utiedu.azureedge.net/images/default-source/global/background-assets/background-callout-metal.jpg?sfvrsn=94b818dc_0');" class="widget background-image  background-blue-dark sf-border" aria-hidden="true"></div>
         <div  id="wwcnty04f5" class="description widget  background-gray-light ">
            <div class="description-inner row column">
               <div class="description-content">
                  <h3 style="text-align:center;">Questions?</h3>
                  <div style="text-align:center;">If you have any questions or need additional information, you can reach out to the UTI Scholarship Department directly. You can contact us at the following:<br /><i class="fa fa-phone"></i><a href="tel:8008597249"><em></em>800-859-7249<br /><em></em></a><i class="fa fa-envelope-o contact"></i><a href="mailto:scholarships@uti.edu"> Scholarships Department</a></div>
               </div>
            </div>
         </div>
         <div style="background-image: url('https://utiedu.azureedge.net/images/default-source/global/background-assets/background-callout-metal.jpg?sfvrsn=94b818dc_0');" class="widget background-image  background-blue-dark sf-border" aria-hidden="true"></div>
         <section  class=" description widget  background-white" id="descrcbs7w6" aria-labelledby="descrcbs7w6-header">
            <div class="description-inner row column">
               <div class="row column">
                  <div class="widget-header text-center" id="descrcbs7w6-header">
                     <h2>4. APPLY FOR AID</h2>
                     <h3>APPLICATION TIPS & RESOURCES</h3>
                     <p>
                     <div align="left">
                        With your choices narrowed, it's time to apply. Relax! Most applications are easy to complete. The key is to follow directions and provide exactly what is required.<br/><br/>
                        <h3>APPLICATION TIPS & RESOURCES</h3>
                        With your choices narrowed, it's time to apply. Relax! Most applications are easy to complete. The key is to follow directions and provide exactly what is required.<br/><br/>
                        <b>Read First. Write Second.</b>
                        Most applications require multiple steps, so make sure you understand the entire application before filling it out.<br/><br/>
                        <b>Master the Basics</b>
                        Many applications require essays or recommendations. Don’t be afraid to ask for assistance. A favorite teacher, high school counselor, or trusted advisor or mentor can help you complete your applications. It’s always a good idea to ask someone to proofread your work and help you catch mistakes before you submit your application!
                     </div>
                     </p>
                  </div>
               </div>
               <div class="description-content">
                  <div class="no-media text-center">
                     <div class="cta-container">
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <div  id="" class="description widget  background-white ">
            <div class="description-inner row column">
               <div class="description-content"></div>
            </div>
         </div>
         <div class="sf-breadcrumb breadcrumbs-container">
            <div class="breadcrumb-inner">
               <nav aria-label="You are here:">
                  <ul class="breadcrumbs">
                     <li><a href="/">Home</a></li>
                     <li><a href="/financial-aid">Financial Aid</a></li>
                     <li><span>Scholarships &amp; Grants</span></li>
                  </ul>
               </nav>
            </div>
         </div>
         <div class="main-footer widget  to-bottom-right-alt background-gradient-blue" >
            <div class="row align-center-middle hide-for-large">
               <div class="small-5 medium-3 columns">
                  <a class="logo" href="/schools/universal-technical-institute">
                  <img data-src="https://utiedu.azureedge.net/images/default-source/global/logos/footer-logos-only/uti_rev_125x60.png?sfvrsn=dfdeb775_6" alt="UTI_Rev_125x60" />
                  </a>
               </div>
               <div class="small-5 medium-3 columns">
                  <a class="logo" href="/schools/motorcycle-mechanics-institute">
                  <img data-src="https://utiedu.azureedge.net/images/default-source/global/logos/footer-logos-only/mmi_moto_rev_125x60.png?sfvrsn=7a4612b6_10" alt="MMI_Moto_Rev_125x60" />
                  </a>
               </div>
               <div class="small-5 medium-3 columns">
                  <a class="logo" href="/schools/marine-mechanics-institute">
                  <img data-src="https://utiedu.azureedge.net/images/default-source/global/logos/footer-logos-only/mmi_marine_rev_125x60.png?sfvrsn=866b017c_8" alt="MMI_Marine_Rev_125x60" />
                  </a>
               </div>
               <div class="small-5 medium-3 columns">
                  <a class="logo" href="/schools/nascar-technical-institute">
                  <img data-src="https://utiedu.azureedge.net/images/default-source/global/logos/footer-logos-only/nascar_tech_rev_125x60.png?sfvrsn=5cbc10f9_6" alt="NASCAR_Tech_Rev_125x60" />
                  </a>
               </div>
            </div>
            <div class="row align-justify">
               <div class="small-6 medium-4 large-2 columns footer-column">
                  <strong>Getting Started</strong>
                  <ul>
                     <li>
                        <a href="/schools">Schools</a>
                     </li>
                     <li>
                        <a href="/programs">Core Programs</a>
                     </li>
                     <li>
                        <a href="/why-uti/industry-partnerships/original-equipment-manufacturers">Specialized Training</a>
                     </li>
                     <li>
                        <a href="/locations/">Locations</a>
                     </li>
                     <li>
                        <a href="/admissions/tuition/">Tuition</a>
                     </li>
                     <li>
                        <a href="/why-uti">Why UTI</a>
                     </li>
                  </ul>
               </div>
               <div class="small-6 medium-4 large-2 columns footer-column">
                  <strong>Becoming a Student</strong>
                  <ul>
                     <li>
                        <a href="/admissions">Admissions</a>
                     </li>
                     <li>
                        <a href="/programs/catalogs">Catalogs</a>
                     </li>
                     <li>
                        <a href="/financial-aid">Financial Aid</a>
                     </li>
                     <li>
                        <a href="/financial-aid/scholarships-grants">Scholarships &amp; Grants</a>
                     </li>
                     <li>
                        <a href="/support-services/military-veteran-services/">Military Services</a>
                     </li>
                     <li>
                        <a href="/support-services/disability-services">Disability Services</a>
                     </li>
                  </ul>
               </div>
               <div class="small-6 medium-4 large-2 columns footer-column">
                  <strong>About Us</strong>
                  <ul>
                     <li>
                        <a href="/about#wwcnyl10iu">Accreditation</a>
                     </li>
                     <li>
                        <a href="/about/">Our Mission &amp; Values</a>
                     </li>
                     <li>
                        <a href="/careers/">Careers</a>
                     </li>
                     <li>
                        <a href="https://uti.investorroom.com/">Investor Relations</a>
                     </li>
                     <li>
                        <a href="https://uti.mediaroom.com/">Newsroom</a>
                     </li>
                  </ul>
               </div>
               <div class="small-6 medium-4 large-2 columns footer-column">
                  <strong>Resources</strong>
                  <ul>
                     <li>
                        <a href="/support-services/housing-assistance">Housing Assistance</a>
                     </li>
                     <li>
                        <a href="/why-uti/jobs-career-opportunities">Job Opportunities</a>
                     </li>
                     <li>
                        <a href="">Parent Information</a>
                     </li>
                     <li>
                        <a href="/support-services/voter-registration">Voter Registration</a>
                     </li>
                     <li>
                        <a href="/1098-t">1098-T</a>
                     </li>
                  </ul>
               </div>
               <div class="small-6 medium-4 large-2 columns footer-column">
                  <strong>Contact</strong>
                  <ul>
                     <li>
                        <a href="tel:8008347308">800-834-7308</a>
                     </li>
                     <li>
                        <a href="/request-info">Request Info</a>
                     </li>
                     <li>
                        <a href="/contact-us">Contact Us</a>
                     </li>
                     <li>
                        <a href="/locations/campus-tours">Campus Tours</a>
                     </li>
                  </ul>
               </div>
               <div class="small-12 medium-4 large-2 columns footer-column show-for-large">
                  <div class="row collapse">
                     <div class="small-6 medium-12 columns">
                        <a class="footer-logo" href="/schools/universal-technical-institute">
                        <img data-src="https://utiedu.azureedge.net/images/default-source/global/logos/footer-logos-only/uti_rev_125x60.png?sfvrsn=dfdeb775_6" alt="UTI_Rev_125x60" />
                        </a>
                     </div>
                     <div class="small-6 medium-12 columns">
                        <a class="footer-logo" href="/schools/motorcycle-mechanics-institute">
                        <img data-src="https://utiedu.azureedge.net/images/default-source/global/logos/footer-logos-only/mmi_moto_rev_125x60.png?sfvrsn=7a4612b6_10" alt="MMI_Moto_Rev_125x60" />
                        </a>
                     </div>
                     <div class="small-6 medium-12 columns">
                        <a class="footer-logo" href="/schools/marine-mechanics-institute">
                        <img data-src="https://utiedu.azureedge.net/images/default-source/global/logos/footer-logos-only/mmi_marine_rev_125x60.png?sfvrsn=866b017c_8" alt="MMI_Marine_Rev_125x60" />
                        </a>
                     </div>
                     <div class="small-6 medium-12 columns">
                        <a class="footer-logo" href="/schools/nascar-technical-institute">
                        <img data-src="https://utiedu.azureedge.net/images/default-source/global/logos/footer-logos-only/nascar_tech_rev_125x60.png?sfvrsn=5cbc10f9_6" alt="NASCAR_Tech_Rev_125x60" />
                        </a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row column" aria-hidden="true">
               <div class="divider" aria-hidden="true"></div>
            </div>
            <div class="main-footer--disclaimer row column" aria-label="disclaimer">
               <p>
               <p>1) UTI cannot guarantee employment or salary.</p>
               <p>2) For important information about the educational debt, earnings and completion rates of students who attended this program, visit our website at <a href="/disclosures">www.uti.edu/disclosures.</a></p>
               </p>
            </div>
            <div class="main-footer--bottom row align-center-middle">
               <div class="small-12 columns">
                  <span aria-label="copyright" class="copyright">&copy; 2017 Universal Technical Institute, Inc.  All rights reserved.</span>
                  <ul class="menu">
                     <li>
                        <a href="/privacy-policy">Privacy Policy</a>
                     </li>
                     <li>
                        <a href="/legal-notice">Legal Notice</a>
                     </li>
                     <li>
                        <a href="/accessibility">Accessibility</a>
                     </li>
                     <li>
                        <a href="/disclosures">Disclosures</a>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script> <script src="/ResourcePackages/Foundation/assets/dist/js/vendor.min.js"></script> <script src="/ResourcePackages/Foundation/assets/dist/js/app.min.js"></script> <script type="text/javascript" src="/WebResource.axd?d=aEUP997Y9Nb8rRTc7ItCh7yWGN90DKKlUetyIYdlaWqudZYJh8srgRDOWtALfuva9X-3WY-wcvSbDKRRAvHxXJxEeDyWPF1twTcl5LwZqBwFiF9kI9eftRlwwL6eKDUkiyskbJC3Ahb1L6a8nX38lZ10Akmw_CbCu-GBQr5L2qjyP2emzkEIns6QOn2CJ4WA0&amp;t=636251111960000000"></script><script type="text/javascript">
         StatsClient.LogVisit('d8fed5c3-ddd0-42d6-85f8-8771e2ed259e', '2dfbbf4e-9bec-44c1-a864-6066ac575c53');
      </script><script type="text/javascript">
         (function($) {
           $(document).ready(function() {
             $('.info-overlay h3').each(function() {
               $(this).text($(this).text().replace("Ε", "E"));
               $(this).text($(this).text().replace("Μ", "M"));
               $(this).text($(this).text().replace("Χ", "X"));
             });
           });
         })(jQuery);
      </script> <script>
         $(function () {
             // Don't set callback function in page editor, but still load the API for geolocation
         
             var mapSrc = "https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBwfNOCPCwCdiMVQAl4EUtuLGBFqbv2lDU";
             mapSrc = ($('.map-list').length > 0 && !$('body').hasClass('sfPageEditor')) ? mapSrc + "&callback=initMap" : mapSrc;
             $.getScript(mapSrc);
         })
      </script> 
   </body>
</html>

