<?php include('../../../../views/header.php'); ?>
<?php include('../../../../views/navigation.php'); ?>
<div id="Contentplaceholder1_TCC38B385001_Col00" class="sf_colsIn container" data-sf-element="Container" data-placeholder-label="Container">
   <header class="widget hero  hero--dark-image background-image  text-center " id="herohal788" style="background-image: url('https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/specialized-training_hero_bmw.jpg?sfvrsn=a9d7d07_8');">
      <div class="overlay" aria-hidden="true"></div>
      <div class="row align-center-middle">
         <div class="small-11 medium-10 large-9 columns">
            <div class="hero-content">
               <h1>
                  BMW SERVICE TECHNICIAN <br>EDUCATION PROGRAM (STEP)
                  <span>Manufacturer-paid training</span>
               </h1>
               <div class="button-area">
               </div>
            </div>
         </div>
      </div>
      <a href="#" class="scroll-to-next"><i class="fa fa-chevron-down" aria-hidden="true"></i><span class="show-for-sr">Scroll to next section</span></a>
   </header>
   <div class="sticky-anchor"></div>
   <div class="anchor-links" role="navigation" style="position: relative; transform: translateY(0px);">
      <div class="row column">
         <ul class="menu expanded show-for-medium" data-magellan="" data-resize="jrgu16-magellan" data-scroll="jrgu16-magellan" id="jrgu16-magellan" data-b="l84bm6-b" data-events="resize">
            <li>
               <a href="#mdsldrewiwl5">Courses</a>
            </li>
            <li>
               <a href="#igye8sl3">Vehicles</a>
            </li>
            <li>
               <a href="#vlpr5xmlf7">Benefits</a>
            </li>
            <li>
               <a href="#stbl4jgmw0">Prerequisites</a>
            </li>
            <li>
               <a href="#descrxk9ca3">Locations</a>
            </li>
            <li>
               <a href="#acfqk1hsse">FAQ</a>
            </li>
         </ul>
         <ul class="vertical menu accordion-menu show-for-small-only" data-accordion-menu="" role="tree" aria-multiselectable="true" data-b="bqgisx-b">
            <li role="treeitem" class="is-accordion-submenu-parent" aria-controls="gxrps0-acc-menu" aria-expanded="false" id="je20wc-acc-menu-link">
               <a href="#" id="anchorTitle">Overview</a>
               <ul class="menu vertical nested submenu is-accordion-submenu" data-submenu="" role="group" style="display: none;" aria-labelledby="je20wc-acc-menu-link" aria-hidden="true" id="gxrps0-acc-menu">
                  <li role="treeitem" class="is-submenu-item is-accordion-submenu-item">
                     <a href="#mdsldrewiwl5">Courses</a>
                  </li>
                  <li role="treeitem" class="is-submenu-item is-accordion-submenu-item">
                     <a href="#igye8sl3">Vehicles</a>
                  </li>
                  <li role="treeitem" class="is-submenu-item is-accordion-submenu-item">
                     <a href="#vlpr5xmlf7">Benefits</a>
                  </li>
                  <li role="treeitem" class="is-submenu-item is-accordion-submenu-item">
                     <a href="#stbl4jgmw0">Prerequisites</a>
                  </li>
                  <li role="treeitem" class="is-submenu-item is-accordion-submenu-item">
                     <a href="#descrxk9ca3">Locations</a>
                  </li>
                  <li role="treeitem" class="is-submenu-item is-accordion-submenu-item">
                     <a href="#acfqk1hsse">FAQ</a>
                  </li>
               </ul>
            </li>
         </ul>
      </div>
   </div>
   <section class=" numbers-facts widget  background-gradient-blue" id="stblap4epj" aria-labelledby="stblap4epj-header">
      <div class="numbers-facts-inner">
         <div class="row align-center">
            <div class="small-12 medium-10 large-9 columns">
               <div class="widget-header text-center" id="stblap4epj-header">
                  <h2>BMW STEP OVERVIEW</h2>
                  <p>The 16-week BMW Manufacturer-Specific Advanced Training (MSAT) program provides select UTI students with extensive training on BMW vehicles. BMW pays for the training to prepare graduates for jobs working directly for BMW at BMW dealers nationwide.</p>
               </div>
            </div>
         </div>
         <div class="row align-center">
            <div class="small-11 large-12 columns">
               <div class="numbers-facts-stats row small-up-1 medium-up-2 large-up-3 align-top align-center-middle">
                  <div class="column" style="height: 166.5px;">
                     <div class="numbers-facts-stat has-accent">
                        <span class="number">16 weeks</span>
                        <span class="stat-text">
                        Length of Specialized Training from start to finish
                        <sup data-anchor-id="2">8</sup>
                        </span>
                     </div>
                  </div>
                  <div class="column" style="height: 166.5px;">
                     <div class="numbers-facts-stat has-accent">
                        <span class="number">8 credentials</span>
                        <span class="stat-text">
                        Factory credentials earned
                        </span>
                     </div>
                  </div>
                  <div class="column" style="height: 166.5px;">
                     <div class="numbers-facts-stat has-accent">
                        <span class="number">Level 3</span>
                        <span class="stat-text">
                        BMW Technician Status achieved
                        </span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <article id="mdsldrewiwl5" class="widget media-slider  background-white" aria-label="">
      <div class="row column">
         <header class="widget-header">
            <h2>SEE WHAT YOU'LL LEARN IN BMW STEP</h2>
            <p>STEP is 16 weeks of intensive BMW-specific training on current models of some of the world's most precision-based driving machines. After successfully completing this program you will achieve Level 3 BMW Technician status. You'll receive advanced training on diagnostic paths and computer strategies unique to this world-class brand.</p>
         </header>
         <div class="media-slider--slides slick-initialized slick-slider">
            <button type="button" data-role="none" class="slick-prev slick-arrow slick-disabled" aria-label="Next Slide" aria-disabled="true" style="top: 210.4px; height: 420.8px;"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>
            <div aria-live="polite" class="slick-list draggable" style="height: 629.2px;">
               <div class="slick-track" style="opacity: 1; width: 3735px; transform: translate3d(0px, 0px, 0px);">
                  <section class="media-slider--slide media-slider--image-slide slick-slide slick-current slick-active" aria-label="Slide 0" data-slick-index="0" aria-hidden="false" style="width: 747px;">
                     <figure class="media-slider--media media-wrapper ">
                        <img src="https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/media-slider/specialized-training_image_bmw_6_slider.jpg?sfvrsn=3ad01413_2" alt="specialized-training_image_bmw_6_slider">
                     </figure>
                     <div class="media-slider--slide-content">
                        <h3>NEW ENGINE TECHNOLOGY</h3>
                        <p>
                           Learn manufacturer-approved processes for complete engine disassembly and reassembly, new engine fundamentals and basic measurement techniques.
                        </p>
                     </div>
                  </section>
                  <section class="media-slider--slide media-slider--image-slide slick-slide" aria-label="Slide 1" data-slick-index="1" aria-hidden="true" style="width: 747px;">
                     <figure class="media-slider--media media-wrapper ">
                        <img src="https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/media-slider/specialized-training_image_bmw_7_slider.jpg?sfvrsn=350614a8_2" alt="specialized-training_image_bmw_7_slider">
                     </figure>
                     <div class="media-slider--slide-content">
                        <h3>BMW TECHNICAL SYSTEMS</h3>
                        <p>
                           Cars no longer run simply off a battery, alternator and carburetor. Instead, their highly advanced systems employ more software code than a Windows operating system. Basically, you need this class. It’s where you'll learn the detailed breakdown of the BMW technical information system (TIS) as well as other diagnostic systems specific to BMW dealers and service centers.
                        </p>
                     </div>
                  </section>
                  <section class="media-slider--slide media-slider--image-slide slick-slide" aria-label="Slide 2" data-slick-index="2" aria-hidden="true" style="width: 747px;">
                     <figure class="media-slider--media media-wrapper ">
                        <img src="https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/media-slider/specialized-training_image_bmw_5_slider.jpg?sfvrsn=9d85ae8a_2" alt="specialized-training_image_bmw_5_slider">
                     </figure>
                     <div class="media-slider--slide-content">
                        <h3>MANUFACTURER PAID</h3>
                        <p>
                           While you're in school working on cars, UTI is working, too. Our team will work with you to arrange job interviews. After graduating from the STEP program, BMW requires you to work in their dealerships for a 12-month minimum. Once this employment period ends, you can choose to explore other career opportunities outside of BMW authorized dealers.
                        </p>
                     </div>
                  </section>
                  <section class="media-slider--slide media-slider--image-slide slick-slide" aria-label="Slide 3" data-slick-index="3" aria-hidden="true" style="width: 747px;">
                     <figure class="media-slider--media media-wrapper ">
                        <img src="https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/media-slider/specialized-training_image_bmw_1_slider.jpg?sfvrsn=77db85b7_2" alt="specialized-training_image_bmw_1_slider">
                     </figure>
                     <div class="media-slider--slide-content">
                        <h3>ELECTRICAL TECHNOLOGY</h3>
                        <p>
                           Students will learn basic electrical concepts and perform vehicle electrical system measurements. Participants will become proficient and confident when performing measurements with a DVOM as well as BMW specific diagnostic tools.
                        </p>
                     </div>
                  </section>
                  <section class="media-slider--slide media-slider--image-slide slick-slide" aria-label="Slide 4" data-slick-index="4" aria-hidden="true" style="width: 747px;">
                     <figure class="media-slider--media media-wrapper ">
                        <img src="https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/media-slider/specialized-training_image_bmw_9_slider.jpg?sfvrsn=cc808701_2" alt="specialized-training_image_bmw_9_slider">
                     </figure>
                     <div class="media-slider--slide-content">
                        <h3>DRIVETRAIN TECHNOLOGY</h3>
                        <p>
                           Students will work on mechanical components and operation of BMW engines, and will gain hands-on experience servicing various engine components to BMW standards.
                        </p>
                     </div>
                  </section>
               </div>
            </div>
            <button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Previous Slide" style="top: 210.4px; height: 420.8px;" aria-disabled="false"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
         </div>
         <div class="media-slider--slide-nav slick-initialized slick-slider">
            <button type="button" data-role="none" class="slick-prev slick-arrow slick-disabled" aria-label="Next Thumbnail Slide" aria-disabled="true" style=""><i class="fa fa-chevron-left" aria-hidden="true"></i></button>
            <div aria-live="polite" class="slick-list draggable">
               <div class="slick-track" style="opacity: 1; width: 1245px; transform: translate3d(0px, 0px, 0px);">
                  <div class="slide-nav--slide slide-nav--image-slide slick-slide slick-current slick-active" style="width: 249px;" data-slick-index="0" aria-hidden="false">
                     <img src="https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/media-slider/specialized-training_image_bmw_6_slider.jpg?sfvrsn=3ad01413_2" alt="specialized-training_image_bmw_6_slider">
                  </div>
                  <div class="slide-nav--slide slide-nav--image-slide slick-slide slick-active" style="width: 249px;" data-slick-index="1" aria-hidden="false">
                     <img src="https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/media-slider/specialized-training_image_bmw_7_slider.jpg?sfvrsn=350614a8_2" alt="specialized-training_image_bmw_7_slider">
                  </div>
                  <div class="slide-nav--slide slide-nav--image-slide slick-slide slick-active" style="width: 249px;" data-slick-index="2" aria-hidden="false">
                     <img src="https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/media-slider/specialized-training_image_bmw_5_slider.jpg?sfvrsn=9d85ae8a_2" alt="specialized-training_image_bmw_5_slider">
                  </div>
                  <div class="slide-nav--slide slide-nav--image-slide slick-slide" style="width: 249px;" data-slick-index="3" aria-hidden="true">
                     <img src="https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/media-slider/specialized-training_image_bmw_1_slider.jpg?sfvrsn=77db85b7_2" alt="specialized-training_image_bmw_1_slider">
                  </div>
                  <div class="slide-nav--slide slide-nav--image-slide slick-slide" style="width: 249px;" data-slick-index="4" aria-hidden="true">
                     <img src="https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/media-slider/specialized-training_image_bmw_9_slider.jpg?sfvrsn=cc808701_2" alt="specialized-training_image_bmw_9_slider">
                  </div>
               </div>
            </div>
            <button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Previous Thumbnail Slide" style="" aria-disabled="false"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
         </div>
      </div>
   </article>
   <section class=" description widget  to-bottom-right background-gradient-red" id="descrrcc7uc" aria-labelledby="descrrcc7uc-header">
      <div class="description-inner row column">
         <div class="row column">
            <div class="widget-header text-center" id="descrrcc7uc-header">
               <h2>CREDENTIALS</h2>
               <p>Graduates of the BMW STEP program enter their careers as Level 3 BMW technicians and earn 8 factory credentials, placing them ahead of other BMW entry-level technicians.</p>
            </div>
         </div>
         <div class="description-content">
            <div class="no-media text-center">
               <div class="cta-container">
               </div>
            </div>
         </div>
      </div>
   </section>
   <section id="igye8sl3" class=" widget  background-white image-grid" aria-labelledby="igye8sl3-header">
      <div class="row align-center">
         <div class="small-12 large-10 columns text-center">
            <div class="widget-header" id="igye8sl3-header">
               <h2>SEE WHAT YOU'LL WORK ON</h2>
               <p>We've spent over two decades in partnership with this famed German automaker. STEP gives you the in-depth knowledge to diagnose and repair engine technologies like direct high-pressure fuel injection, Vanos and Valvetronic, alongside other BMW-approved service procedures.</p>
            </div>
         </div>
      </div>
      <div class="row column">
         <div class="image-grid--container isotope" style="position: relative; height: 441.95px;">
            <div class="grid-sizer"></div>
            <div class="gutter-sizer"></div>
            <div class="image-grid--item " style="position: absolute; left: 0%; top: 0px;">
               <a class="show-for-medium" href="https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/specialized-training_imagegrid_bmw_1.jpg?sfvrsn=970d86c6_6" title=" " target="_blank">
               <img src="https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/specialized-training_imagegrid_bmw_1.jpg?sfvrsn=970d86c6_6" alt="Black BMW car driving in the city freeway ">
               </a>
               <div class="slick-slide-content">
                  <div class="image-grid--image-container">
                     <img src="https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/specialized-training_imagegrid_bmw_1.jpg?sfvrsn=970d86c6_6" alt="Black BMW car driving in the city freeway ">
                  </div>
               </div>
            </div>
            <div class="image-grid--item tall" style="position: absolute; left: 50.8974%; top: 0px;">
               <a class="show-for-medium" href="https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/specialized-training_imagegrid_bmw_2.jpg?sfvrsn=89e73da4_6" title=" " target="_blank">
               <img src="https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/specialized-training_imagegrid_bmw_2.jpg?sfvrsn=89e73da4_6" alt="Red and grey BMW car in the BMW lab">
               </a>
               <div class="slick-slide-content">
                  <div class="image-grid--image-container">
                     <img src="https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/specialized-training_imagegrid_bmw_2.jpg?sfvrsn=89e73da4_6" alt="Red and grey BMW car in the BMW lab">
                  </div>
               </div>
            </div>
            <div class="image-grid--item tall" style="position: absolute; left: 76.48%; top: 0px;">
               <a class="show-for-medium" href="https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/specialized-training_imagegrid_bmw_4.jpg?sfvrsn=695f1d16_6" title=" " target="_blank">
               <img src="https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/specialized-training_imagegrid_bmw_4.jpg?sfvrsn=695f1d16_6" alt="Action shot of a white BMW driving along the oceanside">
               </a>
               <div class="slick-slide-content">
                  <div class="image-grid--image-container">
                     <img src="https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/specialized-training_imagegrid_bmw_4.jpg?sfvrsn=695f1d16_6" alt="Action shot of a white BMW driving along the oceanside">
                  </div>
               </div>
            </div>
            <div class="image-grid--item " style="position: absolute; left: 50.8974%; top: 149px;">
               <a class="show-for-medium" href="https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/specialized-training_imagegrid_bmw_3.jpg?sfvrsn=5086ebd6_6" title=" " target="_blank">
               <img src="https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/specialized-training_imagegrid_bmw_3.jpg?sfvrsn=5086ebd6_6" alt="Action shot of white BMW driving along the highway">
               </a>
               <div class="slick-slide-content">
                  <div class="image-grid--image-container">
                     <img src="https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/specialized-training_imagegrid_bmw_3.jpg?sfvrsn=5086ebd6_6" alt="Action shot of white BMW driving along the highway">
                  </div>
               </div>
            </div>
            <div class="image-grid--item tall" style="position: absolute; left: 0%; top: 292px;">
               <a class="show-for-medium" href="https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/specialized-training_image_bmw_8.jpg?sfvrsn=87aaf0b7_6" title="<h2>Vehicle 5</h2> " target="_blank">
               <img src="https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/specialized-training_image_bmw_8.jpg?sfvrsn=87aaf0b7_6" alt="UTI student looking under the hood in the BMW lab">
               </a>
               <div class="slick-slide-content">
                  <div class="image-grid--image-container">
                     <img src="https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/specialized-training_image_bmw_8.jpg?sfvrsn=87aaf0b7_6" alt="UTI student looking under the hood in the BMW lab">
                  </div>
                  <h2>Vehicle 5</h2>
               </div>
            </div>
            <div class="image-grid--item tall" style="position: absolute; left: 25.4487%; top: 292px;">
               <a class="show-for-medium" href="https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/specialized-training_image_bmw_4.jpg?sfvrsn=1ec9ffb1_6" title=" " target="_blank">
               <img src="https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/specialized-training_image_bmw_4.jpg?sfvrsn=1ec9ffb1_6" alt="UTI students looking at computer screen in the BMW lab">
               </a>
               <div class="slick-slide-content">
                  <div class="image-grid--image-container">
                     <img src="https://utiedu.azureedge.net/images/default-source/programs/specialized-training/bmw/specialized-training_image_bmw_4.jpg?sfvrsn=1ec9ffb1_6" alt="UTI students looking at computer screen in the BMW lab">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="row column text-center">
         <a class="button rtl" href="/why-uti/equipment-technology/">Facilities, Equipment &amp; Technology</a>
      </div>
   </section>
   <div class="widget  background-white sf-border" aria-hidden="true"></div>
</div>
<?php include('../../../../views/footer.php'); ?>
