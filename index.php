<?php include('views/header.php'); ?>
<?php include('views/navigation.php'); ?>
<div id="Contentplaceholder1_TCC38B385001_Col00" class="sf_colsIn container" data-sf-element="Container" data-placeholder-label="Container">
   <header class="widget hero  video-hero hero--dark-image  text-center " id="hero2zqi5o" style="background-image: url('https://utiedu.azureedge.net/images/default-source/homepage---mockup-photos/home_hero_motor.jpg?sfvrsn=b89b2e3b_6');">
      <div class="overlay" aria-hidden="true"></div>
      <div class="video-background" data-youtube-id="519AAYT69e8">
         <div class="video-iframe"></div>
      </div>
      <div class="row align-center-middle">
         <div class="small-11 medium-10 large-9 columns">
            <div class="hero-content">
               <h1>
                  UNIVERSAL TECHNICAL INSTITUTE
                  <span>TECHNICIAN TRAINING SCHOOLS FOR THE SKILLED TRADES</span>
               </h1>
               <p>You never do things the traditional way. Why should school be any different? Realize your dreams. We'll help you Get There. Faster. Smarter.</p>
               <a href="#" class="launch-video" data-open="heroVideo-hero2zqi5o"><span class="show-for-sr">Play the video</span><i class="fa fa-play" aria-hidden="true"></i></a>
               <div class="button-area">
               </div>
            </div>
         </div>
      </div>
      <a href="#" class="scroll-to-next"><i class="fa fa-chevron-down" aria-hidden="true"></i><span class="show-for-sr">Scroll to next section</span></a>
   </header>
   <div class="large reveal video-modal" id="heroVideo-hero2zqi5o" data-reveal data-reset-on-close="true">
      <div class="flex-video widescreen">
         <iframe width="420" height="315" src="//www.youtube-nocookie.com/embed/G5JFYB_qZOA?color=null&modestbranding=1&rel=0" title="hero background youtube video for youtube ID G5JFYB_qZOA" frameborder="0" allowfullscreen></iframe>
      </div>
      <button class="close-button" data-close aria-label="Close reveal" type="button">
      <span aria-hidden="true">&times;</span>
      </button>
   </div>
   <div class="sticky-anchor"></div>
   <div class="anchor-links" role="navigation">
      <div class="row column">
         <ul class="menu expanded show-for-medium" data-magellan>
            <li>
               <a href="/programs/automotive">Automotive</a>
            </li>
            <li>
               <a href="/programs/diesel">Diesel</a>
            </li>
            <li>
               <a href="/programs/collision">Collision Repair</a>
            </li>
            <li>
               <a href="/programs/motorcycle">Motorcycle</a>
            </li>
            <li>
               <a href="/programs/marine">Marine</a>
            </li>
            <li>
               <a href="/programs/welding">Welding</a>
            </li>
            <li>
               <a href="/programs/cnc-machining">CNC Machining</a>
            </li>
         </ul>
         <ul class="vertical menu accordion-menu show-for-small-only" data-accordion-menu>
            <li>
               <a href="#" id="anchorTitle">Overview</a>
               <ul class="menu vertical nested">
                  <li>
                     <a href="/programs/automotive">Automotive</a>
                  </li>
                  <li>
                     <a href="/programs/diesel">Diesel</a>
                  </li>
                  <li>
                     <a href="/programs/collision">Collision Repair</a>
                  </li>
                  <li>
                     <a href="/programs/motorcycle">Motorcycle</a>
                  </li>
                  <li>
                     <a href="/programs/marine">Marine</a>
                  </li>
                  <li>
                     <a href="/programs/welding">Welding</a>
                  </li>
                  <li>
                     <a href="/programs/cnc-machining">CNC Machining</a>
                  </li>
               </ul>
            </li>
         </ul>
      </div>
   </div>
   <div  id="wwcn0dxq4a" class="description widget  to-bottom-right background-gradient-blue less-padding">
      <div class="description-inner row column">
         <div class="description-content">
            <style>@media screen and (max-width: 640px) {
               .wysiwyg-logo {
               margin-top:10px;
               padding-bottom:10px;
               }
               }
            </style>
            <div class="row align-center-middle">
               <div class="small-12  medium-12 large-8 columns">
                  <div class="row align-center-middle">
                     <div class="small-5 medium-3 columns"><a class="wysiwyg-logo" href="/schools/universal-technical-institute" sfref="[f669d9a7-009d-4d83-ddaa-000000000002]aad237ed-4d40-47ed-8955-7edcb926decd"><span class="sf-Image-wrapper" data-sfref="[images|OpenAccessDataProvider]8d3aae43-76ba-45a7-9904-26078216592d"><img alt="Universal Technical Institute" data-customsizemethodproperties="{'MaxWidth':179,'MaxHeight':84,'Width':null,'Height':null,'ScaleUp':false,'Quality':'High','Method':'ResizeFitToAreaArguments'}" data-displaymode="Custom" data-method="ResizeFitToAreaArguments" sfref="[images|OpenAccessDataProvider]8d3aae43-76ba-45a7-9904-26078216592d" src="https://uti.azureedge.net/images/default-source/global/logos/logo-lineup/uti_logo_rev_375x180.png?sfvrsn=ef3adac8_4&amp;MaxWidth=179&amp;MaxHeight=84&amp;ScaleUp=false&amp;Quality=High&amp;Method=ResizeFitToAreaArguments&amp;Signature=D98B5E58040CEE66CB860F2D1D11481D372A81BF" title="UNIVERSAL TECHNICAL INSTITUTE" /></span></a>
                     </div>
                     <div class="small-5 medium-3 columns"><a class="wysiwyg-logo" href="/schools/motorcycle-mechanics-institute" sfref="[f669d9a7-009d-4d83-ddaa-000000000002]b5c63d01-1521-4bc7-b48e-fda4e552cd87"><span class="sf-Image-wrapper" data-sfref="[images|OpenAccessDataProvider]c0bdd6b7-cd8b-473a-85ed-991ffd80db49"><img alt="Motorcycle Mechanics Institute" data-customsizemethodproperties="{'MaxWidth':190,'MaxHeight':86,'Width':null,'Height':null,'ScaleUp':false,'Quality':'High','Method':'ResizeFitToAreaArguments'}" data-displaymode="Custom" data-method="ResizeFitToAreaArguments" sfref="[images|OpenAccessDataProvider]c0bdd6b7-cd8b-473a-85ed-991ffd80db49" src="https://uti.azureedge.net/images/default-source/global/logos/logo-lineup/mmi_moto_rev.png?sfvrsn=a2204f53_4&amp;MaxWidth=190&amp;MaxHeight=86&amp;ScaleUp=false&amp;Quality=High&amp;Method=ResizeFitToAreaArguments&amp;Signature=CEFCC1C4668762F4A751531F55420C7882FC717A" title="MOTORCYCLE MECHANICS INSTITUTE" /></span></a>
                     </div>
                     <div class="small-5 medium-3 columns"><a class="wysiwyg-logo" href="/schools/marine-mechanics-institute" sfref="[f669d9a7-009d-4d83-ddaa-000000000002]7ad153ad-5599-4557-80b9-2d8c11c52033"><span class="sf-Image-wrapper" data-sfref="[images|OpenAccessDataProvider]b77f802a-b890-4e6c-ab38-3bfd0648a14d"><img alt="Marine Mechanics Institute" data-customsizemethodproperties="{'MaxWidth':190,'MaxHeight':86,'Width':null,'Height':null,'ScaleUp':false,'Quality':'High','Method':'ResizeFitToAreaArguments'}" data-displaymode="Custom" data-method="ResizeFitToAreaArguments" sfref="[images|OpenAccessDataProvider]b77f802a-b890-4e6c-ab38-3bfd0648a14d" src="https://uti.azureedge.net/images/default-source/global/logos/logo-lineup/mmi_marine_rev.png?sfvrsn=5d41f4ec_4&amp;MaxWidth=190&amp;MaxHeight=86&amp;ScaleUp=false&amp;Quality=High&amp;Method=ResizeFitToAreaArguments&amp;Signature=8F56B96F74CD9001CB26A85321A2932C14776465" title="MARINE MECHANICS INSTITUTE" /></span></a>
                     </div>
                     <div class="small-5 medium-3 columns"><a class="wysiwyg-logo" href="/schools/nascar-technical-institute" sfref="[f669d9a7-009d-4d83-ddaa-000000000002]a661ad88-95b9-4e74-9c66-a4394e273bc7"><span class="sf-Image-wrapper" data-sfref="[images|OpenAccessDataProvider]7f8fc925-3310-4c7c-a572-136688944483"><img alt="Nascar Technical Institute" data-customsizemethodproperties="{'MaxWidth':200,'MaxHeight':95,'Width':null,'Height':null,'ScaleUp':false,'Quality':'High','Method':'ResizeFitToAreaArguments'}" data-displaymode="Custom" data-method="ResizeFitToAreaArguments" sfref="[images|OpenAccessDataProvider]7f8fc925-3310-4c7c-a572-136688944483" src="https://uti.azureedge.net/images/default-source/global/logos/logo-lineup/nascar_tech_rev.png?sfvrsn=ddd300ec_8&amp;MaxWidth=200&amp;MaxHeight=95&amp;ScaleUp=false&amp;Quality=High&amp;Method=ResizeFitToAreaArguments&amp;Signature=023E14E86367353A9226420787979604203B4407" title="Nascar Technical Institute" /></span></a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div style="background-image: url('https://utiedu.azureedge.net/images/default-source/global/background-assets/background-callout-metal.jpg?sfvrsn=94b818dc_0');" class="widget background-image  background-white sf-border" aria-hidden="true"></div>
   <div style="background-image: url('https://utiedu.azureedge.net/images/default-source/global/background-assets/rough_background_logolineup_v2.jpg?sfvrsn=28a04711_10');" id="wwcn4t9prb" class="description widget background-image  less-padding">
      <div class="description-inner row column">
         <div class="description-content"></div>
      </div>
   </div>
   <div id="fwb23amz1ic" class="widget fw-blocks-container">
      <div class="row small-up-1 medium-up-3 align-center-middle">
         <div data-src=https://utiedu.azureedge.net/images/default-source/homepage---mockup-photos/home_blocks_campuses-opt.jpg?sfvrsn=e93a00ae_0 class="column column-block background-image " >
            <a href="/schools/" class="info-overlay">
               <div class="info-content">
                  <h3>SCHOOLS</h3>
               </div>
            </a>
         </div>
         <div data-src=https://utiedu.azureedge.net/images/default-source/homepage---mockup-photos/home_blocks_programs40790a7e9ff24a6aabe5c0abbbf8a7a7.gif?sfvrsn=59267985_0 class="column column-block background-image " >
            <a href="/programs/" class="info-overlay">
               <div class="info-content">
                  <h3>PROGRAMS</h3>
               </div>
            </a>
         </div>
         <div data-src=https://utiedu.azureedge.net/images/default-source/homepage---mockup-photos/home_blocks_whytrainhere-opt.jpg?sfvrsn=e1ae59ea_0 class="column column-block background-image " >
            <a href="/why-uti/" class="info-overlay">
               <div class="info-content">
                  <h3>WHY TRAIN HERE</h3>
               </div>
            </a>
         </div>
         <div data-src=https://utiedu.azureedge.net/images/default-source/homepage---mockup-photos/home_locations-opt.jpg?sfvrsn=ec2f8299_0 class="column column-block background-image " >
            <a href="/locations/" class="info-overlay">
               <div class="info-content">
                  <h3>LOCATIONS</h3>
               </div>
            </a>
         </div>
         <div data-src=https://utiedu.azureedge.net/images/default-source/homepage---mockup-photos/home_blocks_admissions-opt.jpg?sfvrsn=2770ac88_0 class="column column-block background-image " >
            <a href="/admissions/" class="info-overlay">
               <div class="info-content">
                  <h3>ADMISSIONS</h3>
               </div>
            </a>
         </div>
         <div data-src=https://utiedu.azureedge.net/images/default-source/global/background-assets/background-callout-metal.jpg?sfvrsn=94b818dc_0 class="column column-block  has-screws background-image " >
            <div class="info-overlay">
               <div class="info-content center">
                  <h3>GET STARTED</h3>
                  <p>GET THERE. FASTER. SMARTER.<BR>Our dedicated team is here to help.</p>
                  <a class="button-special rtl" href="/request-info/">Request Info</a>
               </div>
            </div>
            <div class="screws-top" aria-hidden="true"></div>
            <div class="screws-bottom" aria-hidden="true"></div>
         </div>
      </div>
   </div>
   <div style="background-image: url('https://utiedu.azureedge.net/images/default-source/global/background-assets/rough_background_logolineup_v2.jpg?sfvrsn=28a04711_10');" id="wwcnzsbwoy" class="description widget background-image  less-padding">
      <div class="description-inner row column">
         <div class="description-content"></div>
      </div>
   </div>
   <div style="background-image: url('https://utiedu.azureedge.net/images/default-source/global/background-assets/background-callout-metal.jpg?sfvrsn=94b818dc_0');" class="widget background-image  background-white sf-border" aria-hidden="true"></div>
   <section  class=" numbers-facts widget  to-top background-gradient-red" id="stble61ep9" aria-labelledby="stble61ep9-header">
      <div class="numbers-facts-inner">
         <div class="row align-center">
            <div class="small-11 large-12 columns">
               <div class="numbers-facts-stats row small-up-1 medium-up-2 large-up-4 align-top align-center-middle">
                  <div class="column">
                     <div class="numbers-facts-stat has-accent">
                        <span class="number">12 campuses</span>
                        <span class="stat-text">
                        Coast to coast
                        </span>
                        <p>Choose from a range of programs you need to get the technical career you want.</p>
                     </div>
                  </div>
                  <div class="column">
                     <div class="numbers-facts-stat has-accent">
                        <span class="number">51 weeks</span>
                        <span class="stat-text">
                        From start to finish
                        <sup data-anchor-id="footer-disclaimer">4</sup>
                        </span>
                        <p>Get the training you need to start a rewarding career as an auto technician in just about a year.</p>
                     </div>
                  </div>
                  <div class="column">
                     <div class="numbers-facts-stat has-accent">
                        <span class="number">4 out of 5</span>
                        <span class="stat-text">
                        Graduate employment rate
                        <sup data-anchor-id="footer-disclaimer">3</sup>
                        </span>
                        <p>4 out of 5 UTI graduates find employment in careers within their field of study within 1 year of graduation.</p>
                     </div>
                  </div>
                  <div class="column">
                     <div class="numbers-facts-stat has-accent">
                        <span class="number">30+ brands</span>
                        <span class="stat-text">
                        Relationships with manufacturers
                        </span>
                        <p>UTI has relationships with manufacturers of over 30 leading brands.</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <div style="background-image: url('https://utiedu.azureedge.net/images/default-source/global/background-assets/background-callout-metal.jpg?sfvrsn=94b818dc_0');" class="widget background-image  background-white sf-border" aria-hidden="true"></div>
   <section id="iiwtpci096"  class="widget  image-icon-with-text" aria-labelledby="iiwtpci096-header">
      <div class="row image-icon align-center text-center">
         <div class="item text-center small-12 medium-4 columns">
            <div class="item-content">
               <div class="item-img-container">
                  <img src="https://utiedu.azureedge.net/images/default-source/global/icons/small-icons/schedule_tour.png?sfvrsn=22b7327b_2" alt="Schedule_Tour" />
               </div>
               <h3>SCHEDULE A TOUR</h3>
               <p>Come take a look at our hands-on labs with state-of-the-industry equipment and technology.</p>
               <a href="/locations/campus-tours/" class=" btt">Schedule Tour</a>
            </div>
         </div>
         <div class="item text-center small-12 medium-4 columns">
            <div class="item-content">
               <div class="item-img-container">
                  <img src="https://utiedu.azureedge.net/images/default-source/global/icons/small-icons/calendar.png?sfvrsn=8f962196_2" alt="Calendar" />
               </div>
               <h3>START DATE CALENDAR</h3>
               <p>Contact an Admissions Representative to begin the enrollment process.  Classes start on average every 3-6 weeks.</p>
               <a href="/docs/default-source/collateral/calendars-uti-mmi-nascar-tech.pdf" class=" btt">Download Calendar</a>
            </div>
         </div>
         <div class="item text-center small-12 medium-4 columns">
            <div class="item-content">
               <div class="item-img-container">
                  <img src="https://utiedu.azureedge.net/images/default-source/global/icons/small-icons/specialized_training.png?sfvrsn=eae47d99_2" alt="Specialized_Training" />
               </div>
               <h3>SPECIALIZED TRAINING</h3>
               <p>See how manufacturer-specific training can help increase opportunities to work on leading industry brands.</p>
               <a href="/why-uti/industry-partnerships/original-equipment-manufacturers" class=" btt">Learn More</a>
            </div>
         </div>
      </div>
   </section>
   <div  class="widget  background-white sf-border" aria-hidden="true"></div>
</div>
<?php include('views/footer.php'); ?>
