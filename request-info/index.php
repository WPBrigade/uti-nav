<!DOCTYPE html> 
<html lang="en">
   <head>
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta charset="utf-8" />
      <title>
         Request Information | Universal Technical Institute
      </title>
      <link type="text/css" rel="stylesheet" href="/ResourcePackages/Foundation/assets/dist/css/app.min.css" />
      <link rel="stylesheet" type="text/css" href="//cloud.typography.com/7416432/788802/css/fonts.css" />
      <!-- Google Tag Manager --> <script>
         (function (w, d, s, l, i) {
         w[l] = w[l] || []; w[l].push({
         'gtm.start':
         new Date().getTime(), event: 'gtm.js'
         }); var f = d.getElementsByTagName(s)[0],
         j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
         'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
         })(window, document, 'script', 'dataLayer', 'GTM-5BXBPH');
      </script> <!-- End Google Tag Manager -->  <script type="text/javascript">var sf_appPath='/';</script>
      <style type="text/css" media="all">h1 {
         color: #003852;
         font-size: 30px;
         font-weight: 600;
         letter-spacing: .0125em;
         line-height: 89%;
         margin-top: 0;
         margin-bottom: 25px;
         padding-top: 0;
         text-transform: uppercase;    
         }
         h5 {
         color: #003852;
         font-size: 28px;
         font-weight: 300;
         line-height: 1.1;
         margin: 0 20px 20px 0;    
         }
         select { 
         width: 100%; 
         padding-top: 0;
         padding-bottom: 0;
         }
         #introWrapper {
         margin-top: 40px;
         }
         #intro,
         #aoiWrapper {
         padding: 0 5%;
         }
         #aoiWrapper {
         margin-top: 20px;
         }
         .progress-bar-container {
         text-align: center;
         }
         .progress-bar-container .wide-num-stripe {
         border-bottom: 8px solid #ccc;
         display: inline-block;
         width: 32%;
         }
         .progress-bar-container .num-circle {
         position: absolute;
         color: #fff;
         background-color: #ccc;
         width: 30px;
         height: 30px;
         text-align: center;
         border-radius: 100%;
         margin-left: -12px;
         line-height: 30px;
         margin-top: -3px
         }
         .progress-bar-container .blue-progress {
         border-color: #0086d2;
         background-color: #0086d2;
         }
         .programDescriptionWrapper p {
         margin: 10px 0;
         font-size: 12px;
         }
         /* Foundation override */
         li.accordion-item {
         margin-top: 12px;
         margin-bottom: 12px;
         background-color: #f6f6fb;
         }
         /* Foundation override because of accordion-item margins */
         .accordion-title {
         border-bottom: 1px solid #e6e6e6;
         }
         .accordion-title {
         color: #a70e13;
         }
         .accordion-content {
         background-color: #f6f6fb;
         font-size: 13px;
         }
         #backBtnWrapper {
         margin-bottom: 10px;
         font-size: 12px;
         }
         #desktopWrapper {
         margin-bottom: 40px;
         background-color: #f6f6fb;
         border: 1px solid #a6a6a6;
         }
         #desktopWrapper a:hover {
         color: #ccc;
         }
         #programListWrapper {
         background-color: #f6f6fb;
         border-right: 1px solid #a6a6a6;
         }
         #programList {
         margin-left: 0;
         list-style-type: none;
         margin-bottom: 0;
         }
         #programList li {
         border-bottom: 1px solid #a6a6a6;
         }
         #programList li:hover, #programList li.open {
         background-color: #0086d2;
         }
         #programList li:last-child {
         border-bottom: none;
         }
         #programList li:hover .program-title, #programList li.open .program-title {
         color: #fff;
         }
         #programList li h5 {
         color: #a70e13;
         cursor: pointer;
         display: inline-block;
         font-weight: 500;
         font-size: 22px;
         margin: 0;
         padding: 8px 15px;
         text-transform: none;
         vertical-align: top;
         }
         #programList li input[type="radio"] {
         width: 18px;
         margin: 15px 20px 0 0;
         }
         #programInfoWrapper {
         padding: 0;
         height: 100%;
         vertical-align: middle;
         }
         #programImageWrapper {
         position: relative;
         max-height: 368px;
         }
         #programImageWrapper:after {
         padding-top: 87%; /*this specifies the aspect ratio*/
         display: block;
         content: '';
         }
         #programImage {
         position: absolute;
         top: 0; bottom: 0; right: 0; left: 0; /*fill parent*/
         background:     url(https://www.uti.edu/images/default-source/request-info/gif_introimage.jpg) center center;
         background-size: cover;
         }
         .programDescriptionWrapper {
         padding: 12px 10px 0 10px;
         font-size: 11px;
         }
         .program-title {
         font-size: 16px;
         }
         .next-button {
         font-size: 16px;
         display: inline-block;
         text-align: center;
         color: #fff;
         width: 100%;
         height: 40px;
         line-height: 40px;
         box-shadow: inset 0 -2px 0 rgba(0,0,0,.15)!important;
         border-radius: 5px;
         background-color: #d50019!important;
         border: 1px solid #a61d22!important;
         border-bottom: 2px solid #a61d22!important;
         padding: 0;
         cursor: pointer;
         }
         #personalInfo {
         /* margin: 0 5% 10% 5%; */
         margin-bottom: 50px;
         border: solid 2px #efefef;
         padding: 25px 0;
         background-color: #f2f5f7; 
         }
         #personalInfo label {
         font-size: 14px;
         font-weight: 700;
         font-family: "Open Sans","Trebuchet",Arial,sans-serif;
         line-height: 150%;
         }
         #getInfoBtn { 
         color: #fff;
         background-color: #d50019;
         border: 1px solid #a61d22;
         /*font-family: 'Source Sans Pro',sans-serif;
         font-weight: 700;
         font-size: 16px;*/
         letter-spacing: .02em;
         margin: 50px 0 15px 0;
         text-shadow: none;
         text-transform: uppercase;
         /*width: 100%;*/
         -webkit-font-smoothing: antialiased;
         -webkit-transition: all 200ms ease-out;
         -o-transition: all 200ms ease-out;
         -moz-transition: all 200ms ease-out;
         -webkit-border-radius: initial;
         -webkit-box-shadow: initial;
         -moz-box-shadow: initial;
         }
         .tcpa {
         font-size: 12px;
         }
         @media(min-width: 640px)  {
         .ie::after { 
         padding-bottom: 21px;
         }
         #header {
         padding: 20px 0;
         background-color: #003852;
         }
         #header h1 {
         color: #fff;
         font-size: 50px;
         }
         #intro, #aoiWrapper {
         padding: 0;
         }
         #intro {
         padding-top: 10px;
         margin: 0;
         }
         #intro h1 {
         font-size: 48px;
         }
         #intro h5 {
         font-size: 34px;
         }
         .lead-in {
         font-size: 18px;
         }
         #milStatusWrapper fieldset {
         margin-left: .8333333333rem;
         }
         #milStatusWrapper legend {
         font-size: .7777777778rem;
         }
         #milStatusWrapper fieldset {
         max-width: 40%;
         }
         #milReleaseDateWrapper > label {
         margin-top: 20px;
         }
         }
         /*Navbar Overrides*/
         /*------------------------------------------------*/
         /*Sets the height of the div for small screens*/
         .navbar-background-div {
         height: 72px;
         background: #003852;
         }
         /*changes background div height to 90px on med screens*/
         @media print, screen and (min-width: 40em){
         .navbar-background-div {
         height: 90px;
         }
         }
         /*------------------------------------------------*/
         /*Button Overrides*/
         /*------------------------------------------------*/
         .button-special, .button {
         padding: 0;
         background-color: #d50019;
         border: 1px solid #a61d22;
         margin-bottom: 0;
         line-height: 1.4em;
         }
         .button-special:hover, .button:hover {
         background-color: #d50019!important;
         }
         .button-special:after {
         content: "\f054";
         font-family: fontawesome;
         font-size: .75em;
         margin-left: .5em;
         display: inline-block;
         line-height: 2.4rem;
         vertical-align: bottom;
         }
         /*Desktop*/
         @media print, screen and (min-width: 64em) {
         .button-special:hover, .button:hover {
         color: #d50019!important;
         background-color: #d50019!important;
         }
         .button-special:before, .button:before {
         background-image: linear-gradient(to bottom, #fff, #fff);
         }
         }
         /*------------------------------------------------*/
      </style>
      <meta name="Generator" content="Sitefinity 10.0.6400.0 PU" />
      <link rel="canonical" href="https://www.uti.edu/request-info" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <script type="text/javascript">
         (function() {var _rdDeviceWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;var _rdDeviceHeight = (window.innerHeight > 0) ? window.innerHeight : screen.height;var _rdOrientation = (window.width > window.height) ? 'landscape' : 'portrait';})();
      </script>
   </head>
   <body role="main">
      <!-- Google Tag Manager (noscript) --> 
      <noscript> <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5BXBPH"
         height="0" width="0" style="display:none;visibility:hidden" title="google tag manager"></iframe> </noscript>
      <!-- End Google Tag Manager (noscript) -->  
      <div class="sfPublicWrapper" id="PublicWrapper">
         <div >
            <div >
               <div class="navbar-background-div"></div>
            </div>
         </div>
         <div  class="widget  background-gray-light sf-border" aria-hidden="true"></div>
         <div >
            <div >
               <!--Intro-->
               <div class="row" id="introWrapper">
                  <div class="columns small-12" id="intro">
                     <div id="backBtnWrapper"><a id="backBtn" style="display:none;">
                        <span aria-hidden="true" class="glyphicon glyphicon-menu-left"></span>Back to Programs
                        </a>
                     </div>
                     <h1 class="intro-header text-center">A NEW CAREER PATH STARTS WITH UNIVERSAL TECHNICAL INSTITUTE</h1>
                     <div class="progress-bar-container hide-for-medium"><span class="wide-num-stripe one blue-progress"></span><span class="num-circle blue-progress">1</span>
                        <span class="wide-num-stripe two"></span><span class="num-circle two">2</span>
                        <span class="wide-num-stripe three"></span>
                     </div>
                     <p class="lead-in text-center">Take the next step, explore and choose a program by clicking next in a section below.</p>
                  </div>
               </div>
               <!--End Intro -->
            </div>
         </div>
         <div >
            <div >
               <!--AOI Selection-->
               <div id="aoiWrapper">
                  <!--Mobile Accordion-->
                  <div class="row hide-for-medium">
                     <div class="small-12 columns">
                        <!--Mobile Accordion-->
                        <ul class="accordion" data-accordion="" data-allow-all-closed="true" id="accordion">
                           <!--Auto-->
                           <li class="accordion-item" data-accordion-item="">
                              <a class="accordion-title" href="#">Automotive</a>
                              <div class="accordion-content" data-tab-content="">
                                 <p>In this program, you'll learn how to diagnose, maintain and repair domestic and foreign automobiles.
                                    You will find out how to troubleshoot problems of all kinds, using the state-of-the-industry
                                    engine analyzers, handheld scanners and other computerized diagnostic equipment. You'll learn
                                    everything from basic engine systems to computerized fuel injection, anti-lock brakes, passenger
                                    restraint systems, computerized engine controls and much more.
                                 </p>
                                 <p>Simply answer a few questions below to have one of our Admissions Representatives contact you about
                                    technician training opportunities.
                                 </p>
                                 <p>It only takes a minute.</p>
                                 <a class="button-special ltr" name="NextAutomotiveBtn">Next</a>
                              </div>
                           </li>
                           <!--Collision-->
                           <li class="accordion-item" data-accordion-item="">
                              <a class="accordion-title" href="#">Collision Repair &amp; Refinish</a>
                              <div class="accordion-content" data-tab-content="">
                                 <p>If you have the passion to reshape and restore a wide range of vehicles, you&rsquo;re ready for UTI&rsquo;s
                                    Collision Repair &amp; Refinish Technology program. UTI&rsquo;s ASE-certified instructors and
                                    curriculum, developed in cooperation with I-CAR, will teach you the hands-on skills needed to
                                    pursue a career as a collision repair technician.
                                 </p>
                                 <p>Simply answer a few questions below to have one of our Admissions Representatives contact you about
                                    technician training opportunities.
                                 </p>
                                 <p>It only takes a minute.</p>
                                 <a class="button-special ltr" name="NextCollisionBtn">Next</a>
                              </div>
                           </li>
                           <!--Diesel-->
                           <li class="accordion-item" data-accordion-item="">
                              <a class="accordion-title" href="#">Diesel</a>
                              <div class="accordion-content" data-tab-content="">
                                 <p>UTI&rsquo;s Diesel Technology program gives you hands-on training that prepares you for an in-demand
                                    career. You&rsquo;ll work on diesel engines, commercial vehicles and heavy-equipment systems
                                    that feature advanced computer controls and electronic functions.
                                 </p>
                                 <p>Simply answer a few questions below to have one of our Admissions Representatives contact you about
                                    technician training opportunities.
                                 </p>
                                 <p>It only takes a minute.</p>
                                 <a class="button-special ltr" name="NextDieselBtn">Next</a>
                              </div>
                           </li>
                           <!--Marine-->
                           <li class="accordion-item" data-accordion-item="">
                              <a class="accordion-title" href="#">Marine</a>
                              <div class="accordion-content" data-tab-content="">
                                 <p>If you love the water and want to pursue a career in the marine industry, you belong at Marine Mechanics
                                    Institute. Learn to diagnose, service and repair marine mechanical systems through a combination
                                    of hands-on work in the lab and classroom instruction utilizing state-of-the-industry technology
                                    and equipment. Work with leading marine brands on inboard gas and diesel engines, outboard 2-stroke
                                    and 4-stroke motors, and sterndrives.
                                 </p>
                                 <p>Simply answer a few questions below to have one of our Admissions Representatives contact you about
                                    technician training opportunities.
                                 </p>
                                 <p>It only takes a minute.</p>
                                 <a class="button-special ltr" name="NextMarineBtn">Next</a>
                              </div>
                           </li>
                           <!--Moto-->
                           <li class="accordion-item" data-accordion-item="">
                              <a class="accordion-title" href="#">Motorcycle</a>
                              <div class="accordion-content" data-tab-content="">
                                 <p>Street bikes, sport bikes, dirt bikes&mdash;Motorcycle Mechanics Institute (MMI) has them all. You
                                    will learn motorcycle theory, engine troubleshooting and diagnosis, drivability, and performance
                                    testing on leading brands in the industry. MMI&rsquo;s training curriculum is designed to meet
                                    the needs of a changing industry, working with manufacturers to make sure that your training
                                    prepares you for career in the motorcycle industry.
                                 </p>
                                 <p>Simply answer a few questions below to have one of our Admissions Representatives contact you about
                                    technician training opportunities.
                                 </p>
                                 <p>It only takes a minute.</p>
                                 <a class="button-special ltr" name="NextMotorcycleBtn">Next</a>
                              </div>
                           </li>
                           <!--NASCAR-->
                           <li class="accordion-item" data-accordion-item="">
                              <a class="accordion-title" href="#">NASCAR</a>
                              <div class="accordion-content" data-tab-content="">
                                 <p>The Automotive Technology program at NASCAR Tech will teach you how to diagnose, maintain and repair
                                    domestic and foreign automobiles. You'll learn basic engine systems, computerized fuel injection,
                                    anti-lock brakes, passenger restraint systems, computerized engine controls and more. After your
                                    core training, you can add the NASCAR Technology elective where you learn everything from high-performance
                                    engines, fabrication and welding, to aerodynamics and pit crew essentials.
                                 </p>
                                 <p>Simply answer a few questions below to have one of our Admissions Representatives contact you about
                                    technician training opportunities.
                                 </p>
                                 <p>It only takes a minute.</p>
                                 <a class="button-special ltr" name="NextNASCARBtn">Next</a>
                              </div>
                           </li>
                           <!--CNC Machining-->
                           <li class="accordion-item" data-accordion-item="">
                              <a class="accordion-title" href="#">CNC Machining</a>
                              <div class="accordion-content" data-tab-content="">
                                 <p>From mass-produced car parts to precision parts used in aviation, CNC (computer numeric controlled)
                                    machinists can make a variety of products. The CNC Machining Technology program at NASCAR Tech
                                    will teach you a wide range of skills such as programming and operating CNC lathes and mills,
                                    reading blueprints and interpreting geometric dimensioning, and continuous improvement practices
                                    to make you an innovative, effective and efficient machinist.
                                 </p>
                                 <p>Simply answer a few questions below to have one of our Admissions Representatives contact you about
                                    training opportunities.
                                 </p>
                                 <p>It only takes a minute.</p>
                                 <a class="button-special ltr" name="NextCNCBtn">Next</a>
                              </div>
                           </li>
                           <!--Welding-->
                           <li class="accordion-item" data-accordion-item="">
                              <a class="accordion-title" href="#">Welding</a>
                              <div class="accordion-content" data-tab-content="">
                                 <p>All across America, you&rsquo;ll find welders at work in all types of places: shops, bridges, buildings,
                                    pipelines, power plants, refineries and aerospace manufacturers. UTI&rsquo;s Welding Technology
                                    program prepares students for American Welding Society certification with hands-on training developed
                                    in collaboration with Lincoln Electric, a global leader in the welding industry.
                                 </p>
                                 <p>Simply answer a few questions below to have one of our Admissions Representatives contact you about
                                    training opportunities.
                                 </p>
                                 <p>It only takes a minute.</p>
                                 <a class="button-special ltr" name="NextWeldingBtn">Next</a>
                              </div>
                           </li>
                           <!--Undecided-->
                           <li class="accordion-item" data-accordion-item="">
                              <a class="accordion-title" href="#">Undecided</a>
                              <div class="accordion-content" data-tab-content="">
                                 <p>If you&rsquo;re not sure which program you&rsquo;re most interested in, or if you&rsquo;d like information
                                    on more than one program, click &ldquo;next&rdquo; below to answer a few questions and one of
                                    our Admissions Representatives will contact you. They can help you find the program that is right
                                    for you.
                                 </p>
                                 <p>It only takes a minute.</p>
                                 <a class="button-special ltr" name="NextUndecidedBtn">Next</a>
                              </div>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <!--End Mobile Accordion-->
                  <!--Desktop-->
                  <div class="row hide-for-small-only" id="desktopWrapper">
                     <div class="small-12 medium-4" id="programListWrapper">
                        <ul id="programList">
                           <li data-option-num="0">
                              <h5 class="program-title">Automotive</h5>
                              <input class="form-control radio pull-right" type="radio" />
                           </li>
                           <li data-option-num="1">
                              <h5 class="program-title">Collision Repair &amp; Refinish</h5>
                              <input class="form-control radio pull-right" type="radio" />
                           </li>
                           <li data-option-num="2">
                              <h5 class="program-title">Diesel</h5>
                              <input class="form-control radio pull-right" type="radio" />
                           </li>
                           <li data-option-num="3">
                              <h5 class="program-title">Marine</h5>
                              <input class="form-control radio pull-right" type="radio" />
                           </li>
                           <li data-option-num="4">
                              <h5 class="program-title">Motorcycle</h5>
                              <input class="form-control radio pull-right" type="radio" />
                           </li>
                           <li data-option-num="5">
                              <h5 class="program-title">NASCAR</h5>
                              <input class="form-control radio pull-right" type="radio" />
                           </li>
                           <li data-option-num="5">
                              <h5 class="program-title">CNC Machining</h5>
                              <input class="form-control radio pull-right" type="radio" />
                           </li>
                           <li data-option-num="5">
                              <h5 class="program-title">Welding</h5>
                              <input class="form-control radio pull-right" type="radio" />
                           </li>
                           <li data-option-num="6">
                              <h5 class="program-title">Undecided</h5>
                              <input class="form-control radio pull-right" type="radio" />
                           </li>
                        </ul>
                     </div>
                     <div class="small-12 medium-8" id="programInfoWrapper">
                        <div id="programImageWrapper">
                           <div id="programImage"></div>
                        </div>
                        <div class="programs-right program-description" style="display:none;">
                           <div class="programDescriptionWrapper">
                              <tag class="program-title" id="program-title">Automotive</tag>
                              <p>In this program, you'll learn how to diagnose, maintain and repair domestic and foreign automobiles.
                                 You will find out how to troubleshoot problems of all kinds, using the state-of-the-industry engine
                                 analyzers, handheld scanners and other computerized diagnostic equipment. You'll learn everything
                                 from basic engine systems to computerized fuel injection, anti-lock brakes, passenger restraint systems,
                                 computerized engine controls and much more.
                              </p>
                              <p>Simply answer a few questions below to have one of our Admissions Representatives contact you about technician
                                 training opportunities.
                              </p>
                              <p>It only takes a minute.</p>
                              <p class="text-center"><a class="button-special ltr" name="AutomotiveBtn">Next</a>
                              </p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!--End Desktop-->
               </div>
               <!--End AOI Selection-->
            </div>
         </div>
         <div >
            <div >
               <div class="row" id="personalInfo" style="display:none;">
                  <div class="columns small-12">
                     <form data-abide="" id="leadForm" novalidate="">
                        <div class="row">
                           <div class="small-12 medium-5 columns"><label for="inputFName">First Name</label>
                              <input aria-describedby="inputFName" id="inputFName" name="FirstName" placeholder="First" required="" type="text" /><span aria-hidden="true" class="glyphicon form-control-feedback"></span><span class="sr-only">(error)</span>
                           </div>
                           <div class="small-12 medium-5 columns"><label for="inputLName">Last Name</label>
                              <input aria-describedby="inputLName" id="inputLName" name="LastName" placeholder="Last" required="" type="text" /><span aria-hidden="true" class="glyphicon form-control-feedback"></span><span class="sr-only">(error)</span>
                           </div>
                        </div>
                        <div class="row">
                           <div class="small-12 medium-5 columns"><label for="inputEmail">Email</label>
                              <input aria-describedby="inputEmail" id="inputEmail" name="Email" placeholder="email@email.com" required="" type="email" /><span aria-hidden="true" class="glyphicon form-control-feedback"></span><span class="sr-only">(error)</span>
                           </div>
                           <div class="small-12 medium-5 columns"><label for="inputPhone">Phone</label>
                              <input aria-describedby="inputPhone" id="inputPhone" maxlength="14" name="Phone" pattern="^\(?([2-9]{1}\d{9})|([2-9]\d{2}(-| |\.|\)\W?)\d{3}(-| |\.)?\d{4})$" placeholder="555-555-5555" required="" type="tel" /><span aria-hidden="true" class="glyphicon form-control-feedback"></span><span class="sr-only">(error)</span>
                           </div>
                        </div>
                        <div class="row">
                           <div class="small-12 medium-5 columns"><label for="inputPostal">Postal Code</label>
                              <input aria-describedby="inputPostal" id="inputPostal" maxlength="5" name="PostalCode" pattern="^\d{5}$" placeholder="12345" required="" type="text" /><span aria-hidden="true" class="glyphicon form-control-feedback"></span><span class="sr-only">(error)</span>
                           </div>
                           <div class="small-12 medium-5 columns">
                              <span aria-hidden="true" class="glyphicon form-control-feedback select"></span><span class="sr-only">(error)</span>
                              <label for="countrySelect">Select Your Country</label>
                              <select aria-describedby="countrySelect" id="countrySelect" name="Country" required="">
                                 <option value="US">United States</option>
                                 <option value="AF">Afghanistan </option>
                                 <option value="AL">Albania</option>
                                 <option value="DZ">Algeria</option>
                                 <option value="AS">American Samoa</option>
                                 <option value="AD">Andorra</option>
                                 <option value="AO">Angola</option>
                                 <option value="AI">Anguilla</option>
                                 <option value="AQ">Antarctica</option>
                                 <option value="AG">Antigua And Barbuda</option>
                                 <option value="AR">Argentina</option>
                                 <option value="AM">Armenia</option>
                                 <option value="AW">Aruba</option>
                                 <option value="AU">Australia</option>
                                 <option value="AT">Austria</option>
                                 <option value="10">Autauga</option>
                                 <option value="AZ">Azerbaijan</option>
                                 <option value="BS">Bahamas</option>
                                 <option value="BH">Bahrain</option>
                                 <option value="BD">Bangladesh</option>
                                 <option value="BB">Barbados</option>
                                 <option value="BY">Belarus</option>
                                 <option value="BE">Belgium</option>
                                 <option value="BZ">Belize</option>
                                 <option value="BJ">Benin</option>
                                 <option value="BM">Bermuda</option>
                                 <option value="BT">Bhutan</option>
                                 <option value="BO">Bolivia</option>
                                 <option value="BA">Bosnia And Herzegovina</option>
                                 <option value="BW">Botswana</option>
                                 <option value="BV">Bouvet Island</option>
                                 <option value="BR">Brazil</option>
                                 <option value="IO">British Indian Ocean Territory</option>
                                 <option value="BN">Brunei Darussalam</option>
                                 <option value="BG">Bulgaria</option>
                                 <option value="BF">Burkina Faso</option>
                                 <option value="BI">Burundi</option>
                                 <option value="KH">Cambodia</option>
                                 <option value="CM">Cameroon</option>
                                 <option value="CA">Canada</option>
                                 <option value="CV">Cape Verde</option>
                                 <option value="KY">Cayman Islands</option>
                                 <option value="CF">Central African Republic</option>
                                 <option value="TD">Chad</option>
                                 <option value="CL">Chile</option>
                                 <option value="CN">China</option>
                                 <option value="CX">Christmas Island</option>
                                 <option value="CC">Cocos (Keeling) Islands</option>
                                 <option value="CO">Colombia</option>
                                 <option value="KM">Comoros</option>
                                 <option value="CG">Congo</option>
                                 <option value="The Democratic Republic Of The Congo">"Congo</option>
                                 <option value="CK">Cook Islands</option>
                                 <option value="CR">Costa Rica</option>
                                 <option value="CI">Cote D'Ivoire</option>
                                 <option value="HR">Croatia</option>
                                 <option value="CU">Cuba</option>
                                 <option value="CY">Cyprus</option>
                                 <option value="CZ">Czech Republic</option>
                                 <option value="DK">Denmark</option>
                                 <option value="DJ">Djibouti</option>
                                 <option value="DM">Dominica</option>
                                 <option value="DO">Dominican Republic</option>
                                 <option value="TP">East Timor</option>
                                 <option value="EC">Ecuador</option>
                                 <option value="EG">Egypt</option>
                                 <option value="SV">El Salvador</option>
                                 <option value="GQ">Equatorial Guinea</option>
                                 <option value="ER">Eritrea</option>
                                 <option value="EE">Estonia</option>
                                 <option value="ET">Ethiopia</option>
                                 <option value="FK">Falkland Islands (Malvinas)</option>
                                 <option value="FO">Faroe Islands</option>
                                 <option value="FJ">Fiji</option>
                                 <option value="FI">Finland</option>
                                 <option value="FR">France</option>
                                 <option value="GF">French Guiana</option>
                                 <option value="PF">French Polynesia</option>
                                 <option value="TF">French Southern Territories</option>
                                 <option value="GA">Gabon</option>
                                 <option value="GM">Gambia</option>
                                 <option value="GE">Georgia</option>
                                 <option value="DE">Germany</option>
                                 <option value="GH">Ghana</option>
                                 <option value="GI">Gibraltar</option>
                                 <option value="GR">Greece</option>
                                 <option value="GL">Greenland</option>
                                 <option value="GD">Grenada</option>
                                 <option value="GP">Guadeloupe</option>
                                 <option value="GU">Guam</option>
                                 <option value="GT">Guatemala</option>
                                 <option value="GN">Guinea</option>
                                 <option value="GW">Guinea-Bissau</option>
                                 <option value="GY">Guyana</option>
                                 <option value="HT">Haiti</option>
                                 <option value="HM">Heard Island And Mcdonald Islands</option>
                                 <option value="VA">Holy See (Vatican City State)</option>
                                 <option value="HN">Honduras</option>
                                 <option value="HK">Hong Kong</option>
                                 <option value="HU">Hungary</option>
                                 <option value="IS">Iceland</option>
                                 <option value="IN">India</option>
                                 <option value="ID">Indonesia</option>
                                 <option value="IR">Iran</option>
                                 <option value="IQ">Iraq</option>
                                 <option value="IE">Ireland</option>
                                 <option value="IL">Israel</option>
                                 <option value="IT">Italy</option>
                                 <option value="JM">Jamaica</option>
                                 <option value="JP">Japan</option>
                                 <option value="JO">Jordan</option>
                                 <option value="KZ">Kazakstan</option>
                                 <option value="KE">Kenya</option>
                                 <option value="KI">Kiribati</option>
                                 <option value="KP">South Korea</option>
                                 <option value="KR">North Korea</option>
                                 <option value="KW">Kuwait</option>
                                 <option value="KG">Kyrgyzstan</option>
                                 <option value="LA">Lao</option>
                                 <option value="LV">Latvia</option>
                                 <option value="LB">Lebanon</option>
                                 <option value="LS">Lesotho</option>
                                 <option value="LR">Liberia</option>
                                 <option value="LY">Libyan Arab Jamahiriya</option>
                                 <option value="LI">Liechtenstein</option>
                                 <option value="LT">Lithuania</option>
                                 <option value="LU">Luxembourg</option>
                                 <option value="MO">Macau</option>
                                 <option value="MK">Macedonia</option>
                                 <option value="MG">Madagascar</option>
                                 <option value="MW">Malawi</option>
                                 <option value="MY">Malaysia</option>
                                 <option value="MV">Maldives</option>
                                 <option value="ML">Mali</option>
                                 <option value="MT">Malta</option>
                                 <option value="MH">Marshall Islands</option>
                                 <option value="MQ">Martinique</option>
                                 <option value="MR">Mauritania</option>
                                 <option value="MU">Mauritius</option>
                                 <option value="YT">Mayotte</option>
                                 <option value="MX">Mexico</option>
                                 <option value="FM">Micronesia</option>
                                 <option value="MD">Moldova</option>
                                 <option value="MC">Monaco</option>
                                 <option value="MN">Mongolia</option>
                                 <option value="MS">Montserrat</option>
                                 <option value="MA">Morocco</option>
                                 <option value="MZ">Mozambique</option>
                                 <option value="MM">Myanmar</option>
                                 <option value="NA">Namibia</option>
                                 <option value="NR">Nauru</option>
                                 <option value="NP">Nepal</option>
                                 <option value="NL">Netherlands</option>
                                 <option value="AN">Netherlands Antilles</option>
                                 <option value="NC">New Caledonia</option>
                                 <option value="NZ">New Zealand</option>
                                 <option value="NI">Nicaragua</option>
                                 <option value="NE">Niger</option>
                                 <option value="NG">Nigeria</option>
                                 <option value="NU">Niue</option>
                                 <option value="NF">Norfolk Island</option>
                                 <option value="MP">Northern Mariana Islands</option>
                                 <option value="NO">Norway</option>
                                 <option value="OM">Oman</option>
                                 <option value="PK">Pakistan</option>
                                 <option value="PW">Palau</option>
                                 <option value="PS">Palestinian Territory</option>
                                 <option value="PA">Panama</option>
                                 <option value="PG">Papua New Guinea</option>
                                 <option value="PY">Paraguay</option>
                                 <option value="PE">Peru</option>
                                 <option value="PH">Philippines</option>
                                 <option value="PN">Pitcairn</option>
                                 <option value="PL">Poland</option>
                                 <option value="PT">Portugal</option>
                                 <option value="PR">Puerto Rico</option>
                                 <option value="QA">Qatar</option>
                                 <option value="RE">Reunion</option>
                                 <option value="RO">Romania</option>
                                 <option value="RU">Russian Federation</option>
                                 <option value="RW">Rwanda</option>
                                 <option value="SH">Saint Helena</option>
                                 <option value="KN">Saint Kitts And Nevis</option>
                                 <option value="LC">Saint Lucia</option>
                                 <option value="PM">Saint Pierre And Miquelon</option>
                                 <option value="VC">Saint Vincent And The Grenadines</option>
                                 <option value="WS">Samoa</option>
                                 <option value="SM">San Marino</option>
                                 <option value="ST">Sao Tome And Principe</option>
                                 <option value="SA">Saudi Arabia</option>
                                 <option value="SN">Senegal</option>
                                 <option value="SC">Seychelles</option>
                                 <option value="SL">Sierra Leone</option>
                                 <option value="SG">Singapore</option>
                                 <option value="SK">Slovakia</option>
                                 <option value="SI">Slovenia</option>
                                 <option value="SB">Solomon Islands</option>
                                 <option value="SO">Somalia</option>
                                 <option value="ZA">South Africa</option>
                                 <option value="GS">South Georgia And The South Sandwich Isl</option>
                                 <option value="ES">Spain</option>
                                 <option value="LK">Sri Lanka</option>
                                 <option value="SD">Sudan</option>
                                 <option value="SR">Suriname</option>
                                 <option value="SJ">Svalbard And Jan Mayen</option>
                                 <option value="SZ">Swaziland</option>
                                 <option value="SE">Sweden</option>
                                 <option value="CH">Switzerland</option>
                                 <option value="SY">Syrian Arab Republic</option>
                                 <option value="TW">Taiwan</option>
                                 <option value="TJ">Tajikistan</option>
                                 <option value="TZ">Tanzania</option>
                                 <option value="TH">Thailand</option>
                                 <option value="TG">Togo</option>
                                 <option value="TK">Tokelau</option>
                                 <option value="TO">Tonga</option>
                                 <option value="TT">Trinidad And Tobago</option>
                                 <option value="TN">Tunisia</option>
                                 <option value="TR">Turkey</option>
                                 <option value="TM">Turkmenistan</option>
                                 <option value="TC">Turks And Caicos Islands</option>
                                 <option value="TV">Tuvalu</option>
                                 <option value="UG">Uganda</option>
                                 <option value="UA">Ukraine</option>
                                 <option value="AE">United Arab Emirates</option>
                                 <option value="GB">United Kingdom</option>
                                 <option value="UM">United States Minor Outlying Islands</option>
                                 <option value="UY">Uruguay</option>
                                 <option value="UZ">Uzbekistan</option>
                                 <option value="VU">Vanuatu</option>
                                 <option value="VE">Venezuela</option>
                                 <option value="VN">Vietnam</option>
                                 <option value="VG">British Virgin Islands</option>
                                 <option value="VI">US Virgin Islands</option>
                                 <option value="WF">Wallis And Futuna</option>
                                 <option value="EH">Western Sahara</option>
                                 <option value="YE">Yemen</option>
                                 <option value="YU">Yugoslavia</option>
                                 <option value="ZM">Zambia</option>
                                 <option value="ZW">Zimbabwe</option>
                              </select>
                           </div>
                        </div>
                        <div class="row">
                           <div class="small-12 medium-2 columns"><span aria-hidden="true" class="glyphicon form-control-feedback select"></span><span class="sr-only">(error)</span>
                              <label for="ageSelect">Age</label>
                              <select aria-describedby="ageSelect" id="ageSelect" name="Age" required=""></select>
                           </div>
                        </div>
                        <div class="row" id="eduWrapper" style="display:none;">
                           <div class="small-12 medium-5 columns">
                              <label for="eduSelect">What type of Diploma or Certificate will/did you obtain?</label>
                              <select aria-describedby="eduSelect" data-abide-ignore="" id="eduSelect" name="Diploma" required="">
                                 <option value="">- SELECT -</option>
                                 <option value="grad_HiS">High School Diploma</option>
                                 <option value="grad_GED">GED</option>
                                 <option value="grad_HoS">Home School Diploma</option>
                                 <option value="none_HiS">None, I'm in High School</option>
                                 <option value="none_GED">None, I'm getting my GED</option>
                                 <option value="none_HoS">None, I'm being Home Schooled</option>
                                 <option value="none">None</option>
                              </select>
                           </div>
                           <div class="small-12 medium-2 columns" id="gradYearWrapper"><label for="inputGradYear">Year of Graduation/GED</label>
                              <input aria-describedby="inputGradYear" id="inputGradYear" maxlength="5" name="GradYear" placeholder="YYYY" required="" type="number" /><span aria-hidden="true" class="glyphicon form-control-feedback"></span><span class="sr-only">(error)</span>
                           </div>
                        </div>
                        <div class="row" id="milStatusWrapper" style="display:none;">
                           <fieldset class="fieldset small-12 medium-5 columns">
                              <legend>Are you current or former U.S. Military, Guard, or Reserve?</legend>
                              <input aria-describedby="rdoMilYes" data-abide-ignore="" id="rdoMilYes" name="ActiveMilitary" type="radio" value="true" /><label for="rdoMilYes">Yes</label>
                              <input aria-describedby="rdoMilNo" data-abide-ignore="" id="rdoMilNo" name="ActiveMilitary" required="" type="radio" value="false" /><label for="rdoMilNo">No</label>
                           </fieldset>
                           <div class="small-12 medium-5 columns" id="milReleaseDateWrapper" style="display:none;">
                              <label>Military Release Date</label>
                              <div class="row">
                                 <div class="small-6 medium-5 columns"><label for="inputMilReleaseYear">Year</label>
                                    <input aria-describedby="inputMilReleaseYear" data-abide-ignore="" id="inputMilReleaseYear" max="4" name="MilReleaseYear" placeholder="YYYY" required="" type="number" />
                                 </div>
                                 <div class="small-6 medium-5 columns">
                                    <label for="milReleaseMonthSelect">Month</label>
                                    <select aria-describedby="milReleaseMonthSelect" data-abide-ignore="" id="milReleaseMonthSelect" name="MilReleaseMonth" required="">
                                       <option value="">Select</option>
                                       <option value="1">January</option>
                                       <option value="2">February</option>
                                       <option value="3">March</option>
                                       <option value="4">April</option>
                                       <option value="5">May</option>
                                       <option value="6">June</option>
                                       <option value="7">July</option>
                                       <option value="8">August</option>
                                       <option value="9">September</option>
                                       <option value="10">October</option>
                                       <option value="11">November</option>
                                       <option value="12">December</option>
                                    </select>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="columns small-12 tcpa" style="display:none;">
                              <p>By clicking "Get Info", I agree to UTI's
                                 <a data-open="privacyPolicy" href="/privacy-policy" target="_blank">Privacy Policy</a>.
                              </p>
                              <p>By clicking the button below, I agree that Universal Technical Institute, Inc. (&ldquo;UTI&rdquo;) and
                                 its subsidiaries, affiliates and representatives may email, call and/or text me about educational
                                 programs and services at any phone number I provide, including a wireless number, using automated
                                 technology. I understand that my consent is not required to apply, enroll or make any purchase.
                              </p>
                           </div>
                        </div>
                        <div class="row">
                           <div class="small-12 medium-2 columns">
                              <a class="button-special ltr" id="getInfoBtn">Get Info</a>
                              <!--<button type="button"  class="btn btn-default next-button" OnClientClick="return false;">Get Info</button> -->
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
         <?php include('../views/navigation.php'); ?>
         <div id="Contentplaceholder1_TCC38B385001_Col00" class="sf_colsIn container" data-sf-element="Container" data-placeholder-label="Container">
         </div>
         <div id="Contentplaceholder1_TCC38B385002_Col00" class="sf_colsIn container" data-sf-element="Container" data-placeholder-label="Container"></div>
         <div id="Contentplaceholder1_TCC38B385003_Col00" class="sf_colsIn container" data-sf-element="Container" data-placeholder-label="Container"></div>
         <div class="sf-breadcrumb breadcrumbs-container">
            <div class="breadcrumb-inner">
               <nav aria-label="You are here:">
                  <ul class="breadcrumbs">
                     <li><a href="/">Home</a></li>
                     <li><span>Form 15 - General Inquiry - Short</span></li>
                  </ul>
               </nav>
            </div>
         </div>
         <div class="main-footer widget  to-bottom-right-alt background-gradient-blue" >
            <div class="row align-center-middle hide-for-large">
               <div class="small-5 medium-3 columns">
                  <a class="logo" href="/schools/universal-technical-institute">
                  <img data-src="https://utiedu.azureedge.net/images/default-source/global/logos/footer-logos-only/uti_rev_125x60.png?sfvrsn=dfdeb775_6" alt="UTI_Rev_125x60" />
                  </a>
               </div>
               <div class="small-5 medium-3 columns">
                  <a class="logo" href="/schools/motorcycle-mechanics-institute">
                  <img data-src="https://utiedu.azureedge.net/images/default-source/global/logos/footer-logos-only/mmi_moto_rev_125x60.png?sfvrsn=7a4612b6_10" alt="MMI_Moto_Rev_125x60" />
                  </a>
               </div>
               <div class="small-5 medium-3 columns">
                  <a class="logo" href="/schools/marine-mechanics-institute">
                  <img data-src="https://utiedu.azureedge.net/images/default-source/global/logos/footer-logos-only/mmi_marine_rev_125x60.png?sfvrsn=866b017c_8" alt="MMI_Marine_Rev_125x60" />
                  </a>
               </div>
               <div class="small-5 medium-3 columns">
                  <a class="logo" href="/schools/nascar-technical-institute">
                  <img data-src="https://utiedu.azureedge.net/images/default-source/global/logos/footer-logos-only/nascar_tech_rev_125x60.png?sfvrsn=5cbc10f9_6" alt="NASCAR_Tech_Rev_125x60" />
                  </a>
               </div>
            </div>
            <div class="row align-justify">
               <div class="small-6 medium-4 large-2 columns footer-column">
                  <strong>Getting Started</strong>
                  <ul>
                     <li>
                        <a href="/schools">Schools</a>
                     </li>
                     <li>
                        <a href="/programs">Core Programs</a>
                     </li>
                     <li>
                        <a href="/why-uti/industry-partnerships/original-equipment-manufacturers">Specialized Training</a>
                     </li>
                     <li>
                        <a href="/locations/">Locations</a>
                     </li>
                     <li>
                        <a href="/admissions/tuition/">Tuition</a>
                     </li>
                     <li>
                        <a href="/why-uti">Why UTI</a>
                     </li>
                  </ul>
               </div>
               <div class="small-6 medium-4 large-2 columns footer-column">
                  <strong>Becoming a Student</strong>
                  <ul>
                     <li>
                        <a href="/admissions">Admissions</a>
                     </li>
                     <li>
                        <a href="/programs/catalogs">Catalogs</a>
                     </li>
                     <li>
                        <a href="/financial-aid">Financial Aid</a>
                     </li>
                     <li>
                        <a href="/financial-aid/scholarships-grants">Scholarships &amp; Grants</a>
                     </li>
                     <li>
                        <a href="/support-services/military-veteran-services/">Military Services</a>
                     </li>
                     <li>
                        <a href="/support-services/disability-services">Disability Services</a>
                     </li>
                  </ul>
               </div>
               <div class="small-6 medium-4 large-2 columns footer-column">
                  <strong>About Us</strong>
                  <ul>
                     <li>
                        <a href="/about#wwcnyl10iu">Accreditation</a>
                     </li>
                     <li>
                        <a href="/about/">Our Mission &amp; Values</a>
                     </li>
                     <li>
                        <a href="/careers/">Careers</a>
                     </li>
                     <li>
                        <a href="https://uti.investorroom.com/">Investor Relations</a>
                     </li>
                     <li>
                        <a href="https://uti.mediaroom.com/">Newsroom</a>
                     </li>
                  </ul>
               </div>
               <div class="small-6 medium-4 large-2 columns footer-column">
                  <strong>Resources</strong>
                  <ul>
                     <li>
                        <a href="/support-services/housing-assistance">Housing Assistance</a>
                     </li>
                     <li>
                        <a href="/why-uti/jobs-career-opportunities">Job Opportunities</a>
                     </li>
                     <li>
                        <a href="">Parent Information</a>
                     </li>
                     <li>
                        <a href="/support-services/voter-registration">Voter Registration</a>
                     </li>
                     <li>
                        <a href="/1098-t">1098-T</a>
                     </li>
                  </ul>
               </div>
               <div class="small-6 medium-4 large-2 columns footer-column">
                  <strong>Contact</strong>
                  <ul>
                     <li>
                        <a href="tel:8008347308">800-834-7308</a>
                     </li>
                     <li>
                        <a href="/request-info">Request Info</a>
                     </li>
                     <li>
                        <a href="/contact-us">Contact Us</a>
                     </li>
                     <li>
                        <a href="/locations/campus-tours">Campus Tours</a>
                     </li>
                  </ul>
               </div>
               <div class="small-12 medium-4 large-2 columns footer-column show-for-large">
                  <div class="row collapse">
                     <div class="small-6 medium-12 columns">
                        <a class="footer-logo" href="/schools/universal-technical-institute">
                        <img data-src="https://utiedu.azureedge.net/images/default-source/global/logos/footer-logos-only/uti_rev_125x60.png?sfvrsn=dfdeb775_6" alt="UTI_Rev_125x60" />
                        </a>
                     </div>
                     <div class="small-6 medium-12 columns">
                        <a class="footer-logo" href="/schools/motorcycle-mechanics-institute">
                        <img data-src="https://utiedu.azureedge.net/images/default-source/global/logos/footer-logos-only/mmi_moto_rev_125x60.png?sfvrsn=7a4612b6_10" alt="MMI_Moto_Rev_125x60" />
                        </a>
                     </div>
                     <div class="small-6 medium-12 columns">
                        <a class="footer-logo" href="/schools/marine-mechanics-institute">
                        <img data-src="https://utiedu.azureedge.net/images/default-source/global/logos/footer-logos-only/mmi_marine_rev_125x60.png?sfvrsn=866b017c_8" alt="MMI_Marine_Rev_125x60" />
                        </a>
                     </div>
                     <div class="small-6 medium-12 columns">
                        <a class="footer-logo" href="/schools/nascar-technical-institute">
                        <img data-src="https://utiedu.azureedge.net/images/default-source/global/logos/footer-logos-only/nascar_tech_rev_125x60.png?sfvrsn=5cbc10f9_6" alt="NASCAR_Tech_Rev_125x60" />
                        </a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row column" aria-hidden="true">
               <div class="divider" aria-hidden="true"></div>
            </div>
            <div class="main-footer--disclaimer row column" aria-label="disclaimer">
               <p>
               <p>1) UTI cannot guarantee employment or salary.</p>
               <p>2) For important information about the educational debt, earnings and completion rates of students who attended this program, visit our website at <a href="/disclosures">www.uti.edu/disclosures.</a></p>
               </p>
            </div>
            <div class="main-footer--bottom row align-center-middle">
               <div class="small-12 columns">
                  <span aria-label="copyright" class="copyright">&copy; 2017 Universal Technical Institute, Inc.  All rights reserved.</span>
                  <ul class="menu">
                     <li>
                        <a href="/privacy-policy">Privacy Policy</a>
                     </li>
                     <li>
                        <a href="/legal-notice">Legal Notice</a>
                     </li>
                     <li>
                        <a href="/accessibility">Accessibility</a>
                     </li>
                     <li>
                        <a href="/disclosures">Disclosures</a>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script> <script src="/ResourcePackages/Foundation/assets/dist/js/vendor.min.js"></script> <script src="/ResourcePackages/Foundation/assets/dist/js/app.min.js"></script> <script type="text/javascript" src="/WebResource.axd?d=aEUP997Y9Nb8rRTc7ItCh7yWGN90DKKlUetyIYdlaWqudZYJh8srgRDOWtALfuva9X-3WY-wcvSbDKRRAvHxXJxEeDyWPF1twTcl5LwZqBwFiF9kI9eftRlwwL6eKDUkiyskbJC3Ahb1L6a8nX38lZ10Akmw_CbCu-GBQr5L2qjyP2emzkEIns6QOn2CJ4WA0&amp;t=636251111960000000"></script><script type="text/javascript">
         StatsClient.LogVisit('367b96fc-c327-4822-a528-62c676f4f2ac', 'daa64c3d-d260-450d-888e-9b25f7b29183');
      </script><script type="text/javascript">
         (function($) {
           $(document).ready(function() {
             $('.info-overlay h3').each(function() {
               $(this).text($(this).text().replace("Ε", "E"));
               $(this).text($(this).text().replace("Μ", "M"));
               $(this).text($(this).text().replace("Χ", "X"));
             });
           });
         })(jQuery);
      </script><script type="text/javascript">
         /********** ENVIRONMENT CONFIG **********/
         /* Enviroment Specific Configuration */
         
         var currentEnvironment = '';
         var host = window.location.host;
         
         /* Gateway URLs */
         
         /*var environments = {*/
         var gateway = {
             local: '//localhost:39540',
             dev: '//utileadgateway-dev.uti.edu',
             stage: '//utileadgatewaystage.uti.edu',
             qa: '//utileadgatewayqa.uti.edu',
             prod: '//utileadgateway.uti.edu',
             beta: '//utileadgateway.uti.edu',
             lead: '//crspc.cloudapp.net'
         };
         
         var activeEnvironment = {
             active: "",
             lead: "",
             gtm: ""
         };
         
         var getActiveEnvironment = function (host) {
             if (host.indexOf("www.uti.edu") >= 0 || host.indexOf('utiedu.azurewebsites.net') >= 0) {
                 activeEnvironment.active = gateway.prod;
                 activeEnvironment.lead = gateway.prod;
                 currentEnvironment = 'prod';
             } else if (host.indexOf("beta.uti.edu") >= 0) {
                 currentEnvironment = 'beta';
                 activeEnvironment.active = gateway.prod;
                 activeEnvironment.lead = gateway.prod;
             } else if (host.indexOf('stage.uti.edu') >= 0 || host.indexOf('uti-staging') >= 0) {
                 activeEnvironment.active = gateway.stage;
                 activeEnvironment.lead = gateway.stage;
                 currentEnvironment = 'stage';
             } else if (host.indexOf('dev.uti.edu') >= 0) {
                 activeEnvironment.active = gateway.dev;
                 activeEnvironment.lead = gateway.dev;
                 currentEnvironment = 'dev';
             } else if (host.indexOf('local.uti.edu') >= 0) {
                 activeEnvironment.active = gateway.dev;
                 activeEnvironment.lead = gateway.dev;
                 currentEnvironment = 'local';
             } else {
                 console.log("unable to determine correct environment from host " + host);
             }
         };
         
         getActiveEnvironment(host);
         /*End Environment Config*/
         /********** END ENVIRONMENT CONFIG **********/
         
         
         
         /********** ZIP CODE COMPLETER **********/
         /// <!--reference path="../jquery-1.10.1-vsdoc.js" /-->
         /**
          * @author : Ryan Divis <rdivis@solutionstream.com>, 2013
          * ZipCode - UTI Completer (typahead or autocompleter)
          *
          * Instantiates and extends the UTI Completer base. Talks to the zipcodes api.
          * Every option/function can be overridden/extended. There must be an adapter 
          * (jQuery/Bootstrap or other) in order to work.
          *
          * Simplest use on the page is jQuery('selector').zipCompleter();
          *
          * @option String sourceType : can be js (meaning that you have setup a javascript array/object) or ajax
          * @option String/Variable source : either the url for ajax connection or the name of the js variable for source
          * @option String indentifier : the unique indentifier returned from the api (the zipcode api returns zip)
          * @option Integer minLength : the minimum length for lookup
          * @option String valueField : this should be the field key that you want passed back as the actual value in the form
          * @option Bool fakeElement : whether or not to create a "real" element for value submission
          * @option Bool data : whether or not data needs to be submitted to the api
          * @option Bool updateStateCity : if true, state and city fields (#State,#City) will be updated with values from the selected item
          * 
          */
         
         
         (function (jQuery) {
         
             jQuery.zipCompleter = function (element, options) {
                 var defaults = {
                     source: activeEnvironment.active + '/api/zipcodes',
                     workingIndicator: '.workingIndicator',
                     fields: {
                         'Zip': 'zip',
                         'State': 'state',
                         'City': 'city'
                     },
                     afterUpdate: function () {
                     }
                 };
         
                 var plugin = this;
         
                 var jQueryelement = jQuery(element);
         
                 var jQueryform = jQuery(element).parents('form');
         
                 plugin.ajax = jQuery.ajax();
         
                 this.init = function () {
                     this.settings = jQuery.extend(defaults, options);
                     this.settings.workingIndicator += ":not(disabled)";
                     jQueryelement.change(function () {
                         if (jQuery(this).val().length === 5) {
                             plugin.getResults(jQuery(this).val());
                         }
                     });
                 };
         
                 this.getResults = function (value) {
                     jQuery.ajax({
                         url: plugin.settings.source + "/" + value,
                         type: 'get',
                         dateType: 'json',
                         beforeSend: function () {
                             if (jQuery(defaults.workingIndicator).length > 0) { jQuery(defaults.workingIndicator).show() };
                         },
                         error: function () {
                             if (jQuery(defaults.workingIndicator).length > 0) { jQuery(defaults.workingIndicator).hide() };
                         },
                         success: function (data) {
                             if (jQuery(defaults.workingIndicator).length > 0) { jQuery(defaults.workingIndicator).hide() };
                             //plugin.updateFields({ "Zip": data[0].Zip, "City": data[0].City, "State": data[0].State });
                             var state = data[0].State;
                             /// Update value of _state in app.js when ajax call is complete
                             if (data.length > 0) { _state = state; } else { _state = ""; }
                         }
                     });
                     // plugin.updateFields({"Zip":"84096","City":"Herriman","State":"UT"});
                 };
         
                 this.updateFields = function (data) {
                     jQuery.each(plugin.settings.fields, function (key, value) {
                         jQueryform.find('input[name="' + value + '"],select[name="' + value + '"]').val(data[key]);
                     });
         
                     this.settings.afterUpdate();
                 };
         
                 this.init();
             };
         
             jQuery.fn.zipCompleter = function (options) {
                 return this.each(function () {
                     if (undefined == jQuery(this).data('zipCompleter')) {
                         var plugin = new jQuery.zipCompleter(this, options);
                         jQuery(this).data('zipCompleter', plugin);
                     }
                     else {
                         jQuery(this).data('zipCompleter', jQuery(this).data('zipCompleter'));
                     }
                 });
             };
         
         })(jQuery);
         /********** END ZIP CODE COMPLETER **********/
         
         
         
         /********** JQUERY.UI **********/
         /*!
          * jQuery UI 1.8.5
          *
          * Copyright 2010, AUTHORS.txt (http://jqueryui.com/about)
          * Dual licensed under the MIT or GPL Version 2 licenses.
          * http://jquery.org/license
          *
          * http://docs.jquery.com/UI
          */
         (function (c, j) {
             function k(a) { return !c(a).parents().andSelf().filter(function () { return c.curCSS(this, "visibility") === "hidden" || c.expr.filters.hidden(this) }).length } c.ui = c.ui || {}; if (!c.ui.version) {
                 c.extend(c.ui, {
                     version: "1.8.5", keyCode: {
                         ALT: 18, BACKSPACE: 8, CAPS_LOCK: 20, COMMA: 188, COMMAND: 91, COMMAND_LEFT: 91, COMMAND_RIGHT: 93, CONTROL: 17, DELETE: 46, DOWN: 40, END: 35, ENTER: 13, ESCAPE: 27, HOME: 36, INSERT: 45, LEFT: 37, MENU: 93, NUMPAD_ADD: 107, NUMPAD_DECIMAL: 110, NUMPAD_DIVIDE: 111, NUMPAD_ENTER: 108, NUMPAD_MULTIPLY: 106,
                         NUMPAD_SUBTRACT: 109, PAGE_DOWN: 34, PAGE_UP: 33, PERIOD: 190, RIGHT: 39, SHIFT: 16, SPACE: 32, TAB: 9, UP: 38, WINDOWS: 91
                     }
                 }); c.fn.extend({
                     _focus: c.fn.focus, focus: function (a, b) { return typeof a === "number" ? this.each(function () { var d = this; setTimeout(function () { c(d).focus(); b && b.call(d) }, a) }) : this._focus.apply(this, arguments) }, scrollParent: function () {
                         var a; a = c.browser.msie && /(static|relative)/.test(this.css("position")) || /absolute/.test(this.css("position")) ? this.parents().filter(function () {
                             return /(relative|absolute|fixed)/.test(c.curCSS(this,
                                 "position", 1)) && /(auto|scroll)/.test(c.curCSS(this, "overflow", 1) + c.curCSS(this, "overflow-y", 1) + c.curCSS(this, "overflow-x", 1))
                         }).eq(0) : this.parents().filter(function () { return /(auto|scroll)/.test(c.curCSS(this, "overflow", 1) + c.curCSS(this, "overflow-y", 1) + c.curCSS(this, "overflow-x", 1)) }).eq(0); return /fixed/.test(this.css("position")) || !a.length ? c(document) : a
                     }, zIndex: function (a) {
                         if (a !== j) return this.css("zIndex", a); if (this.length) {
                             a = c(this[0]); for (var b; a.length && a[0] !== document;) {
                                 b = a.css("position");
                                 if (b === "absolute" || b === "relative" || b === "fixed") { b = parseInt(a.css("zIndex")); if (!isNaN(b) && b != 0) return b } a = a.parent()
                             }
                         } return 0
                     }, disableSelection: function () { return this.bind("mousedown.ui-disableSelection selectstart.ui-disableSelection", function (a) { a.preventDefault() }) }, enableSelection: function () { return this.unbind(".ui-disableSelection") }
                 }); c.each(["Width", "Height"], function (a, b) {
                     function d(f, g, l, m) {
                         c.each(e, function () {
                             g -= parseFloat(c.curCSS(f, "padding" + this, true)) || 0; if (l) g -= parseFloat(c.curCSS(f,
                                 "border" + this + "Width", true)) || 0; if (m) g -= parseFloat(c.curCSS(f, "margin" + this, true)) || 0
                         }); return g
                     } var e = b === "Width" ? ["Left", "Right"] : ["Top", "Bottom"], h = b.toLowerCase(), i = { innerWidth: c.fn.innerWidth, innerHeight: c.fn.innerHeight, outerWidth: c.fn.outerWidth, outerHeight: c.fn.outerHeight }; c.fn["inner" + b] = function (f) { if (f === j) return i["inner" + b].call(this); return this.each(function () { c.style(this, h, d(this, f) + "px") }) }; c.fn["outer" + b] = function (f, g) {
                         if (typeof f !== "number") return i["outer" + b].call(this, f); return this.each(function () {
                             c.style(this,
                                 h, d(this, f, true, g) + "px")
                         })
                     }
                 }); c.extend(c.expr[":"], { data: function (a, b, d) { return !!c.data(a, d[3]) }, focusable: function (a) { var b = a.nodeName.toLowerCase(), d = c.attr(a, "tabindex"); if ("area" === b) { b = a.parentNode; d = b.name; if (!a.href || !d || b.nodeName.toLowerCase() !== "map") return false; a = c("img[usemap=#" + d + "]")[0]; return !!a && k(a) } return (/input|select|textarea|button|object/.test(b) ? !a.disabled : "a" == b ? a.href || !isNaN(d) : !isNaN(d)) && k(a) }, tabbable: function (a) { var b = c.attr(a, "tabindex"); return (isNaN(b) || b >= 0) && c(a).is(":focusable") } });
                 c(function () { var a = document.createElement("div"), b = document.body; c.extend(a.style, { minHeight: "100px", height: "auto", padding: 0, borderWidth: 0 }); c.support.minHeight = b.appendChild(a).offsetHeight === 100; b.removeChild(a).style.display = "none" }); c.extend(c.ui, {
                     plugin: {
                         add: function (a, b, d) { a = c.ui[a].prototype; for (var e in d) { a.plugins[e] = a.plugins[e] || []; a.plugins[e].push([b, d[e]]) } }, call: function (a, b, d) {
                             if ((b = a.plugins[b]) && a.element[0].parentNode) for (var e = 0; e < b.length; e++) a.options[b[e][0]] && b[e][1].apply(a.element,
                                 d)
                         }
                     }, contains: function (a, b) { return document.compareDocumentPosition ? a.compareDocumentPosition(b) & 16 : a !== b && a.contains(b) }, hasScroll: function (a, b) { if (c(a).css("overflow") === "hidden") return false; b = b && b === "left" ? "scrollLeft" : "scrollTop"; var d = false; if (a[b] > 0) return true; a[b] = 1; d = a[b] > 0; a[b] = 0; return d }, isOverAxis: function (a, b, d) { return a > b && a < b + d }, isOver: function (a, b, d, e, h, i) { return c.ui.isOverAxis(a, d, h) && c.ui.isOverAxis(b, e, i) }
                 })
             }
         })(jQuery);
         /*!
          * jQuery UI Widget 1.8.5
          *
          * Copyright 2010, AUTHORS.txt (http://jqueryui.com/about)
          * Dual licensed under the MIT or GPL Version 2 licenses.
          * http://jquery.org/license
          *
          * http://docs.jquery.com/UI/Widget
          */
         (function (b, j) {
             if (b.cleanData) { var k = b.cleanData; b.cleanData = function (a) { for (var c = 0, d; (d = a[c]) != null; c++) b(d).triggerHandler("remove"); k(a) } } else { var l = b.fn.remove; b.fn.remove = function (a, c) { return this.each(function () { if (!c) if (!a || b.filter(a, [this]).length) b("*", this).add([this]).each(function () { b(this).triggerHandler("remove") }); return l.call(b(this), a, c) }) } } b.widget = function (a, c, d) {
                 var e = a.split(".")[0], f; a = a.split(".")[1]; f = e + "-" + a; if (!d) { d = c; c = b.Widget } b.expr[":"][f] = function (h) {
                     return !!b.data(h,
                         a)
                 }; b[e] = b[e] || {}; b[e][a] = function (h, g) { arguments.length && this._createWidget(h, g) }; c = new c; c.options = b.extend(true, {}, c.options); b[e][a].prototype = b.extend(true, c, { namespace: e, widgetName: a, widgetEventPrefix: b[e][a].prototype.widgetEventPrefix || a, widgetBaseClass: f }, d); b.widget.bridge(a, b[e][a])
             }; b.widget.bridge = function (a, c) {
                 b.fn[a] = function (d) {
                     var e = typeof d === "string", f = Array.prototype.slice.call(arguments, 1), h = this; d = !e && f.length ? b.extend.apply(null, [true, d].concat(f)) : d; if (e && d.substring(0, 1) ===
                         "_") return h; e ? this.each(function () { var g = b.data(this, a); if (!g) throw "cannot call methods on " + a + " prior to initialization; attempted to call method '" + d + "'"; if (!b.isFunction(g[d])) throw "no such method '" + d + "' for " + a + " widget instance"; var i = g[d].apply(g, f); if (i !== g && i !== j) { h = i; return false } }) : this.each(function () { var g = b.data(this, a); g ? g.option(d || {})._init() : b.data(this, a, new c(d, this)) }); return h
                 }
             }; b.Widget = function (a, c) { arguments.length && this._createWidget(a, c) }; b.Widget.prototype = {
                 widgetName: "widget",
                 widgetEventPrefix: "", options: { disabled: false }, _createWidget: function (a, c) { b.data(c, this.widgetName, this); this.element = b(c); this.options = b.extend(true, {}, this.options, b.metadata && b.metadata.get(c)[this.widgetName], a); var d = this; this.element.bind("remove." + this.widgetName, function () { d.destroy() }); this._create(); this._init() }, _create: function () { }, _init: function () { }, destroy: function () {
                     this.element.unbind("." + this.widgetName).removeData(this.widgetName); this.widget().unbind("." + this.widgetName).removeAttr("aria-disabled").removeClass(this.widgetBaseClass +
                         "-disabled ui-state-disabled")
                 }, widget: function () { return this.element }, option: function (a, c) { var d = a, e = this; if (arguments.length === 0) return b.extend({}, e.options); if (typeof a === "string") { if (c === j) return this.options[a]; d = {}; d[a] = c } b.each(d, function (f, h) { e._setOption(f, h) }); return e }, _setOption: function (a, c) { this.options[a] = c; if (a === "disabled") this.widget()[c ? "addClass" : "removeClass"](this.widgetBaseClass + "-disabled ui-state-disabled").attr("aria-disabled", c); return this }, enable: function () {
                     return this._setOption("disabled",
                         false)
                 }, disable: function () { return this._setOption("disabled", true) }, _trigger: function (a, c, d) { var e = this.options[a]; c = b.Event(c); c.type = (a === this.widgetEventPrefix ? a : this.widgetEventPrefix + a).toLowerCase(); d = d || {}; if (c.originalEvent) { a = b.event.props.length; for (var f; a;) { f = b.event.props[--a]; c[f] = c.originalEvent[f] } } this.element.trigger(c, d); return !(b.isFunction(e) && e.call(this.element[0], c, d) === false || c.isDefaultPrevented()) }
             }
         })(jQuery);
         /*
          * jQuery UI Effects 1.8.5
          *
          * Copyright 2010, AUTHORS.txt (http://jqueryui.com/about)
          * Dual licensed under the MIT or GPL Version 2 licenses.
          * http://jquery.org/license
          *
          * http://docs.jquery.com/UI/Effects/
          */
         jQuery.effects || function (f, j) {
             function l(c) {
                 var a; if (c && c.constructor == Array && c.length == 3) return c; if (a = /rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/.exec(c)) return [parseInt(a[1], 10), parseInt(a[2], 10), parseInt(a[3], 10)]; if (a = /rgb\(\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*\)/.exec(c)) return [parseFloat(a[1]) * 2.55, parseFloat(a[2]) * 2.55, parseFloat(a[3]) * 2.55]; if (a = /#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})/.exec(c)) return [parseInt(a[1],
                     16), parseInt(a[2], 16), parseInt(a[3], 16)]; if (a = /#([a-fA-F0-9])([a-fA-F0-9])([a-fA-F0-9])/.exec(c)) return [parseInt(a[1] + a[1], 16), parseInt(a[2] + a[2], 16), parseInt(a[3] + a[3], 16)]; if (/rgba\(0, 0, 0, 0\)/.exec(c)) return m.transparent; return m[f.trim(c).toLowerCase()]
             } function r(c, a) { var b; do { b = f.curCSS(c, a); if (b != "" && b != "transparent" || f.nodeName(c, "body")) break; a = "backgroundColor" } while (c = c.parentNode); return l(b) } function n() {
                 var c = document.defaultView ? document.defaultView.getComputedStyle(this, null) : this.currentStyle,
                     a = {}, b, d; if (c && c.length && c[0] && c[c[0]]) for (var e = c.length; e--;) { b = c[e]; if (typeof c[b] == "string") { d = b.replace(/\-(\w)/g, function (g, h) { return h.toUpperCase() }); a[d] = c[b] } } else for (b in c) if (typeof c[b] === "string") a[b] = c[b]; return a
             } function o(c) { var a, b; for (a in c) { b = c[a]; if (b == null || f.isFunction(b) || a in s || /scrollbar/.test(a) || !/color/i.test(a) && isNaN(parseFloat(b))) delete c[a] } return c } function t(c, a) { var b = { _: 0 }, d; for (d in a) if (c[d] != a[d]) b[d] = a[d]; return b } function k(c, a, b, d) {
                 if (typeof c == "object") {
                     d =
                         a; b = null; a = c; c = a.effect
                 } if (f.isFunction(a)) { d = a; b = null; a = {} } if (typeof a == "number" || f.fx.speeds[a]) { d = b; b = a; a = {} } if (f.isFunction(b)) { d = b; b = null } a = a || {}; b = b || a.duration; b = f.fx.off ? 0 : typeof b == "number" ? b : f.fx.speeds[b] || f.fx.speeds._default; d = d || a.complete; return [c, a, b, d]
             } f.effects = {}; f.each(["backgroundColor", "borderBottomColor", "borderLeftColor", "borderRightColor", "borderTopColor", "color", "outlineColor"], function (c, a) {
                 f.fx.step[a] = function (b) {
                     if (!b.colorInit) {
                         b.start = r(b.elem, a); b.end = l(b.end); b.colorInit =
                             true
                     } b.elem.style[a] = "rgb(" + Math.max(Math.min(parseInt(b.pos * (b.end[0] - b.start[0]) + b.start[0], 10), 255), 0) + "," + Math.max(Math.min(parseInt(b.pos * (b.end[1] - b.start[1]) + b.start[1], 10), 255), 0) + "," + Math.max(Math.min(parseInt(b.pos * (b.end[2] - b.start[2]) + b.start[2], 10), 255), 0) + ")"
                 }
             }); var m = {
                 aqua: [0, 255, 255], azure: [240, 255, 255], beige: [245, 245, 220], black: [0, 0, 0], blue: [0, 0, 255], brown: [165, 42, 42], cyan: [0, 255, 255], darkblue: [0, 0, 139], darkcyan: [0, 139, 139], darkgrey: [169, 169, 169], darkgreen: [0, 100, 0], darkkhaki: [189,
                     183, 107], darkmagenta: [139, 0, 139], darkolivegreen: [85, 107, 47], darkorange: [255, 140, 0], darkorchid: [153, 50, 204], darkred: [139, 0, 0], darksalmon: [233, 150, 122], darkviolet: [148, 0, 211], fuchsia: [255, 0, 255], gold: [255, 215, 0], green: [0, 128, 0], indigo: [75, 0, 130], khaki: [240, 230, 140], lightblue: [173, 216, 230], lightcyan: [224, 255, 255], lightgreen: [144, 238, 144], lightgrey: [211, 211, 211], lightpink: [255, 182, 193], lightyellow: [255, 255, 224], lime: [0, 255, 0], magenta: [255, 0, 255], maroon: [128, 0, 0], navy: [0, 0, 128], olive: [128, 128, 0], orange: [255,
                         165, 0], pink: [255, 192, 203], purple: [128, 0, 128], violet: [128, 0, 128], red: [255, 0, 0], silver: [192, 192, 192], white: [255, 255, 255], yellow: [255, 255, 0], transparent: [255, 255, 255]
             }, p = ["add", "remove", "toggle"], s = { border: 1, borderBottom: 1, borderColor: 1, borderLeft: 1, borderRight: 1, borderTop: 1, borderWidth: 1, margin: 1, padding: 1 }; f.effects.animateClass = function (c, a, b, d) {
                 if (f.isFunction(b)) { d = b; b = null } return this.each(function () {
                     var e = f(this), g = e.attr("style") || " ", h = o(n.call(this)), q, u = e.attr("className"); f.each(p, function (v,
                         i) { c[i] && e[i + "Class"](c[i]) }); q = o(n.call(this)); e.attr("className", u); e.animate(t(h, q), a, b, function () { f.each(p, function (v, i) { c[i] && e[i + "Class"](c[i]) }); if (typeof e.attr("style") == "object") { e.attr("style").cssText = ""; e.attr("style").cssText = g } else e.attr("style", g); d && d.apply(this, arguments) })
                 })
             }; f.fn.extend({
                 _addClass: f.fn.addClass, addClass: function (c, a, b, d) { return a ? f.effects.animateClass.apply(this, [{ add: c }, a, b, d]) : this._addClass(c) }, _removeClass: f.fn.removeClass, removeClass: function (c, a, b, d) {
                     return a ?
                         f.effects.animateClass.apply(this, [{ remove: c }, a, b, d]) : this._removeClass(c)
                 }, _toggleClass: f.fn.toggleClass, toggleClass: function (c, a, b, d, e) { return typeof a == "boolean" || a === j ? b ? f.effects.animateClass.apply(this, [a ? { add: c } : { remove: c }, b, d, e]) : this._toggleClass(c, a) : f.effects.animateClass.apply(this, [{ toggle: c }, a, b, d]) }, switchClass: function (c, a, b, d, e) { return f.effects.animateClass.apply(this, [{ add: a, remove: c }, b, d, e]) }
             }); f.extend(f.effects, {
                 version: "1.8.5", save: function (c, a) {
                     for (var b = 0; b < a.length; b++) a[b] !==
                         null && c.data("ec.storage." + a[b], c[0].style[a[b]])
                 }, restore: function (c, a) { for (var b = 0; b < a.length; b++) a[b] !== null && c.css(a[b], c.data("ec.storage." + a[b])) }, setMode: function (c, a) { if (a == "toggle") a = c.is(":hidden") ? "show" : "hide"; return a }, getBaseline: function (c, a) { var b; switch (c[0]) { case "top": b = 0; break; case "middle": b = 0.5; break; case "bottom": b = 1; break; default: b = c[0] / a.height } switch (c[1]) { case "left": c = 0; break; case "center": c = 0.5; break; case "right": c = 1; break; default: c = c[1] / a.width } return { x: c, y: b } }, createWrapper: function (c) {
                     if (c.parent().is(".ui-effects-wrapper")) return c.parent();
                     var a = { width: c.outerWidth(true), height: c.outerHeight(true), "float": c.css("float") }, b = f("<div></div>").addClass("ui-effects-wrapper").css({ fontSize: "100%", background: "transparent", border: "none", margin: 0, padding: 0 }); c.wrap(b); b = c.parent(); if (c.css("position") == "static") { b.css({ position: "relative" }); c.css({ position: "relative" }) } else {
                         f.extend(a, { position: c.css("position"), zIndex: c.css("z-index") }); f.each(["top", "left", "bottom", "right"], function (d, e) { a[e] = c.css(e); if (isNaN(parseInt(a[e], 10))) a[e] = "auto" });
                         c.css({ position: "relative", top: 0, left: 0 })
                     } return b.css(a).show()
                 }, removeWrapper: function (c) { if (c.parent().is(".ui-effects-wrapper")) return c.parent().replaceWith(c); return c }, setTransition: function (c, a, b, d) { d = d || {}; f.each(a, function (e, g) { unit = c.cssUnit(g); if (unit[0] > 0) d[g] = unit[0] * b + unit[1] }); return d }
             }); f.fn.extend({
                 effect: function (c) { var a = k.apply(this, arguments); a = { options: a[1], duration: a[2], callback: a[3] }; var b = f.effects[c]; return b && !f.fx.off ? b.call(this, a) : this }, _show: f.fn.show, show: function (c) {
                     if (!c ||
                         typeof c == "number" || f.fx.speeds[c] || !f.effects[c]) return this._show.apply(this, arguments); else { var a = k.apply(this, arguments); a[1].mode = "show"; return this.effect.apply(this, a) }
                 }, _hide: f.fn.hide, hide: function (c) { if (!c || typeof c == "number" || f.fx.speeds[c] || !f.effects[c]) return this._hide.apply(this, arguments); else { var a = k.apply(this, arguments); a[1].mode = "hide"; return this.effect.apply(this, a) } }, __toggle: f.fn.toggle, toggle: function (c) {
                     if (!c || typeof c == "number" || f.fx.speeds[c] || !f.effects[c] || typeof c ==
                         "boolean" || f.isFunction(c)) return this.__toggle.apply(this, arguments); else { var a = k.apply(this, arguments); a[1].mode = "toggle"; return this.effect.apply(this, a) }
                 }, cssUnit: function (c) { var a = this.css(c), b = []; f.each(["em", "px", "%", "pt"], function (d, e) { if (a.indexOf(e) > 0) b = [parseFloat(a), e] }); return b }
             }); f.easing.jswing = f.easing.swing; f.extend(f.easing, {
                 def: "easeOutQuad", swing: function (c, a, b, d, e) { return f.easing[f.easing.def](c, a, b, d, e) }, easeInQuad: function (c, a, b, d, e) { return d * (a /= e) * a + b }, easeOutQuad: function (c,
                     a, b, d, e) { return -d * (a /= e) * (a - 2) + b }, easeInOutQuad: function (c, a, b, d, e) { if ((a /= e / 2) < 1) return d / 2 * a * a + b; return -d / 2 * (--a * (a - 2) - 1) + b }, easeInCubic: function (c, a, b, d, e) { return d * (a /= e) * a * a + b }, easeOutCubic: function (c, a, b, d, e) { return d * ((a = a / e - 1) * a * a + 1) + b }, easeInOutCubic: function (c, a, b, d, e) { if ((a /= e / 2) < 1) return d / 2 * a * a * a + b; return d / 2 * ((a -= 2) * a * a + 2) + b }, easeInQuart: function (c, a, b, d, e) { return d * (a /= e) * a * a * a + b }, easeOutQuart: function (c, a, b, d, e) { return -d * ((a = a / e - 1) * a * a * a - 1) + b }, easeInOutQuart: function (c, a, b, d, e) {
                         if ((a /=
                             e / 2) < 1) return d / 2 * a * a * a * a + b; return -d / 2 * ((a -= 2) * a * a * a - 2) + b
                     }, easeInQuint: function (c, a, b, d, e) { return d * (a /= e) * a * a * a * a + b }, easeOutQuint: function (c, a, b, d, e) { return d * ((a = a / e - 1) * a * a * a * a + 1) + b }, easeInOutQuint: function (c, a, b, d, e) { if ((a /= e / 2) < 1) return d / 2 * a * a * a * a * a + b; return d / 2 * ((a -= 2) * a * a * a * a + 2) + b }, easeInSine: function (c, a, b, d, e) { return -d * Math.cos(a / e * (Math.PI / 2)) + d + b }, easeOutSine: function (c, a, b, d, e) { return d * Math.sin(a / e * (Math.PI / 2)) + b }, easeInOutSine: function (c, a, b, d, e) {
                         return -d / 2 * (Math.cos(Math.PI * a / e) - 1) +
                             b
                     }, easeInExpo: function (c, a, b, d, e) { return a == 0 ? b : d * Math.pow(2, 10 * (a / e - 1)) + b }, easeOutExpo: function (c, a, b, d, e) { return a == e ? b + d : d * (-Math.pow(2, -10 * a / e) + 1) + b }, easeInOutExpo: function (c, a, b, d, e) { if (a == 0) return b; if (a == e) return b + d; if ((a /= e / 2) < 1) return d / 2 * Math.pow(2, 10 * (a - 1)) + b; return d / 2 * (-Math.pow(2, -10 * --a) + 2) + b }, easeInCirc: function (c, a, b, d, e) { return -d * (Math.sqrt(1 - (a /= e) * a) - 1) + b }, easeOutCirc: function (c, a, b, d, e) { return d * Math.sqrt(1 - (a = a / e - 1) * a) + b }, easeInOutCirc: function (c, a, b, d, e) {
                         if ((a /= e / 2) < 1) return -d /
                             2 * (Math.sqrt(1 - a * a) - 1) + b; return d / 2 * (Math.sqrt(1 - (a -= 2) * a) + 1) + b
                     }, easeInElastic: function (c, a, b, d, e) { c = 1.70158; var g = 0, h = d; if (a == 0) return b; if ((a /= e) == 1) return b + d; g || (g = e * 0.3); if (h < Math.abs(d)) { h = d; c = g / 4 } else c = g / (2 * Math.PI) * Math.asin(d / h); return -(h * Math.pow(2, 10 * (a -= 1)) * Math.sin((a * e - c) * 2 * Math.PI / g)) + b }, easeOutElastic: function (c, a, b, d, e) {
                         c = 1.70158; var g = 0, h = d; if (a == 0) return b; if ((a /= e) == 1) return b + d; g || (g = e * 0.3); if (h < Math.abs(d)) { h = d; c = g / 4 } else c = g / (2 * Math.PI) * Math.asin(d / h); return h * Math.pow(2, -10 *
                             a) * Math.sin((a * e - c) * 2 * Math.PI / g) + d + b
                     }, easeInOutElastic: function (c, a, b, d, e) { c = 1.70158; var g = 0, h = d; if (a == 0) return b; if ((a /= e / 2) == 2) return b + d; g || (g = e * 0.3 * 1.5); if (h < Math.abs(d)) { h = d; c = g / 4 } else c = g / (2 * Math.PI) * Math.asin(d / h); if (a < 1) return -0.5 * h * Math.pow(2, 10 * (a -= 1)) * Math.sin((a * e - c) * 2 * Math.PI / g) + b; return h * Math.pow(2, -10 * (a -= 1)) * Math.sin((a * e - c) * 2 * Math.PI / g) * 0.5 + d + b }, easeInBack: function (c, a, b, d, e, g) { if (g == j) g = 1.70158; return d * (a /= e) * a * ((g + 1) * a - g) + b }, easeOutBack: function (c, a, b, d, e, g) {
                         if (g == j) g = 1.70158;
                         return d * ((a = a / e - 1) * a * ((g + 1) * a + g) + 1) + b
                     }, easeInOutBack: function (c, a, b, d, e, g) { if (g == j) g = 1.70158; if ((a /= e / 2) < 1) return d / 2 * a * a * (((g *= 1.525) + 1) * a - g) + b; return d / 2 * ((a -= 2) * a * (((g *= 1.525) + 1) * a + g) + 2) + b }, easeInBounce: function (c, a, b, d, e) { return d - f.easing.easeOutBounce(c, e - a, 0, d, e) + b }, easeOutBounce: function (c, a, b, d, e) { return (a /= e) < 1 / 2.75 ? d * 7.5625 * a * a + b : a < 2 / 2.75 ? d * (7.5625 * (a -= 1.5 / 2.75) * a + 0.75) + b : a < 2.5 / 2.75 ? d * (7.5625 * (a -= 2.25 / 2.75) * a + 0.9375) + b : d * (7.5625 * (a -= 2.625 / 2.75) * a + 0.984375) + b }, easeInOutBounce: function (c,
                         a, b, d, e) { if (a < e / 2) return f.easing.easeInBounce(c, a * 2, 0, d, e) * 0.5 + b; return f.easing.easeOutBounce(c, a * 2 - e, 0, d, e) * 0.5 + d * 0.5 + b }
             })
         }(jQuery);
         /********** END JQUERY.UI **********/
         
         
         
         /********** JQUERY.UTI.UI **********/
         /// <!--reference path="jquery-1.9.1.js" /-->
         /// <!--reference path="purl/2.3.1/purl.js" /-->
         /// <!--reference path="jquery-ui-1.10.4.custom.min.js" /-->
         /*  
             Uti form plug-in that creates an underlying lead based on the option: leadTemplate or the overidden version of
             and submits it to the gateway with the proper headers.  Uses the jquery-ui widget framework to manage object lifetime,
             instantiation, and events.
         
             Dependencies:
             -----------------------------------------------------------------
             jquery-ui, jquery, purl.js
           
             Options:
             url:            The url to submit leads to
             selector:       The element selector to attach the leads object to.
             leadTemplate:   The base lead to load so that it can be populated.  Can be replaced/merged by passing in a leadTemplate: option 
                             the widget constructor.  Merges fields if leadTemplate object provided, replaces if all fields provided are an
                             match the template exactly.          
           
             Usage:          See quUtiLeadApiTests.html
         
             Methods of interest:
             -----------------------------------------------------------------
             post:           Submits the lead object to the gateway.
             _appendAdditionalData  Appends the query string parameters to the AdditionalInfos section of the lead.
          */
         (function (jQuery, undefined) {
             jQuery.widget('uti.uiForm', {
                 options: {
                     url: activeEnvironment.lead + '/api/leads',
                     dataType: "json",
                     contentType: "application/json;charset=utf-8",
                     selector: 'form',
                     posted: null, // Callback for ajax done promise.
                     failed: null, // Callback for ajax fail promise.
                     leadTemplate: {
                         FormID: null,
                         InterestCode: null,
                         IPAddress: null,
                         LeadSourceCode: null,
                         Education: null,
                         FirstName: null,
                         LastName: null,
                         DateOfBirth: null,
                         Comments: null,
                         EmailAddresses: [
                             {
                                 Type: 'P',
                                 Address: null
                             }
                         ],
                         Addresses: [
                             {
                                 AddressType: "L",
                                 AddressLine1: null,
                                 City: null,
                                 State: null,
                                 PostalCode: null,
                                 Country: null,
                             }
                             //,{                        Removed from template if address is present it must contain a ZipCode.
                             //    AddressType: "A",
                             //    AddressLine1: null,
                             //    City: null,
                             //    State: null,
                             //    PostalCode: null,
                             //    Country: null,
                             //}
                         ],
                         PhoneNumbers: [
                             {
                                 Type: "C",
                                 Number: null,
                                 IsPrimary: true
                             },
                             {
                                 Type: "L",
                                 Number: null,
                                 IsPrimary: false
                             }
                         ],
                         GradMonth: null,
                         GradYear: null,
                         HighSchoolID: null,
                         HighSchoolState: null,
                         HighSchoolCity: null,
                         HighSchoolNotListed: null,
                         IsMilitary: null,
                         MilitaryID: null,
                         MilitarySeparation: null,
                         MilitaryBaseState: null,
                         InstallationNotListed: null,
                         AdditionalInfo: [
                             {
                                 Key: 'IsComplete', Value: false
                             },
                             {
                                 Key: 'LeadGuid', Value: null
                             },
                             {
                                 Key: 'IpAddress', Value: null
                             },
                             {
                                 Key: 'Age', Value: null
                             },
                             {
                                 Key: 'Destination', Value: null
                             }
                         ],
                     }
                 },
         
                 _create: function () {
                     this.element.addClass("uti-uiForm");
                 },
         
                 _init: function () {
                     // Load/copy lead template to element instance data...
                     this.element.data('lead', this.options.leadTemplate);
                     //this._appendQSParms(this.element.data('lead'));       ***OLD
                     this._appendAdditionalData(this.element.data('lead'));  ///NEW 
                     this.element.data('isLtIETen', this._isLtIETen());
                     if (this.element.data('isLtIETen')) {
         
                         this.options.dataType = "text";
                         this.options.contentType = "text/plain";
                         this.options.url =
                             jQuery.url(this.options.url).attr('protocol') + '://'
                             + jQuery.url(this.options.url).attr('host')
                             + ((jQuery.url(this.options.url).attr('port').length > 0) ? ':' + jQuery.url(this.options.url).attr('port') : '')
                             + '/corsproxy/';
                     }
                 },
         
                 // Use the _setOption method to respond to changes to options
                 _setOption: function (key, value) {
                     switch (key) {
                         case "clear":
                             // handle changes to clear option
                             break;
                     }
                     // In jQuery UI 1.8, you have to manually invoke the _setOption method from the base widget
                     jQuery.Widget.prototype._setOption.apply(this, arguments);
                     // In jQuery UI 1.9 and above, you use the _super method instead
                     // this._super("_setOption", key, value);
                 },
         
                 destroy: function () {
                     // Restore element to pre-uiForm create state...
                     this.element.removeClass('uti-uiForm');
                     this._destroy();
         
                 },
         
                 _appendAdditionalData: function (lead) {
                     /* 
                         // Summary
                         // Add all additional data to additionalData var, in query string format
                         // All 'addtionalData' gets added to lead.AddtionalInfo
                         // additionalData consists of...
                         //
                         // 1. current query string params (take prescedence over what's in cookieManager)
                         // 2. query string data saved in cookieManager to persist through any browsing on the site
                         // 3. cookie manager params generated based on referrer (referrer, campaign, medium, source, content, term, MarketoCookieId, InquiryId, ConversionUrl, AgencyId (Leapfrog ID))
                         // 4. optimizely (cookie manager)
                         // 5. device info (jQuery.pgwBrowser())
                         // 6. collection date (Date().toISOString())
                         // 7. google analytics client id (ga.getAll()[0].get('clientId'))
                         // 8. marketo cookie id (if cookie manager didnt have it)
                         //
                         // Details...
                         // Everything in the current QS is put in a variable. 
                         // If CM is available and CM has QS info saved in it that doesnt already exist in the current QS, CM's QS data is added to this variable (additionalData) 
                         // and the Referrer is set to CM's referralCookie val (which should be maintaining the original referrer if they have been redirects). 
                         // However, If CM is not available for some reason, the referrer val is set to 'empty'.
                         //
                         // If utm_content is in the additionalData variable, it's value trimmed.
                         // Add "&" back into Mkto cookie ID value to matain correct format.
                     */
         
                     try {
                         var additionalData = '';
                         var currentQS = window.location.search.substring(1);
                         var _infos = [];
                         var cm_OptAccID = '';
                         var cm_optPrjID = '';
                         var cm_optExpVarID = '';
         
                         /// CURRENT QUERY STRING DATA
                         if (currentQS.length > 0) { additionalData = currentQS; }
         
                         /// COOKIE MANAGER DATA     
                         ///
                         if (typeof cookieManager == 'object') {
         
                             /// Add cookieManager's data querystring data and referral info to additionalData, if there is any...
                             /// CM saves the QS data when a user first arrives at any page on uti.edu and persists that data through form submission.
         
                             /// ADD CM's QUERY STRING PARAMS, if keys dont already exist in current queryString
                             ///
                             var cmQSInfo = decodeURIComponent(cookieManager.getQSInfoCookie());
                             var refCookie = '';
         
                             if (cmQSInfo != null) {
                                 cmQSInfo = cmQSInfo.replace('#', '');
                                 cmQSInfo = cmQSInfo.replace('?', '&');
         
                                 /// If cookieManager has queryString info
                                 if (cmQSInfo.length > 0) {
                                     /// If page currently has queryString info
                                     if (currentQS.length > 0) {
                                         var cmQSArray = cmQSInfo.split('&');
                                         var cmQSPair;
         
                                         for (var x = 1; x < cmQSArray.length; x++) {
                                             cmQSPair = cmQSArray[x].split('=');
                                             /// If key from cookieManager queryString info doesnt already exist in additionalData, add the key/value pair
                                             if (additionalData.indexOf(cmQSPair[0]) === -1) {
                                                 additionalData += cmQSArray[x];
                                             }
                                             else {
                                                 /// Already exists in additionalData. Do not overwrite.
                                             }
                                         }
                                     }
                                     else {
                                         /// No QS in current URL; add CM's QS data to addtionalData
                                         additionalData += cmQSInfo;
                                     }
                                 }
                             }
                             ///
                             /// END ADD CM's QUERY STRING PARAMS
         
                             /// ADD CM's PARAMs, if not already added from current QS or CM's QS
         
                             if (cookieManager.cookiesEnabled() == false) {
                                 if (additionalData.indexOf('referrer') == -1) { additionalData += '&referrer=' + document.referrer; }
                             } else {
                                 if (additionalData.indexOf('referrer') == -1) { additionalData += '&referrer=' + cookieManager.getReferralCookie(); }
                             }
         
                             if (additionalData.indexOf('campaign') == -1) { additionalData += '&utm_campaign=' + cookieManager.campaign(); }
                             if (additionalData.indexOf('medium') == -1) { additionalData += '&utm_medium=' + cookieManager.medium(); }
                             if (additionalData.indexOf('source') == -1) { additionalData += '&utm_source=' + cookieManager.source(); }
                             if (additionalData.indexOf('content') == -1) { additionalData += '&utm_content=' + cookieManager.content(); }
                             if (additionalData.indexOf('term') == -1) { additionalData += '&term=' + cookieManager.term(); }
                             if (additionalData.indexOf('MarketoCookieId') == -1) { additionalData += '&MarketoCookieId=' + cookieManager.mktoID().replace("&", "|"); }
                             if (additionalData.indexOf('InquiryId') == -1) { additionalData += '&InquiryId=' + cookieManager.inqID(); }
                             if (additionalData.indexOf('ConversionUrl') == -1) { additionalData += '&ConversionUrl=' + cookieManager.conversionURL(); }
                             if (additionalData.indexOf('AgencyId') == -1) { additionalData += '&AgencyId=' + cookieManager.lfID(); } /// AGENCY ID (Leapfrog ID)
         
                             /// OPTIMIZELY
                             ///                    
         
                             /// Optimizely info from any previous browsing
                             cm_OptAccID = decodeURIComponent(cookieManager.optAcctID());
                             cm_optPrjID = decodeURIComponent(cookieManager.optProjID());
                             cm_optExpVarID = decodeURIComponent(cookieManager.optExpVarID());
         
                             /// Add Optimizely info to additionalData
                             /// Currently, only adds Active Experiments and their Variations            
         
                             if (cm_OptAccID.length > 0) {
                                 additionalData += '&Opt_AccountId=' + cm_OptAccID;
                             }
         
                             if (cm_optPrjID.length > 0) {
                                 additionalData += '&Opt_ProjectId=' + cm_optPrjID;
                             }
         
                             if (cm_optExpVarID.length > 0) {
                                 additionalData += '&Opt_ExpId_VarId=' + cm_optExpVarID;
                             }
                             ///
                             /// END OPTIMIZELY
                         } else {
                             if (additionalData.indexOf('referrer') == -1) { additionalData += '&referrer=' + document.referrer; }
                         }
                         ///
                         /// END COOKIE MANAGER DATA     
                         /// DEVICE INFO
                         additionalData += '&device=' + JSON.stringify(jQuery.pgwBrowser());
                         /// COLLECTION DATE
                         additionalData += '&CollectionDate=' + new Date().toISOString();
                         /// setTimeout: Need to wait a second or two to make sure Optimizely and GA object get loaded; any code not wrapped in setTime will get executed before code in setTimeout
                         setTimeout(function () {
                             /// GOOGLE ANALYTICS CLIENT ID
                             ///
                             /// ***Cant get from CM because ga object takes time to load so CM has to wait to pull the ga client id and the val is not available in CM when this file calls CM***
                             //if (cookieManager.gaClientID().length > 0) { additionalData += '&GoogleAnalyticsClientId=' + cookieManager.gaClientID(); }//.getCookieByName('uti_ga_clientid');
                             if (typeof ga === 'function') {
                                 if (typeof ga.getAll === 'function') {
                                     additionalData += '&GoogleAnalyticsClientId=' + ga.getAll()[0].get('clientId');
                                 }
                             }
                             ///
                             /// END GOOGLE ANALYTICS CLIENT ID
                             /// OPTIMIZELY
                             /// If CM is available, take all Optimizely data from CM, else try and get it from optimizely object              
                             /// Need to wait a second or two to make sure Optimizely object gets loaded; any code not wrapped in setTime will get executed before code in setTimeout
                             if (cm_OptAccID == undefined || cm_OptAccID.length == 0) {
         
                                 var optExpVarIDVals = '';
         
                                 if (typeof optimizely == 'object') {
                                     try {
                                         var optObj = optimizely;
                                         var optAccountId, optProjectId, activeExperimentIds, variationIds; //sectionId
                                         optAccountId = optimizely.getAccountId();
         
                                         if (optAccountId) {
                                             optProjectId = optimizely.getProjectId();
                                             activeExperimentIds = optimizely.data.state.activeExperiments;
         
                                             if (optimizely.data.state.activeExperiments.length > 0) {
                                                 if (optAccountId && cm_OptAccID.length < 1) {
                                                     additionalData += '&Opt_AccountId=' + optAccountId;
                                                 }
         
                                                 if (optProjectId && cm_optPrjID.length < 1) {
                                                     additionalData += '&Opt_ProjectId=' + optProjectId;
                                                 }
         
                                                 for (j = 0; j < activeExperimentIds.length; j++) {
                                                     var expID = activeExperimentIds[j];
                                                     var varID = optimizely.data.state.variationIdsMap[expID][0];
                                                     optExpVarIDVals = '&Opt_ExpId_VarId=' + expID + '_' + varID + '|';
                                                 }
         
                                                 if (optExpVarIDVals.length > 0) {
                                                     additionalData += optExpVarIDVals.substr(0, optExpVarIDVals.length - 1);
                                                 }
                                             }
                                             else {
                                                 /// No Active Experiments 
                                             }
                                         } else {
                                             /// No Optimizely Account ID available
                                         }
                                     } catch (e) { }
                                 } else {
                                     /// Optimizely not loaded
                                 }
                             }
                             ///
                             /// END OPTIMIZELY
                             /// ADD ADDITIONAL DATA TO LEAD OBJ
                             if (additionalData == null || additionalData.length === 0) {
                                 /// If no additional data
                                 return jQuery.param(_infos);         //rw added return value
                             } else {
                                 var appendAt = lead.AdditionalInfo.length;
                                 var _adPairs = additionalData.toLowerCase().replace('?', '').trim().replace(/^&{1}/, '').split('&');
                                 var _keyVals;
                                 for (var x = 0; x < _adPairs.length; x++) {
                                     _keyVals = _adPairs[x].split('=');
         
                                     /// If there is a key...
                                     if (_keyVals[0].length > 1) {
                                         if (_keyVals[0].indexOf('utm_content') > -1) {
                                             /// if utm_content is greater than 100 characters it causes an errer in the gateway, truncate                            
                                             var val = _keyVals[1].substring(0, 99);
                                             lead.AdditionalInfo[appendAt] = { Key: _keyVals[0], Value: val };
                                         } else {
                                             /// Add "&" back into Mkto cookie ID value to matain correct format.
         
                                             if (_keyVals[0] == undefined || _keyVals[0].length == 0) {
                                             } else if (_keyVals[0].toLowerCase().indexOf('marketo') > -1) {
                                                 lead.AdditionalInfo[appendAt] = { Key: _keyVals[0], Value: _keyVals[1].replace('|', '&') };
                                             } else {
                                                 lead.AdditionalInfo[appendAt] = { Key: _keyVals[0].replace('uti_', 'utm_'), Value: _keyVals[1] };
                                             }
                                         }
                                     }
         
                                     appendAt++;
                                 }
                             }
         
                         }, 2000);
         
                         //return jQuery.param(_infos);             //rw added return value        
                     }
                     catch (err) {
                         var appendAt = lead.AdditionalInfo.length;
                         lead.AdditionalInfo[appendAt] = { Key: 'utm_medium', Value: 'err: ' + err };
                         lead.AdditionalInfo[appendAt + 1] = { Key: 'utm_source', Value: 'err: ' + err };
                         lead.AdditionalInfo[appendAt + 2] = { Key: 'referrer', Value: 'err: ' + err };
                         lead.AdditionalInfo[appendAt + 3] = { Key: 'utm_campaign', Value: 'CMP-01032-Q0V1H0' };
                     }
         
                     return jQuery.param(_infos);             //rw added return value
                 },
         
                 // Very basic diagnosic method to check that all minimum requirements are populated on the object.
                 checkLead: function (consoleOutput) {
         
                     var errors = [];
                     if (null == this.element.data('lead').FormID) errors.push('FormID is required.');
                     if (null == this.element.data('lead').FirstName) errors.push('FirstName is required.');
                     if (null == this.element.data('lead').LastName) errors.push('LastName is required.');
                     if (null == this.element.data('lead').InterestCode) errors.push('InterestCode is required.');
         
                     if ((null == this.element.data('lead').EmailAddresses[0].Address) && (null == this.element.data('lead').PhoneNumbers[0].Number))
                         errors.push('An email address or phone number is required');
         
                     if (null == this.element.data('lead').Addresses[0].PostalCode) errors.push('A Postal code is required.');
         
                     if (consoleOutput) {
                         jQuery.each(errors, function (idx, val) {
                             if (console)
                                 console.log(val);
                         });
                     }
         
                     return errors;
                 },
         
                 _isLtIETen: function () {
         
                     if (navigator.appName == "Microsoft Internet Explorer") {
                         var ua = navigator.userAgent;
                         var re = new RegExp("MSIE ([0-9]{1,}[.0-9]{0,})");
                         if (re.exec(ua) != null) {
                             return parseInt(RegExp.jQuery1) < 10;
                         }
                     } else {
                         return false;
                     }
                 },
         
                 // TODO EXPOSE .done as a posted callback in options.
                 post: function () {
                     var jQueryajaxResult;
                     var self = this;
                     jQuery.ajax({
                         type: 'POST',
                         url: this.options.url,
                         data: JSON.stringify(this.element.data('lead')),
                         dataType: this.options.dataType,
                         contentType: this.options.contentType
                     })
                         // callback handler on success - populate lead object with returned data.
                         .done(function (response, textStatus, jqXHR) {
         
                             if (self.element.data('isLtIETen')) {
                                 var lead = jQuery.parseJSON(response);
                                 self.element.data('lead', lead);
                             }
                             else {
                                 self.element.data('lead', response);
                             }
                             self._trigger("posted", self.element.data('lead'));
         
                         })
                         // callback handler that will be called on failure
                         .fail(function (jqXHR, textStatus, errorThrown) { //.fail(function () {
                             // log the error to the console
                             //console.error("Error POSTing gateway object. Status:" + textStatus + " Error: " + errorThrown);
                             //console.error('jqXHR - ' + jqXHR);
                             self._trigger("failed", jqXHR);
                         })
                         .always(function (jqXHR, textStatus, errorThrown) {});
         
                     return self; // support chaining...
                 }
             });
         
             (function (jQuery) {
                 jQuery.pgwBrowser = function () {
                     var pgwBrowser = {};
                     pgwBrowser.userAgent = navigator.userAgent;
                     pgwBrowser.browser = {};
                     pgwBrowser.viewport = {};
                     pgwBrowser.os = {};
                     resizeEvent = null;
         
                     // The order of the following arrays is important, be careful if you change it.
         
                     var browserData = [
                         { name: 'Chromium', group: 'Chrome', identifier: 'Chromium/([0-9.]*)' },
                         { name: 'Chrome Mobile', group: 'Chrome', identifier: 'Chrome/([0-9.]*) Mobile', versionIdentifier: 'Chrome/([0-9.]*)' },
                         { name: 'Chrome', group: 'Chrome', identifier: 'Chrome/([0-9.]*)' },
                         { name: 'Chrome for iOS', group: 'Chrome', identifier: 'CriOS/([0-9.]*)' },
                         { name: 'Android Browser', group: 'Chrome', identifier: 'CrMo/([0-9.]*)' },
                         { name: 'Firefox', group: 'Firefox', identifier: 'Firefox/([0-9.]*)' },
                         { name: 'Opera Mini', group: 'Opera', identifier: 'Opera Mini/([0-9.]*)' },
                         { name: 'Opera', group: 'Opera', identifier: 'Opera ([0-9.]*)' },
                         { name: 'Opera', group: 'Opera', identifier: 'Opera/([0-9.]*)', versionIdentifier: 'Version/([0-9.]*)' },
                         { name: 'IEMobile', group: 'Explorer', identifier: 'IEMobile/([0-9.]*)' },
                         { name: 'Internet Explorer', group: 'Explorer', identifier: 'MSIE ([a-zA-Z0-9.]*)' },
                         { name: 'Internet Explorer', group: 'Explorer', identifier: 'Trident/([0-9.]*)', versionIdentifier: 'rv:([0-9.]*)' },
                         { name: 'Safari', group: 'Safari', identifier: 'Safari/([0-9.]*)', versionIdentifier: 'Version/([0-9.]*)' }
                     ];
         
                     var osData = [
                         { name: 'Windows 2000', group: 'Windows', identifier: 'Windows NT 5.0', version: '5.0' },
                         { name: 'Windows XP', group: 'Windows', identifier: 'Windows NT 5.1', version: '5.1' },
                         { name: 'Windows Vista', group: 'Windows', identifier: 'Windows NT 6.0', version: '6.0' },
                         { name: 'Windows 7', group: 'Windows', identifier: 'Windows NT 6.1', version: '7.0' },
                         { name: 'Windows 8', group: 'Windows', identifier: 'Windows NT 6.2', version: '8.0' },
                         { name: 'Windows 8.1', group: 'Windows', identifier: 'Windows NT 6.3', version: '8.1' },
                         { name: 'Windows Phone', group: 'Windows Phone', identifier: 'Windows Phone ([0-9.]*)', },
                         { name: 'Windows Phone', group: 'Windows Phone', identifier: 'Windows Phone OS ([0-9.]*)', },
                         { name: 'Windows', group: 'Windows', identifier: 'Windows', },
                         { name: 'Chrome OS', group: 'Chrome OS', identifier: 'CrOS', },
                         { name: 'Android', group: 'Android', identifier: 'Android', versionIdentifier: 'Android ([a-zA-Z0-9.-]*)' },
                         { name: 'iPad', group: 'iOS', identifier: 'iPad', versionIdentifier: 'OS ([0-9_]*)', versionSeparator: '[_|.]' },
                         { name: 'iPod', group: 'iOS', identifier: 'iPod', versionIdentifier: 'OS ([0-9_]*)', versionSeparator: '[_|.]' },
                         { name: 'iPhone', group: 'iOS', identifier: 'iPhone OS', versionIdentifier: 'OS ([0-9_]*)', versionSeparator: '[_|.]' },
                         { name: 'Mac OS X Cheetah', group: 'Mac OS', identifier: 'Mac OS X (10([_|.])0([0-9_.]*))', versionSeparator: '[_|.]' },
                         { name: 'Mac OS X Puma', group: 'Mac OS', identifier: 'Mac OS X (10([_|.])1([0-9_.]*))', versionSeparator: '[_|.]' },
                         { name: 'Mac OS X Jaguar', group: 'Mac OS', identifier: 'Mac OS X (10([_|.])2([0-9_.]*))', versionSeparator: '[_|.]' },
                         { name: 'Mac OS X Panther', group: 'Mac OS', identifier: 'Mac OS X (10([_|.])3([0-9_.]*))', versionSeparator: '[_|.]' },
                         { name: 'Mac OS X Tiger', group: 'Mac OS', identifier: 'Mac OS X (10([_|.])4([0-9_.]*))', versionSeparator: '[_|.]' },
                         { name: 'Mac OS X Leopard', group: 'Mac OS', identifier: 'Mac OS X (10([_|.])5([0-9_.]*))', versionSeparator: '[_|.]' },
                         { name: 'Mac OS X Snow Leopard', group: 'Mac OS', identifier: 'Mac OS X (10([_|.])6([0-9_.]*))', versionSeparator: '[_|.]' },
                         { name: 'Mac OS X Lion', group: 'Mac OS', identifier: 'Mac OS X (10([_|.])7([0-9_.]*))', versionSeparator: '[_|.]' },
                         { name: 'Mac OS X Mountain Lion', group: 'Mac OS', identifier: 'Mac OS X (10([_|.])8([0-9_.]*))', versionSeparator: '[_|.]' },
                         { name: 'Mac OS X Mavericks', group: 'Mac OS', identifier: 'Mac OS X (10([_|.])9([0-9_.]*))', versionSeparator: '[_|.]' },
                         { name: 'Mac OS', group: 'Mac OS', identifier: 'Mac OS' },
                         { name: 'Ubuntu', group: 'Linux', identifier: 'Ubuntu', versionIdentifier: 'Ubuntu/([0-9.]*)' },
                         { name: 'Debian', group: 'Linux', identifier: 'Debian', },
                         { name: 'Gentoo', group: 'Linux', identifier: 'Gentoo', },
                         { name: 'Linux', group: 'Linux', identifier: 'Linux', },
                         { name: 'BlackBerry', group: 'BlackBerry', identifier: 'BlackBerry', }
                     ];
         
                     //  Set browser data
                     var setBrowserData = function () {
                         var userAgent = pgwBrowser.userAgent.toLowerCase();
                         // Check browser type
                         for (i in browserData) {
                             var browserRegExp = new RegExp(browserData[i].identifier.toLowerCase());
                             var browserRegExpResult = browserRegExp.exec(userAgent);
         
                             if (browserRegExpResult != null && browserRegExpResult[1]) {
                                 pgwBrowser.browser.name = browserData[i].name;
                                 pgwBrowser.browser.group = browserData[i].group;
         
                                 // Check version
                                 if (browserData[i].versionIdentifier) {
                                     var versionRegExp = new RegExp(browserData[i].versionIdentifier.toLowerCase());
                                     var versionRegExpResult = versionRegExp.exec(userAgent);
         
                                     if (versionRegExpResult != null && versionRegExpResult[1]) {
                                         setBrowserVersion(versionRegExpResult[1]);
                                     }
         
                                 } else {
                                     setBrowserVersion(browserRegExpResult[1]);
                                 }
         
                                 break;
                             }
                         }
                         return true;
                     };
         
                     // Set browser version
                     var setBrowserVersion = function (version) {
                         var splitVersion = version.split('.', 2);
                         pgwBrowser.browser.fullVersion = version;
         
                         // Major version
                         if (splitVersion[0]) {
                             pgwBrowser.browser.majorVersion = parseInt(splitVersion[0]);
                         }
         
                         // Minor version
                         if (splitVersion[1]) {
                             pgwBrowser.browser.minorVersion = parseInt(splitVersion[1]);
                         }
         
                         return true;
                     };
         
                     //  Set OS data
                     var setOsData = function () {
                         var userAgent = pgwBrowser.userAgent.toLowerCase();
         
                         // Check browser type
                         for (i in osData) {
                             var osRegExp = new RegExp(osData[i].identifier.toLowerCase());
                             var osRegExpResult = osRegExp.exec(userAgent);
         
                             if (osRegExpResult != null) {
                                 pgwBrowser.os.name = osData[i].name;
                                 pgwBrowser.os.group = osData[i].group;
         
                                 // Version defined
                                 if (osData[i].version) {
                                     setOsVersion(osData[i].version, (osData[i].versionSeparator) ? osData[i].versionSeparator : '.');
         
                                     // Version detected
                                 } else if (osRegExpResult[1]) {
                                     setOsVersion(osRegExpResult[1], (osData[i].versionSeparator) ? osData[i].versionSeparator : '.');
         
                                     // Version identifier
                                 } else if (osData[i].versionIdentifier) {
                                     var versionRegExp = new RegExp(osData[i].versionIdentifier.toLowerCase());
                                     var versionRegExpResult = versionRegExp.exec(userAgent);
         
                                     if (versionRegExpResult != null && versionRegExpResult[1]) {
                                         setOsVersion(versionRegExpResult[1], (osData[i].versionSeparator) ? osData[i].versionSeparator : '.');
                                     }
                                 }
         
                                 break;
                             }
                         }
         
                         return true;
                     };
         
                     // Set OS version
                     var setOsVersion = function (version, separator) {
                         if (separator.substr(0, 1) == '[') {
                             var splitVersion = version.split(new RegExp(separator, 'g'), 2);
                         } else {
                             var splitVersion = version.split(separator, 2);
                         }
         
                         if (separator != '.') {
                             version = version.replace(new RegExp(separator, 'g'), '.');
                         }
         
                         pgwBrowser.os.fullVersion = version;
         
                         // Major version
                         if (splitVersion[0]) {
                             pgwBrowser.os.majorVersion = parseInt(splitVersion[0]);
                         }
         
                         // Minor version
                         if (splitVersion[1]) {
                             pgwBrowser.os.minorVersion = parseInt(splitVersion[1]);
                         }
         
                         return true;
                     };
         
                     // Set viewport size
                     var setViewportSize = function (init) {
                         pgwBrowser.viewport.width = jQuery(window).width();
                         pgwBrowser.viewport.height = jQuery(window).height();
         
                         // Resize triggers
                         if (typeof init == undefined) {
                             if (resizeEvent == null) {
                                 jQuery(window).trigger('PgwBrowser::StartResizing');
                             } else {
                                 clearTimeout(resizeEvent);
                             }
         
                             resizeEvent = setTimeout(function () {
                                 jQuery(window).trigger('PgwBrowser::StopResizing');
                                 clearTimeout(resizeEvent);
                                 resizeEvent = null;
                             }, 300);
                         }
         
                         return true;
                     };
         
                     // Set viewport orientation
                     var setViewportOrientation = function () {
                         if (typeof window.orientation == undefined) {
         
                             if (pgwBrowser.viewport.width >= pgwBrowser.viewport.height) {
                                 pgwBrowser.viewport.orientation = 'landscape';
                             } else {
                                 pgwBrowser.viewport.orientation = 'portrait';
                             }
         
                         } else {
                             switch (window.orientation) {
                                 case -90:
                                 case 90:
                                     pgwBrowser.viewport.orientation = 'landscape';
                                     break;
                                 default:
                                     pgwBrowser.viewport.orientation = 'portrait';
                                     break;
                             }
                         }
         
                         // Orientation trigger
                         jQuery(window).trigger('PgwBrowser::OrientationChange');
         
                         return true;
                     };
         
                     // Replace default value for the user-agent tester on pgwjs.com
                     //if (typeof window.pgwJsUserAgentTester != undefined) {
                     //    pgwBrowser.userAgent = window.pgwJsUserAgentTester;
                     //}
         
                     // Initialization
                     setBrowserData();
                     setOsData();
                     setViewportSize(true);
                     setViewportOrientation();
         
                     // Triggers
                     jQuery(window).on('orientationchange', function (e) {
                         setViewportOrientation();
                     });
         
                     jQuery(window).resize(function (e) {
                         setViewportSize();
                     });
         
                     return pgwBrowser;
                 }
             })(window.Zepto || window.jQuery);
         
             /*
               jQuery deparam is an extraction of the deparam method from Ben Alman's jQuery BBQ
               http://benalman.com/projects/jquery-bbq-plugin/
             */
             (function (jQuery) {
                 jQuery.deparam = function (params, coerce) {
                     var obj = {},
                         coerce_types = { 'true': !0, 'false': !1, 'null': null };
         
                     // Iterate over all name=value pairs.
                     jQuery.each(params.replace(/\+/g, ' ').split('&'), function (j, v) {
                         var param = v.split('='),
                             key = decodeURIComponent(param[0]),
                             val,
                             cur = obj,
                             i = 0,
         
                             // If key is more complex than 'foo', like 'a[]' or 'a[b][c]', split it
                             // into its component parts.
                             keys = key.split(']['),
                             keys_last = keys.length - 1;
         
                         // If the first keys part contains [ and the last ends with ], then []
                         // are correctly balanced.
                         if (/\[/.test(keys[0]) && /\]jQuery/.test(keys[keys_last])) {
                             // Remove the trailing ] from the last keys part.
                             keys[keys_last] = keys[keys_last].replace(/\]jQuery/, '');
         
                             // Split first keys part into two parts on the [ and add them back onto
                             // the beginning of the keys array.
                             keys = keys.shift().split('[').concat(keys);
         
                             keys_last = keys.length - 1;
                         } else {
                             // Basic 'foo' style key.
                             keys_last = 0;
                         }
         
                         // Are we dealing with a name=value pair, or just a name?
                         if (param.length === 2) {
                             val = decodeURIComponent(param[1]);
         
                             // Coerce values.
                             if (coerce) {
                                 val = val && !isNaN(val) ? +val              // number
                                     : val === undefined ? undefined         // undefined
                                         : coerce_types[val] !== undefined ? coerce_types[val] // true, false, null
                                             : val;                                                // string
                             }
         
                             if (keys_last) {
                                 // Complex key, build deep object structure based on a few rules:
                                 // * The 'cur' pointer starts at the object top-level.
                                 // * [] = array push (n is set to array length), [n] = array if n is 
                                 //   numeric, otherwise object.
                                 // * If at the last keys part, set the value.
                                 // * For each keys part, if the current level is undefined create an
                                 //   object or array based on the type of the next keys part.
                                 // * Move the 'cur' pointer to the next level.
                                 // * Rinse & repeat.
                                 for (; i <= keys_last; i++) {
                                     key = keys[i] === '' ? cur.length : keys[i];
                                     cur = cur[key] = i < keys_last
                                         ? cur[key] || (keys[i + 1] && isNaN(keys[i + 1]) ? {} : [])
                                         : val;
                                 }
         
                             } else {
                                 // Simple key, even simpler rules, since only scalars and shallow
                                 // arrays are allowed.
         
                                 if (jQuery.isArray(obj[key])) {
                                     // val is already an array, so push on the next value.
                                     obj[key].push(val);
         
                                 } else if (obj[key] !== undefined) {
                                     // val isn't an array, but since a second value has been specified,
                                     // convert val into an array.
                                     obj[key] = [obj[key], val];
         
                                 } else {
                                     // val is a scalar.
                                     obj[key] = val;
                                 }
                             }
         
                         } else if (key) {
                             // No value was defined, so set something meaningful.
                             obj[key] = coerce
                                 ? undefined
                                 : '';
                         }
                     });
         
                     return obj;
                 };
             })(jQuery);
         
             /// Function checks to see if checkFor value exists in the JSON array arrayToSearch
             function jsonArrayCheck(arrayToSearch, checkFor) {
                 for (var k = 0; k < arrayToSearch.length - 1; k++) {
                     if (arrayToSearch[k].indexOf(checkFor) > -1) {
                         return true;
                     }
                 }
                 return false;
             }
         
             /// Function returns the value for the searchFor key if it exists in the JSON array arrayToSearch
             function jsonArraySearch(arrayToSearch, searchFor) {
                 for (var k = 0; k < arrayToSearch.length - 1; k++) {
                     if (arrayToSearch[k].indexOf(searchFor) > -1) {
                         var keyValPair = arrayToSearch[k];
                         var keyValArray = keyValPair.split('=');
                         return keyValArray[1];
                     }
                 }
                 return false;
             }
         
         })(jQuery);
         /********** END JQUERY.UTI.UI **********/
         
         
         
         
         /********** FORMS COMMON **********/
         /* 
           formsCommon contains contains 
           common javascript used by all forms which post to gateway. 
         */
         
         var _state = "";
         var _tries = 0;
         var _formID = "";
         var _aoi = "";
         var _TYPath = "";
         var usPostalPattern = "^\\d{5}(-\\d{4})?$";
         var usPhPattern = '^\\(?([2-9]{1}\\d{9})|([2-9]\\d{2}(-| |\\.|\\)\\W?)\\d{3}(-| |\\.)?\\d{4})$';
         var intPhPattern = '^\\+?((9[976]\\d|8[987530]\\d|6[987]\\d|5[90]\\d|42\\d|3[875]\\d|2[98654321]\\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\\W*\\d\\W*\\d\\W*\\d\\W*\\d\\W*\\d\\W*\\d\\W*\\d\\W*\\d\\W*(\\d{1,3})|(\\d{12,15}))$';
         
         
         function getAcquisitionPoint() {
             var location = document.location.pathname;
             if (location.indexOf('form15') > -1 || location.indexOf('request-info') > -1) {
                 return 'MARKETO004';
             } else if (location.indexOf('form16') > -1) {
                 return 'MARKETO007';
             } else if (location.indexOf('form17') > -1) {
                 return 'MARKETO007';
             } else if (location.indexOf('form18') > -1) {
                 return 'MARKETO007';
             } else if (location.indexOf('cnc') > -1 || location.indexOf('form19') > -1) {
                 return 'MARKETO007';
             } else if (location.indexOf('weld') > -1 || location.indexOf('form20') > -1) {
                 return 'MARKETO007';
             } else if (location.indexOf('/fbn/') > -1) {
                 return 'MARKETO009';
             } else { return 'MARKETO000'; }
         }
         
         function getDestination() {
             /// Default Destination is MCC
             /// Adult, non-Military – Not from UTAH = Sis
             /// - Greater than 19 yrs old
             /// - Not Active Military or Veteran
             /// - Postal code not in UTAH
             /// Adult, non-Military – From UTAH = Sis
             /// - Greater than 20 yrs old
             /// - Not Active Military or Veteran
             /// - Postal code in UTAH
             var destination = 'MCC';
             var mil = jQuery("input[name=ActiveMilitary]:checked").val() == "true";
             var age = jQuery('#ageSelect').val();
             var fromUtah = utahCheck();
         
             if (!mil) {
                 if (age >= 21) {
                     destination = 'Sis'
                 } else if (!fromUtah && age >= 20) {
                     destination = 'Sis'
                 }
             }
             return destination;
         }
         
         function utahCheck() {
             if (_state.length > 0 && _state == "UT") {
                 return true;
             } else { return false; }
         }
         
         function _s4() {
             return Math.floor((1 + Math.random()) * 0x10000)
                 .toString(16)
                 .substring(1);
         }
         
         function _guid() {
             return _s4() + _s4() + '-' + _s4() + '-' + _s4() + '-' +
                 _s4() + '-' + _s4() + _s4() + _s4();
         }
         
         /// Initializes jQuery UI properties and posts form to gateway
         function submitForm(uiForm, uid, ip) {
         
             /// Element Mapping
             uiForm.data('lead').FormID = _formID;
             uiForm.data('lead').InterestCode = _aoi;
         
             uiForm.data('lead').FirstName = jQuery("#inputFName").val();
             uiForm.data('lead').LastName = jQuery("#inputLName").val();
             uiForm.data('lead').EmailAddresses[0].Address = jQuery("#inputEmail").val();
             uiForm.data('lead').Addresses[0].PostalCode = jQuery("#inputPostal").val();
             uiForm.data('lead').Addresses[0].AddressType = "L";
             uiForm.data('lead').Addresses[0].AddressLine1 = "";
             uiForm.data('lead').Addresses[0].City = "";
             uiForm.data('lead').Addresses[0].State = "";
             uiForm.data('lead').Addresses[0].Country = jQuery("#countrySelect").val();
         
             /// phone numbers
             uiForm.data('lead').PhoneNumbers[0].Number = jQuery("#inputPhone").val().replace(/\D/g, '');
             uiForm.data('lead').PhoneNumbers[0].Type = "C";
             uiForm.data('lead').PhoneNumbers[1].Number = "";
             uiForm.data('lead').PhoneNumbers[1].Type = "L";
         
             /// education
             uiForm.data('lead').Education = jQuery("#eduSelect").val();
             uiForm.data('lead').GradMonth = jQuery('#inputGradYear').val().length > 0 ? "6" : "";
             uiForm.data('lead').GradYear = jQuery('#inputGradYear').val();
             uiForm.data('lead').HighSchoolID = "";
             uiForm.data('lead').HighSchoolState = "";
             uiForm.data('lead').HighSchoolCity = "";
             uiForm.data('lead').HighSchoolNotListed = "";
         
             /// military
             uiForm.data('lead').IsMilitary = jQuery("input[name='ActiveMilitary']:checked").val();
             uiForm.data('lead').MilitaryID = "";
             uiForm.data('lead').MilitarySeparation = jQuery('#rdoMilYes').is(':checked') == true ? jQuery('#inputMilReleaseYear').val() + '/' + jQuery('#milReleaseMonthSelect option:selected').attr('value') + '/01' : '';
             uiForm.data('lead').MilitaryBaseState = "";
             uiForm.data('lead').InstallationNotListed = "";
         
             uiForm.data('lead').Comments = "";
         
             /// set isComplete to true
             /// keys for these values are added in jquery.uti.ui.forms.js
             uiForm.data('lead').AdditionalInfo[0].Value = true; /// isComplete
             uiForm.data('lead').AdditionalInfo[1].Value = uid;  /// lead guid
             uiForm.data('lead').AdditionalInfo[2].Value = ip;   /// ip address
             uiForm.data('lead').AdditionalInfo[3].Value = jQuery('#ageSelect').val();   /// age
             uiForm.data('lead').AdditionalInfo[4].Value = getDestination(); /// Destination
             uiForm.data('lead').IPAddress = ip;
         
             uiForm.uiForm('post');
         }
         
         function attachBtnSubmitClick() {
             jQuery('#getInfoBtn').on('click.uti', function () {
                 "use strict";
                 /// Remove click event from button to prevent double click
                 $(this).off('.uti');
                 /// Disable button to change style
                 $(this).attr('disabled', 'true');
                 $('#leadForm').foundation('validateForm');
             });
         }
         
         function toggleGradYear(eduVal) {
             if (eduVal.length > 0 && eduVal != 'none') {
                 jQuery('#gradYearWrapper').fadeIn(800);
                 jQuery('#inputGradYear').removeAttr('data-abide-ignore');
             } else {
                 jQuery('#gradYearWrapper').fadeOut(800);
                 jQuery('#inputGradYear').val('');
                 jQuery('#inputGradYear').attr('data-abide-ignore', 'true');
             }
         }
         
         function toggleMilReleaseYear() {
             if (jQuery('#rdoMilYes').is(':checked')) {
                 jQuery('#milReleaseDateWrapper').fadeIn(800);
                 jQuery('#inputMilReleaseYear, #milReleaseMonthSelect').removeAttr('data-abide-ignore');
             } else {
                 jQuery('#milReleaseDateWrapper').fadeOut(800);
                 jQuery('#inputMilReleaseYear, #milReleaseMonthSelect').attr('data-abide-ignore', 'true');
                 jQuery('#inputMilReleaseYear').val('');
                 jQuery('#milReleaseMonthSelect').prop('selectedIndex', 0);
             }
         }
         
         jQuery(document).ready(function () {
         
             /// Init Age select
             jQuery('#ageSelect').append('<option value=""> - SELECT - </option>');
             for (var x = 14; x < 81; x++) {
                 jQuery('#ageSelect').append('<option value="' + x + '">' + x + '</option>');
             }
         
             /// Get public IP
             var ip = '';
             if (typeof jQuery !== 'undefined') {
                 jQuery(function () {
                     jQuery.getJSON('https://api.ipify.org?format=jsonp&callback=?',
                         function (json) {
                             ip = json.ip;
                         }
                     );
                 });
             }
         
             /// Init jQuery UI form widget plugin
             var uiForm = jQuery('#leadForm').uiForm({
                 // url: activeEnvironment.active + '/api/leads',
                 posted: function (evt) {
                     var dataLayer = window.dataLayer = window.dataLayer || [];
         
                     /// HTML- Facebook Pixel tag
                     //dataLayer.push({
                     //    'dl_Email': jQuery("#email").val(),
                     //    'dl_FName': jQuery("#firstName").val(),
                     //    'dl_LName': jQuery("#lastName").val(),
                     //    'dl_Phone': jQuery("#phone1").val() + jQuery("#phone2").val() + jQuery("#phone3").val(),
                     //    'dl_InqID': cookieManager.inqID()
                     //});
         
                     /// Create cookie with email address for C3 Metrics Leads Conversion Tag to use on thank you page
                     var expiry = new Date(new Date().getTime() + 1 * 86400000).toUTCString();
                     var tmp = document.domain.split('.');
                     var rootDomain = '.uti.edu';
                     if (tmp.length === 3) { rootDomain = '.' + tmp[1] + '.' + tmp[2]; } else { rootDomain = '.' + tmp[0] + '.' + tmp[1]; }
                     document.cookie = 'c3mEmail=' + jQuery("#inputEmail").val(); + '; expires=' + expiry + '; path=/; domain=' + rootDomain + ';';
                     /// End
         
                     // Event for GTM DC - Inquiry Conversion tag
                     dataLayer.push({
                         'event': 'formSuccess',
                         'acqusitionPoint': getAcquisitionPoint(),
                         'action': 'Submitted',
                         'eventLabel': _aoi
                     });
         
                     var host = "//" + window.location.host;
                     window.location.replace(host + _TYPath);
                 },
                 failed: function (evt) {
                     _tries++;
                     if (_tries < 2) {
                         /// Try again...
                         jQuery('#getInfoBtn').trigger('click');
                     }
                     else {
                         /// Give up... 
                         jQuery("#getInfoBtn").before('<p class="text-center is-invalid-label"><strong>Sorry there has been an error in the form. Please try again later.</strong></p>');
                     }
                     evt.preventDefault();
                     return false;
                 }
             });
         
             jQuery('#inputPostal').zipCompleter({});
         
             jQuery('#getInfoBtn').parent().css('text-align', 'center');
         
         
         
             /// Page Events
         
             /// Show TCPA language section when valid phone number is entered
             jQuery('#inputPhone').on("valid.zf.abide", function (ev, elem) {
                 jQuery('.tcpa').fadeIn(800);
             });
         
             /// Change postal code input type based on country
             jQuery('#countrySelect').change(function () {
                 if (jQuery(this).val() != 'US') {
                     //jQuery('#inputPostal').attr('placeholder', 'postal code').attr('maxlength', '10').attr('type', 'text').attr('pattern', 'alpha_numeric');
                     jQuery('#inputPostal').attr('placeholder', 'postal code').attr('pattern', 'alpha_numeric');
                     jQuery('#inputPhone').attr('placeholder', 'phone number').attr('maxlength', 'none').attr('pattern', intPhPattern);
                 } else {
                     //jQuery('#inputPostal').attr('placeholder', '55555').attr('maxlength', '5').attr('type', 'tel').attr('pattern', '^[1-9]{5}$');
                     jQuery('#inputPostal').attr('placeholder', '55555').attr('pattern', usPostalPattern);
                     jQuery('#inputPhone').attr('placeholder', '555 555 5555').attr('maxlength', '12').attr('pattern', usPhPattern);
                 }
             });
         
             /// On Age changes show/hide military/education sections
             jQuery('#ageSelect').change(function () {
                 var age = jQuery('#ageSelect option:selected').text();
         
                 if (age <= 19) {
                     jQuery('#eduWrapper').show();
                     jQuery('#eduSelect').removeAttr('data-abide-ignore');
                 } else if (age > 19) {
                     jQuery('#eduWrapper').hide();
                     jQuery('#eduSelect').prop('selectedIndex', 0);
                     jQuery('#eduSelect').attr('data-abide-ignore', 'true');
                     toggleGradYear(jQuery('#eduSelect').val());
                 }
         
                 if (age < 18) {
                     jQuery('#milStatusWrapper').hide();
                     jQuery('#milStatusWrapper input').attr('checked', false);
                     jQuery('input[name="ActiveMilitary"]').attr('data-abide-ignore', 'true');
                     toggleMilReleaseYear();
                 } else if (age >= 18) {
                     jQuery('#milStatusWrapper').show();
                     jQuery('input[name="ActiveMilitary"]').removeAttr('data-abide-ignore', 'true');
                     toggleMilReleaseYear();
                 }
             });
         
             /// Show/Hide grad year
             jQuery('#eduSelect').change(function () {
                 toggleGradYear($(this).val());
             });
         
             /// Show/Hide military release date
             jQuery('input[name="ActiveMilitary"]').click(function () {
                 toggleMilReleaseYear();
             });
         
             /// Submit button click event
             attachBtnSubmitClick();
         
             /// Validation failed event
             jQuery(document).on("forminvalid.zf.abide", function (ev, frm) {
                 ev.preventDefault();
         
                 /// Set focus and move screen to first error element for better mobile experience
                 var invalid_fields = jQuery(this).find('[data-invalid]');
         
                 /// re-attach submit button if form validation failed
                 attachBtnSubmitClick();
                 /// Renable to update button style
                 $('#getInfoBtn').removeAttr('disabled');
         
                 jQuery(document).ready(function () {
                     jQuery("html, body").animate({ scrollTop: jQuery(invalid_fields).offset().top - 20 }, 500);
                 });
                 jQuery(invalid_fields).first().focus();
             })
         
             /// Validate passed event
             jQuery(document).on("formvalid.zf.abide", function (ev, frm) {
                 /// Get public IP;
                 if (typeof cookieManager == 'object') {
                     uid = cookieManager.getCookieByName('uti_inqid');
                 }
                 else {
                     uid = _guid();
                 }
                 submitForm(uiForm, uid, ip);
             });
         });
         /* End formsCommon */
         /********** END FORMS COMMON **********/
      </script><script type="text/javascript">
         /// Form15 custom js
         
         function areaOfInterestVal(name) {
             if (name.toString().toLowerCase().indexOf('auto') >= 0) {
                 return "AU";
             } else if (name.toString().toLowerCase().indexOf('moto') >= 0) {
                 return "MO";
             } else if (name.toString().toLowerCase().indexOf('diesel') >= 0) {
                 return "DI";
             } else if (name.toString().toLowerCase().indexOf('col') >= 0) {
                 return "CR";
             } else if (name.toString().toLowerCase().indexOf('nascar') >= 0) {
                 return "NASCAR";
             } else if (name.toString().toLowerCase().indexOf('marine') >= 0) {
                 return "MA";
             } else if (name.toString().toLowerCase().indexOf('cnc') >= 0) {
                 return "CNC";
             } else if (name.toString().toLowerCase().indexOf('weld') >= 0) {
                 return "WELD";
             } else {
                 return "AU";
             }
         }
         
         function toggleProgDescs(program) {
             if (program.indexOf('auto') > -1) {
                 jQuery('.program-description').fadeOut(800, function () {
                     jQuery('.program-description #program-title').text('Automotive');
                     jQuery('.program-description p').eq(0).text("In this program, you'll learn how to diagnose, maintain and repair domestic and foreign automobiles. You will find out how to troubleshoot problems of all kinds, using the state-of-the-industry engine analyzers, handheld scanners and other computerized diagnostic equipment. You'll learn everything from basic engine systems to computerized fuel injection, anti-lock brakes, passenger restraint systems, computerized engine controls and much more.");
                     jQuery('.program-description p').eq(1).text('Simply answer a few questions below to have one of our Admissions Representatives contact you about technician training opportunities.');
                     jQuery('.program-description .button-special').attr('name', 'AutomotiveBtn');
                 });
                 jQuery('.program-description').fadeIn(600);
             } else if (program.indexOf('coll') > -1) {
                 jQuery('.program-description').fadeOut(800, function () {
                     jQuery('.program-description #program-title').text('Collision Repair & Refinish');
                     jQuery('.program-description p').eq(0).text("If you have the passion to reshape and restore a wide range of vehicles, you’re ready for UTI’s Collision Repair & Refinish Technology program. UTI’s ASE-certified instructors and curriculum, developed in cooperation with I-CAR, will teach you the hands-on skills needed to pursue a career as a collision repair technician.");
                     jQuery('.program-description p').eq(1).text('Simply answer a few questions below to have one of our Admissions Representatives contact you about technician training opportunities.');
                     jQuery('.program-description .button-special').attr('name', 'CollisionBtn');
                 });
                 jQuery('.program-description').fadeIn(600);
             } else if (program.indexOf('diesel') > -1) {
                 jQuery('.program-description').fadeOut(800, function () {
                     jQuery('.program-description #program-title').text('Diesel');
                     jQuery('.program-description p').eq(0).text("UTI’s Diesel Technology program gives you hands-on training that prepares you for an in-demand career. You’ll work on diesel engines, commercial vehicles and heavy-equipment systems that feature advanced computer controls and electronic functions.");
                     jQuery('.program-description p').eq(1).text('Simply answer a few questions below to have one of our Admissions Representatives contact you about technician training opportunities.');
                     jQuery('.program-description .button-special').attr('name', 'DieselBtn');
                 });
                 jQuery('.program-description').fadeIn(600);
             } else if (program.indexOf('marine') > -1) {
                 jQuery('.program-description').fadeOut(800, function () {
                     jQuery('.program-description #program-title').text('Marine');
                     jQuery('.program-description p').eq(0).text("If you love the water and want to pursue a career in the marine industry, you belong at Marine Mechanics Institute. Learn to diagnose, service and repair marine mechanical systems through a combination of hands-on work in the lab and classroom instruction utilizing state-of-the-industry technology and equipment. Work with leading marine brands on inboard gas and diesel engines, outboard 2-stroke and 4-stroke motors, and sterndrives.");
                     jQuery('.program-description p').eq(1).text('Simply answer a few questions below to have one of our Admissions Representatives contact you about technician training opportunities.');
                     jQuery('.program-description .button-special').attr('name', 'MarineBtn');
                 });
                 jQuery('.program-description').fadeIn(600);
             } else if (program.indexOf('moto') > -1) {
                 jQuery('.program-description').fadeOut(800, function () {
                     jQuery('.program-description #program-title').text('Motorcycle');
                     jQuery('.program-description p').eq(0).text("Street bikes, sport bikes, dirt bikes—Motorcycle Mechanics Institute (MMI) has them all. You will learn motorcycle theory, engine troubleshooting and diagnosis, drivability, and performance testing on leading brands in the industry. MMI’s training curriculum is designed to meet the needs of a changing industry, working with manufacturers to make sure that your training prepares you for career in the motorcycle industry.");
                     jQuery('.program-description p').eq(1).text('Simply answer a few questions below to have one of our Admissions Representatives contact you about technician training opportunities.');
                     jQuery('.program-description .button-special').attr('name', 'MotorcycleBtn');
                 });
                 jQuery('.program-description').fadeIn(600);
             } else if (program.indexOf('nascar') > -1) {
                 jQuery('.program-description').fadeOut(800, function () {
                     jQuery('.program-description #program-title').text('NASCAR');
                     jQuery('.program-description p').eq(0).text("The Automotive Technology program at NASCAR Tech will teach you how to diagnose, maintain and repair domestic and foreign automobiles. You'll learn basic engine systems, computerized fuel injection, anti-lock brakes, passenger restraint systems, computerized engine controls and more.  After your core training, you can add the NASCAR Technology elective where you learn everything from high-performance engines, fabrication and welding, to aerodynamics and pit crew essentials.");
                     jQuery('.program-description p').eq(1).text('Simply answer a few questions below to have one of our Admissions Representatives contact you about technician training opportunities.');
                     jQuery('.program-description .button-special').attr('name', 'NASCARBtn');
                 });
                 jQuery('.program-description').fadeIn(600);
             } else if (program.indexOf('cnc') > -1) {
                 jQuery('.program-description').fadeOut(800, function () {
                     jQuery('.program-description #program-title').text('CNC Machining');
                     jQuery('.program-description p').eq(0).text("From mass-produced car parts to precision parts used in aviation, CNC (computer numeric controlled) machinists can make a variety of products. The CNC Machining Technology program at NASCAR Tech will teach you a wide range of skills such as programming and operating CNC lathes and mills, reading blueprints and interpreting geometric dimensioning, and continuous improvement practices to make you an innovative, effective and efficient machinist.");
                     jQuery('.program-description p').eq(1).text('Simply answer a few questions below to have one of our Admissions Representatives contact you about training opportunities.');
                     jQuery('.program-description .button-special').attr('name', 'CNCBtn');
                 });
                 jQuery('.program-description').fadeIn(600);
             } else if (program.indexOf('welding') > -1) {
                 jQuery('.program-description').fadeOut(800, function () {
                     jQuery('.program-description #program-title').text('Welding');
                     jQuery('.program-description p').eq(0).text("All across America, you’ll find welders at work in all types of places: shops, bridges, buildings, pipelines, power plants, refineries and aerospace manufacturers. UTI’s Welding Technology program prepares students for American Welding Society certification with hands-on training developed in collaboration with Lincoln Electric, a global leader in the welding industry.");
                     jQuery('.program-description p').eq(1).text('Simply answer a few questions below to have one of our Admissions Representatives contact you about training opportunities.');
                     jQuery('.program-description .button-special').attr('name', 'WeldingBtn');
                 });
                 jQuery('.program-description').fadeIn(600);
             } else if (program.indexOf('un') > -1) {
                 jQuery('.program-description').fadeOut(800, function () {
                     jQuery('.program-description #program-title').text('Undecided');
                     jQuery('.program-description p').eq(0).text("If you’re not sure which program you’re most interested in, or if you’d like information on more than one program, click “next” below to answer a few questions and one of our Admissions Representatives will contact you. They can help you find the program that is right for you.");
                     jQuery('.program-description p').eq(1).text('Simply answer a few questions below to have one of our Admissions Representatives contact you about technician training opportunities.');
                     jQuery('.program-description .button-special').attr('name', 'UndecidedBtn');
                 });
                 jQuery('.program-description').fadeIn(600);
             }
         }
         
         function detectIE() {
             var ua = window.navigator.userAgent;
         
             // Test values; Uncomment to check result …
         
             // IE 10
             // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';
         
             // IE 11
             // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';
         
             // Edge 12 (Spartan)
             // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';
         
             // Edge 13
             // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';
         
             var msie = ua.indexOf('MSIE ');
             if (msie > 0) {
                 // IE 10 or older => return version number
                 return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
             }
         
             var trident = ua.indexOf('Trident/');
             if (trident > 0) {
                 // IE 11 => return version number
                 var rv = ua.indexOf('rv:');
                 return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
             }
         
             var edge = ua.indexOf('Edge/');
             if (edge > 0) {
                 // Edge (IE 12+) => return version number
                 return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
             }
         
             // other browser
             return false;
         }
         
         jQuery(document).ready(function () {
             /// Set page specific values for submitForm() in formsCommon.js
             _formID = "form15"
             _TYPath = "/forms/thankyou";
         
             if (detectIE()) {
                 $('.button-special').addClass('ie');
             }
         
             /// Page Events
             /// Accordion Click Event (mobile)
             jQuery('.accordion').on('down.zf.accordion', function () {
                 var open = jQuery('.accordion-item.is-active');
                 jQuery('html, body').stop().animate({
                     'scrollTop': jQuery(open).offset().top + 20
                 }, 1000);
             });
         
             /// Program-list Click Event (desktop)
             jQuery('#programList li').click(function () {
                 jQuery('#programList li').removeClass('open');
                 jQuery('#programList input[type="radio"]').prop('checked', false);
                 jQuery(this).addClass('open');
                 jQuery(this).children('input[type="radio"]').prop('checked', true);
                 var program = jQuery(this).children('.program-title').text().toLowerCase();
                 if (jQuery('#programImageWrapper').css('display') != 'none') {
                     jQuery('#programImageWrapper').fadeOut(800, function () {
                         toggleProgDescs(program);
                     });
                 } else {
                     toggleProgDescs(program);
                 }
             });
         
             /// Back Button Click Event
             jQuery('#backBtn').click(function () {
                 if (jQuery(window).width() < 992) {
                     /// Mobile
                     jQuery('.progress-bar-container .wide-num-stripe.two').removeClass('blue-progress');
                     jQuery('.progress-bar-container .num-circle.two').removeClass('blue-progress');
                     jQuery('.lead-in').fadeOut(800, function () {
                         jQuery('.lead-in').text('Take the next step, explore and choose a program by clicking next in a section below.');
                     }).fadeIn(800);
                     jQuery('.intro-header').fadeOut(800, function () {
                         jQuery(this).text('A new career path starts with Universal Technical Institute');
                     }).fadeIn(800);
                     jQuery('#personalInfo').fadeOut(800, function () {
                         jQuery('#aoiWrapper').fadeIn(800);
                     });
                     jQuery('main section:eq(1)').removeClass('light-bg');
                 } else {
                     /// Desktop
                     jQuery('.intro-header').fadeOut(800, function () {
                         jQuery(this).text('A new career path starts with Universal Technical Institute');
                     }).fadeIn(800);
                     jQuery('.lead-in').fadeOut(800, function () {
                         jQuery('.lead-in').text('Take the next step, explore and choose a program by clicking next in a section below.');
                     }).fadeIn(800);
                     jQuery('#personalInfo').fadeOut(800, function () {
                         jQuery('#aoiWrapper').fadeIn(800);
                     });
                     jQuery('main section:eq(1)').addClass('light-bg', 800);
                 }
                 jQuery('.panel-collapse').removeClass('in');
                 jQuery('.accordion-toggle').addClass('collapsed');
                 jQuery('#backBtn').fadeOut(800);
             });
         
             /// Next Buttons Click Event
             jQuery('.button-special[id!="getInfoBtn"]').click(function () {
                 var aoi = jQuery(this).attr('name').replace('Next', '').replace('Btn', '');
         
                 /// Set AoI var for lead attribute in submitForm() formsCommon.js
                 _aoi = areaOfInterestVal(aoi);
         
                 if (jQuery(window).width() < 640) {
                     /// Mobile
                     jQuery('.lead-in').fadeOut(800, function () {
                         jQuery('.lead-in').text('Fill out the form below to receive more information.');
                     }).fadeIn(800);
                     jQuery('.intro-header').fadeOut(800, function () {
                         jQuery(this).text('Find Out More About Our Programs');
                     }).fadeIn(800);
                     jQuery('#aoiWrapper').fadeOut(800, function () {
                         jQuery('#personalInfo').fadeIn(800);
                     });
                     jQuery('.progress-bar-container .wide-num-stripe.two').addClass('blue-progress');
                     jQuery('.progress-bar-container .num-circle.two').addClass('blue-progress');
                 } else {
                     /// Desktop
                     jQuery('.intro-header').fadeOut(800, function () {
                         jQuery('.intro-header').text('Find out more about our programs');
                     }).fadeIn(800);
                     if (aoi == 'Undecided') {
                         jQuery('.lead-in').fadeOut(800, function () {
                             jQuery(this).text('Fill out the form below to have one of our Admissions Representatives contact you about what Universal Technical Institute has to offer.');
                         }).fadeIn(800);
                     } else {
                         jQuery('.lead-in').fadeOut(800, function () {
                             jQuery('.lead-in').text('Fill out the form below to have one of our Admissions Representatives send you information about the ' + aoi + ' program.');
                         }).fadeIn(800);
                     }
                     jQuery('#aoiWrapper').fadeOut(800, function () {
                         jQuery('#personalInfo').fadeIn(800);
                     });
                 }
                 jQuery('#backBtn').fadeIn();
         
                 /// Move screen back up
                 var scrollTo = jQuery(this).attr('name');
                 jQuery('html, body').animate({
                     scrollTop: jQuery('#backBtn').offset().top - 10
                 }, 1000);
         
                 jQuery('#inputFName').focus();
             });
         });
         
      </script><script type="text/javascript">
         //General Inquiry Form Navbar Overrides
         //------------------------------------------------------------
         
         //Removes Request Info Button
         jQuery(".request-info").remove();
         
         
         //------------------------------------------------------------
      </script><script type="text/javascript">
         //Changes Breadcrumb Text from Form 15 to Request Info
         jQuery("ul.breadcrumbs li span").html("Request Info");
      </script> <script>
         $(function () {
             // Don't set callback function in page editor, but still load the API for geolocation
         
             var mapSrc = "https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBwfNOCPCwCdiMVQAl4EUtuLGBFqbv2lDU";
             mapSrc = ($('.map-list').length > 0 && !$('body').hasClass('sfPageEditor')) ? mapSrc + "&callback=initMap" : mapSrc;
             $.getScript(mapSrc);
         })
      </script> 
   </body>
</html>

