﻿$(function () {
  var widgetIds = [];
  var $widgets = $('.widget');

  var regenId = function (id) {
    var newId = id.substr(0, 4);
    var chars = 'abcdefghijklmopqrstuvwxyz0123456789';
    for (var i = 0; i < 6; i++) {
      newId += chars[
        Math.floor(Math.random() * chars.length)
      ];
    }
    return newId;
  };

  var updateIdentifiers = function (el, newId) {
    var $el = el.ref;
    var ariaLabel = $el.attr('aria-labelledby');

    if (typeof ariaLabel === 'undefined')
      return;

    var elementType = ariaLabel.split('-')[1];
    var newLabelId = newId + '-' + elementType;

    $el.attr('aria-labelledby', newLabelId);
    $el.find('.widget-header').attr('id', newLabelId);
  };

  var checkDuplicate = function (id) {
    var appCount = 0;
    widgetIds.forEach(function (_this) {
      if (_this.id === id)
        appCount++;
    });
    return (appCount > 1);
  };

  $widgets.each(function () {
    var _id = $(this).attr('id');
    if (typeof _id !== 'undefined' && _id.length > 0) {
      widgetIds.push({
        'ref': $(this),
        'id': $(this).attr('id')
      });
    }
  });

  widgetIds.forEach(function (_this) {
    if (checkDuplicate(_this.id)) {
      var newId = regenId(_this.id);
      _this.id = newId;
      _this.ref.attr('id', newId);
      updateIdentifiers(_this, newId);
    }
  });
});