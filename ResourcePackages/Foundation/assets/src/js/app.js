if (window.personalizationManager) {
  window.personalizationManager.addPersonalizedContentLoaded(function () {
    $(document).foundation();
  });
} else {
  $(document).foundation();
}

