﻿$(function () {
    if ($('.main-header').length > 0) {
        // Overwriting Foundation's drilldown functionality gets a little weird
        $('.is-drilldown-submenu-parent > a:first-child').unbind();
        $('.is-drilldown-submenu-parent > a:first-child').each(function () {
            $(this).attr('href', $(this).attr('data-href'))
        });

        $('.main-header .off-canvas-main-nav a.active').parents('li').find('> a').addClass('active');

        $('body').on('click', function (e) {
            console.log()
            if ($('.main-header.open').length > 0 &&
                $(e.target).parents('.main-nav-toggle').length < 1 &&
                $(e.target).parents('.off-canvas-main-nav').length < 1) {
                $('.close-nav').click();
            }
        });

        $('body').on('click', '.main-nav-toggle a', function (e) {
            e.preventDefault();

            var $closeNav = $('.main-nav-toggle .close-nav');
            var $openNav = $('.main-nav-toggle .open-nav');
            var ww = $(window).width();

            if ($('.main-header').hasClass('open')) {
                $('.main-header').removeClass('open');
                $('.main-header').addClass('close');

                if (ww <= 640) {
                    $closeNav.addClass('hide');
                    $openNav.removeClass('hide');
                }
            } else {
                $('.main-header').addClass('open');
                $('.main-header').removeClass('close');

                if (!$('body').hasClass('menu-open')) {
                    $('body').addClass('menu-open');
                }

                if (ww <= 640) {
                    $closeNav.removeClass('hide');
                    $openNav.addClass('hide');
                }

                $('.main-header .off-canvas-main-nav .drilldown > li:first-child > a:first-child').focus();

                var $activeLinks = $('.main-header a.active');
                $activeLinks.each(function (inc) {
                    if (inc < ($activeLinks.length - 1)) {
                        $(this).click();
                    }
                });
            }
        });
        $('body').on('click', '.close-nav', function (e) {
            e.preventDefault();

            $('.main-header').removeClass('open');
            $('.main-header').addClass('close');

            if ($('body').hasClass('menu-open')) {
                $('body').removeClass('menu-open');
            }
        });

        var scrollPos = $(window).scrollTop();
        var scrollLimit = 150;

        setHeaderStyle($(window).scrollTop(), scrollPos, false);

        $(window).scroll(function () {
            var curScrollPos = $(this).scrollTop();
            var isAnimated = $("html, body").is(':animated');

            if (curScrollPos < (scrollPos - scrollLimit) || curScrollPos > scrollPos || curScrollPos < 1) {
                setHeaderStyle(curScrollPos, scrollPos, isAnimated);
                scrollPos = curScrollPos;
            }
        });

        function setHeaderStyle(curScrollPos, scrollPos, isAnimated) {
            var $header = $('.main-header');
            var $aLinks = $('.anchor-links');
            var headerHeight = $header.outerHeight();

            if (curScrollPos > 0) {
                if (!$header.hasClass('white')) {
                    $header.addClass('white');
                }
            } else {
                if ($header.hasClass('white')) {
                    $header.removeClass('white');
                }
            }
            if ((curScrollPos < scrollPos || curScrollPos < 1) && (!isAnimated)) {
                //Scrolling Down
                $('.main-header:not(.open)').css({ top: 0 });

                if ($aLinks.length > 0 && $aLinks.css("position") === "fixed" && !$aLinks.hasClass('down')) {
                    $aLinks.css({ transform: "translateY(" + headerHeight + "px)" }).addClass('down');
                }
            } else {
                //Scrolling Up
                $('.main-header:not(.open)').css({ top: -100 });

                if ($aLinks.length > 0) {
                    $aLinks.css({ transform: "translateY(0px)" }).removeClass('down');
                }
            }
            if ($('.sticky-anchor').length > 0 && curScrollPos < $('.sticky-anchor').offset().top) {
                $aLinks.css({ transform: "translateY(0px)" }).removeClass('down');
            }
        }

        /*
         * Search display js
         */
        $('body').on('click', '.nav-search-close', function (e) {
            e.preventDefault();
            $('#nav-search').toggleClass("open");
        });

        $('body').on('click', '.main-nav-search', function (e) {
            e.preventDefault();
            $('#nav-search').toggleClass("open");
            $('#nav-search .search').focus();
        });

        /*
         * Enforce required compliance for older browsers
         */
        $('form[name="nav-search"]').submit(function (e) {
            var $search = $(this).find('input[name="searchQuery"]');
            var _value = $search.val().trim();

            if (_value.length === 0) {
                e.preventDefault();
                e.stopPropagation();
            }
        });
    }

    if($('.header').length>0){
        $('.main_navigation>li').each(function(){
          var nav = $(this);
          if(nav.children('div').length>0 || nav.children('ul').length>0){
             nav.addClass('has_menu');
             $('<span class="down-arrow"><i class="fa fa-chevron-down"></i></span>').appendTo(nav);
          }
        });
        $('.sub_menu ul>li').each(function(){
          var nav = $(this);
          if(nav.children('div').length>0 || nav.children('ul').length>0){
             nav.addClass('has_sub_menu');
             $('<span class="down-arrow"><i class="fa fa-chevron-down"></i></span>').appendTo(nav);
          }
        });
        $(document).on('click', '.main_navigation>li>.down-arrow',function(){
            var $this = $(this);
            var relativeY = ($(this).offset().top - $(this).offsetParent().offsetParent().offsetParent().offset().top);
          if(!$(this).parent().hasClass('active-menu')){
             $(this).parent('li').siblings().children('.sub_menu').slideUp();
             $(this).parent('li').children('.sub_menu').slideToggle("400","swing", function(){
                if($this.offset().top <75){
                    $('.header-bottom-section').animate({
                        scrollTop: $this.offset().top - $this.offsetParent().offsetParent().offsetParent().offset().top
                    }, 400);
                }
             });
             $(this).parent().addClass('active-menu');
             $(this).parent().siblings().removeClass('active-menu');
          } else{
             $(this).parent().removeClass('active-menu');
             $(this).parent('li').children('.sub_menu').slideUp();
          }
        });
        $(document).on('click', '.sub_menu ul>li>.down-arrow',function(){
            var $this = $(this);
            
          if(!$(this).parent().hasClass('active-menu')){
             $('.sub_menu ul>li>ul').slideUp();
             $('.sub_menu ul>li').removeClass('active-menu');
             $(this).parent('li').children('ul').slideToggle("400","swing", function(){
                if($this.offset().top <75){
                    $('.header-bottom-section').animate({
                        scrollTop: $this.offset().top - $this.offsetParent().offsetParent().offsetParent().offsetParent().offsetParent().offset().top
                    }, 400);
                }
             });
             $(this).parent().addClass('active-menu');
          } else{
             $(this).parent().removeClass('active-menu');
             $(this).parent('li').children('ul').slideUp();
          }
        });

        $(document).on('click','.menu-btn', function(){
            $('html').toggleClass('ov_hidden');
            $(this).toggleClass('fa-bars');
            $(this).toggleClass('fa-times');
            $('.header-bottom-section').slideToggle();
              if($(this).text() == 'Close'){
                  $(this).text('Menu');
              } else {
                  $(this).text('Close');
              }
          });
        $('.searchicon').on('click', function (e) {
             e.preventDefault();
             e.stopPropagation();
             $('.searchform').css('display', 'flex');
             $('html').addClass('ov_search_hidden');
          });
          $('.close-form-btn').on('click', function (e) {
              e.stopPropagation();
             $('.searchform').hide();
             $('html').removeClass('ov_search_hidden');
          });

          $(".search-input").click(function(e){
              e.stopPropagation();
          });

          $(document).click(function(){
            if($(window).width()>1023){
              $(".searchform").hide();
            }
            $('html').removeClass('ov_search_hidden');
          });
          // Detect IE version
          var iev=0;
          var st = 0;
          var ieold = (/MSIE (\d+\.\d+);/.test(navigator.userAgent));
          var trident = !!navigator.userAgent.match(/Trident\/7.0/);
          var rv=navigator.userAgent.indexOf("rv:11.0");

          if (ieold) iev=new Number(RegExp.$1);
          if (navigator.appVersion.indexOf("MSIE 10") != -1) iev=10;
          if (trident&&rv!=-1) iev=11;

          // Firefox or IE 11
          if(typeof InstallTrigger !== 'undefined' || iev == 11) {
              var lastScrollTop = 0;
              $(window).on('scroll load', function() {
               
                  st = $(this).scrollTop();
 
                  if(st<10){
                      $('.header').removeClass('scrolled dark-bg');
                      return false;
                     }
                    if(st >($('.hero-content').offset().top - $('.header').height())){
                    $('.header').addClass('dark-bg');
                   }else{
                    $('.header').removeClass('dark-bg');
                   }
                   if(st > lastScrollTop) {
                      $('.header').addClass('scrolled');
                    }
                    if(!$('.header').hasClass('scroll')){
                      if(st > $('.sticky-anchor').offset().top){
                        $('.anchor-links').css('top', $('.header').height()+'px');
                      }else{
                        $('.anchor-links').css('top', '0px');
                      }
                    }else{
                        $('.anchor-links').css('top', '0px');
                    }
                 
                  lastScrollTop = st;
              });
          }
          // Other browsers
          else {
            var lastScrollTop = 0;
              $(window).on('scroll load', function() {
                 
                  st = $(this).scrollTop();

                  if(st<=10){
                    $('.header').removeClass('scrolled dark-bg');
                    return false;
                   }
                   if(st >($('.hero-content').offset().top - $('.header').height())){
                    $('.header').addClass('dark-bg');
                   }else{
                    $('.header').removeClass('dark-bg');
                   }
                    if(st < lastScrollTop) {
                        $('.header').removeClass('scrolled');
                        
                    }
                    else if(st > lastScrollTop) {
                      $('.header').addClass('scrolled');
                    }
                    if(!$('.header').hasClass('scroll')){
                      if(st > $('.sticky-anchor').offset().top){
                        $('.anchor-links').css('top', $('.header').height()+'px');
                      }else{
                        $('.anchor-links').css('top', '0px');
                      }
                    }else{
                        $('.anchor-links').css('top', '0px');
                    }
                  
                  lastScrollTop = st;
              });
          }
          var maxHeight = -1;
          $('.sub_menu').each(function() {
            maxHeight = maxHeight > $(this).outerHeight() ? maxHeight : $(this).outerHeight();
          });
          $('.back-overlay').each(function() {
              $(this).height((maxHeight+13+$('.header-bottom-section').height())+'px'); 
          });
        // var maxHeight = Math.max.apply(null, $('.sub_menu').map(function () {
        //     console.log(this.clientHeight);
        //     return this.clientHeight; // or $(this).height()
        // }));
            
            
            $('.main_navigation>li').on('mouseover', function(e){
                e.preventDefault();
                $(this).closest('.header-bottom-section').addClass('active-overlay');
                $('.back-overlay').each(function() {
                    $(this).height((maxHeight+13+$('.header-bottom-section').height())+'px'); 
                });
                
            }).on('mouseleave', function(){
                $('.header-bottom-section').removeClass('active-overlay');
            });
            // $(window).on('scroll', function(){
            //     if($('.sticky-anchor').offset())
            // })
            //store the element
            var $cache = $('.anchor-links');

            //store the initial position of the element
            var vTop = $cache.offset().top - $('.header').height();
            $(window).scroll(function (event) {
                // what the y position of the scroll is
                var y = $(this).scrollTop();

                // whether that's below the form
                if (y >= vTop) {
                // if so, ad the fixed class
                $cache.css({'position': 'fixed', 'top': $('.header').height() +'px' });
                $('.sticky-anchor').css('height', $('.anchor-links').height()+ 'px');
                } else {
                // otherwise remove it
                $cache.css({'position': 'relative', 'top': '0px' });
                $('.sticky-anchor').css('height', '0px');
                }
            });
    }
});
