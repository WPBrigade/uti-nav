﻿$(function () {
	var widgets = $('.image-grid .image-grid--container');
	var checkImg = function () {
		widgets.each(function () {
			var _this = $(this);
			
			if ($(window).width() < 640) {
				var slideImg = _this.find('.slick-current .image-grid--image-container img');
				var imageHeight = slideImg.height();
				_this.find('.slick-arrow').css({ top: imageHeight/2 })
			}

		});
	}
	var imageGrid = function () {
		widgets.each(function () {
			var _this = $(this);
			if ($(window).width() > 639 && !_this.hasClass('isotope')) {
				if (_this.hasClass('slick-initialized')) {
					_this.slick('unslick');
				}
				if (_this.find('.grid-sizer').length < 1) {
					_this.prepend('<div class="grid-sizer"></div><div class="gutter-sizer"></div>');
				}

				// init Isotope
				_this.addClass('isotope');
				_this.isotope({
					itemSelector: '.image-grid--item',
					percentPosition: true,
					resizable: false, // disable normal resizing
					masonry: {
						columnWidth: '.grid-sizer',
						gutter: '.gutter-sizer'
					}
				});
				// init lightbox
				var lightbox = _this.find('.image-grid--item > a').simpleLightbox({
					nextOnImageClick: false
				}).data('simpleLightbox');

			} else if ($(window).width() < 640) {

				if (_this.hasClass('isotope')) {
					_this.removeClass('isotope');
					_this.isotope('destroy');
				}
				if (_this.find('.grid-sizer').length > 0) {
					_this.find('.grid-sizer').remove()
				}
				if (_this.find('.gutter-sizer').length > 0) {
					_this.find('.gutter-sizer').remove()
				}
				
				if (!_this.hasClass('slick-slider'))  {
					_this.on('init', function (event, slick) {
						var slideImg = $(slick.$slides[0]).find('.image-grid--image-container img');
						var imageHeight = slideImg.height();
						_this.find('.slick-arrow').css({ top: imageHeight/2 })
					});

					_this.slick({
						adaptiveHeight: true,
						infinite: false,
						prevArrow: '<button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Next" role="button"><i class="fa fa-chevron-left"></i></button>',
						nextArrow: '<button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Previous" role="button"><i class="fa fa-chevron-right"></i></button>'
					});

					_this.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
						var slideImg = $(slick.$slides[nextSlide]).find('.image-grid--image-container img');
						var imageHeight = slideImg.height();
						_this.find('.slick-arrow').css({ top: imageHeight/2 })
					});
				}

			}
		});
	}

	$('body').on('click', '.image-grid--item > a', function () {
		if ($(window).width() > 639) {
			setTimeout(function () {
				$('.slbImageWrap:not(.row)').addClass('row column');
				if ($('.slbCloseBtn').length > 0 && !$('.slbCloseBtn').parent().hasClass('row')) {
					$('.slbCloseBtn').wrap("<div class='row column'></div>");
				}
				if ($('.slbArrows').length > 0 && !$('.slbArrows').parent().hasClass('row')) {
					$('.slbArrows').wrapInner("<div class='row column'></div>");
				}
			}, 25);
		}
	});
	$('body').on('click', '.slbArrow', function () {
		if ($(window).width() > 639) {
			setTimeout(function () {
				$('.slbImageWrap:not(.row)').addClass('row column');
			}, 25);
		}
	});

    setTimeout(function () {
        imageGrid();
        checkImg();
    }, 1000);
	$(window).resize(function () {
		imageGrid();
		checkImg();
	});

});
