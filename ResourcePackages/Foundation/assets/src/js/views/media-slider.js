﻿$(function () {

	var	mediaSliderModule = $('.media-slider');

	if (mediaSliderModule.length > 0) {
	  if (typeof (YT) == 'undefined' || typeof (YT.Player) == 'undefined') {
	    $.getScript('//www.youtube.com/iframe_api');

	    window.onYouTubeIframeAPIReady = function () {
	      $('.media-slider .media-slider--video-container').each(function () {
	        var player;
	        var _this = $(this);
	        player = new YT.Player(this, {
	          videoId: _this.attr('data-youtube-id'), // YouTube Video ID
	          width: "100%",               // Player width (in px)
	          height: "100%",              // Player height (in px)
              playerVars: {
                   rel: 0,             // Hides related videos
	            autoplay: 0,        // Auto-play the video on load
	            controls: 1,        // Show pause/play buttons in player
	            showinfo: 0,        // Hide the video title
	            modestbranding: 1,  // Hide the Youtube Logo
	            loop: 0,            // Run the video in a loop
	            fs: 0,              // Hide the full screen button
	            cc_load_policy: 0, // Hide closed captions
	            iv_load_policy: 3,  // Hide the Video Annotations
	            autohide: 0         // Hide video controls when playing
	          }
	        });
	      });

          $('.media-slider--video-container').removeAttr('frameborder');
	    };

	  }

		mediaSliderModule.each(function () {
			var _this = $(this);
			var mediaSlider = _this.find('.media-slider--slides');
			var slideNav = _this.find('.media-slider--slide-nav');

			/* Main media slider */
			mediaSlider.on('init', function (event, slick) {
				var slideImg = ($(slick.$slides[0]).hasClass('media-slider--video-slide')) ? $(slick.$slides[0]).find('.media-wrapper .media-slider--video-container') : $(slick.$slides[0]).find('.media-wrapper img');
				var imageHeight = slideImg.height();
                _this.find('.slick-arrow').css({ top: imageHeight / 2, height: imageHeight });
                $('.media-slider--video-container').removeAttr('frameborder');
			});
            mediaSlider.slick({
                accessibility: false,
				slidesToShow: 1,
				slidesToScroll: 1,
				speed: 0,
				dots: false,
				asNavFor: slideNav,
				adaptiveHeight: true,
				infinite: false,
				prevArrow: '<button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Next Slide"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>',
				nextArrow: '<button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Previous Slide"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>'
			});

			mediaSlider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
				var slideImg = ($(slick.$slides[nextSlide]).hasClass('media-slider--video-slide')) ? $(slick.$slides[nextSlide]).find('.media-wrapper .media-slider--video-container') : $(slick.$slides[nextSlide]).find('.media-wrapper img');
				var imageHeight = slideImg.height();
                mediaSlider.find('.slick-arrow').css({ top: imageHeight / 2, height: imageHeight })
				if ($(slick.$slides[currentSlide]).hasClass('media-slider--video-slide')) {
					var slideVideo = $(slick.$slides[currentSlide]).find('.media-slider--video-container');
					slideVideo[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
				}
            });

            mediaSlider.on('afterChange', function (event, slick, currentSlide) {
                if ($(slick.$slides[currentSlide]).hasClass('media-slider--video-slide')) {
                    $('.media-slider--video-container').removeAttr('frameborder');
                }
            });

			/* Slider thumbnail nav */
            slideNav.slick({
                accessibility: false,
				slidesToShow: 5,
				slidesToScroll: 1,
				dots: false,
				focusOnSelect: true,
				asNavFor: mediaSlider,
				infinite: false,
				prevArrow: '<button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Next Thumbnail Slide"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>',
				nextArrow: '<button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Previous Thumbnail Slide"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>',
				responsive: [
					{
						breakpoint: 1024,
						settings: {
							slidesToShow: 4,
							slidesToScroll: 1
						}
					},
					{
						breakpoint: 900,
						settings: {
							slidesToShow: 3,
							slidesToScroll: 1
						}
					},
					{
    				breakpoint: 640,
    				settings: {
    					slidesToShow: 2,
    					slidesToScroll: 1
    				}
					}
				]
			});
		});

    // Some ADA fixes
		$('.slick-track[role="listbox"]').attr('aria-label', 'Container for slides');
		$('.media-slider--slide,.slide-nav--slide').removeAttr('aria-describedby');
        $('.media-slider--video-container').removeAttr('frameborder');
        $('.media-slider--slide').each(function (i) {
            $(this).attr('aria-label', "Slide " + i);
        });

		$('body').on('click', '.slide-nav--slide', function (e) {
		  e.preventDefault();
		  var _this = $(this);

          /*
		  $('html, body').animate({
		    scrollTop: _this.closest('.media-slider').find('.media-slider--slides').offset().top - 20
		  }, 250);
          */
		});

		$(window).resize(function () {
		  setTimeout(function () {
		    var mediaSlider = $('.media-slider--slides');
		    var slideImg = (mediaSlider.find('.slick-active').hasClass('media-slider--video-slide')) ? mediaSlider.find('.media-wrapper .media-slider--video-container') : mediaSlider.find('.media-wrapper img');
		    var imageHeight = slideImg.height();
		    mediaSlider.find('.slick-arrow').css({ top: imageHeight/2, height: imageHeight })
		  }, 50)
		});
	}
});