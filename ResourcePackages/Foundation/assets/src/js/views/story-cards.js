﻿$(function () {
    $('body').on('click', '.related-story-cards .launch-sc-video', function (e) {
        e.preventDefault();
        var playerID = $(this).data('open');

        setTimeout(function () {
            var $player = $('#' + playerID).find('iframe');
            console.log("Player", $player);
            var url = $player[0].src;
            var arr = url.split('?');
            if (url.length > 0 && arr[1] !== '' && url.indexOf('autoplay=1') < 0) {
                $player[0].src += "&autoplay=1";
            }
        }, 150);
    });

    $('body').on('click', '.filter-cards .launch-sc-video', function (e) {
        e.preventDefault();
        var playerID = $(this).data('open');
        var $player = $('#' + playerID);
        console.log("PLAYER", $player);
        $player.css('display', 'block');
        $player.parent().css('display', 'block');

        setTimeout(function () {
            var $player = $('#' + playerID).find('iframe');
            console.log("Player", $player);
            var url = $player[0].src;
            var arr = url.split('?');
            if (url.length > 0 && arr[1] !== '' && url.indexOf('autoplay=1') < 0) {
                $player[0].src += "&autoplay=1";
            }
        }, 150);
    });

    $('body').on('click', '.reveal-overlay.filter .close-button', function (e) {
        e.preventDefault();
        var $parent = $(this).parent();
        $parent.css('display', 'none');
        $parent.parent().css('display', 'none');
    });
});