﻿if ($('.natural-language-form-container').length > 0) {
    $('body').on('submit', '.natural-language-form-container .form-fields', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var $form = $(this);
        var siteUrl = window.location.origin;
        var $interest = $form.find('select[name="interest-area"]');
        var $studentType = $form.find('select[name="student-type"]');
        var shouldSubmit = ($interest.val().length > 0 || $studentType.val().length > 0);

        if (!shouldSubmit) {
            return false;
        }

        var typeValue = $studentType.val();
        var pageUri = $interest.find(':selected').data('url');
        var typeParam = (typeValue.length > 0) ? ("?g=" + typeValue) : "";
        window.location.href = (siteUrl + pageUri + encodeURI(typeParam));
    });

    $('body').on('change', '.natural-language-form-container select', function (e) {
        var $form = $(this).closest('form');
        var shouldDisable = ($("select[name='interest-area']").val().length === 0 ||
            $("select[name='student-type']").val().length === 0);
        $form.find('button:disabled').prop('disabled', shouldDisable);
	});
}