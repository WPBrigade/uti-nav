﻿$(function () {
  'use strict';

  //$('body').on('change', '.filter-blocks-form select[name="programs"], .filter-blocks-form select[name="brands"]', function () {
  //  var form = $(this).closest('.filter-blocks-form');
  //  var programVal = form.find('select[name="programs"]').val();
  //  var brandVal = form.find('select[name="brands"]').val();

  //  if (brandVal === "" && programVal === "") {
  //    $(this).closest('.filter-blocks-form').find('button[type="submit"]').prop('disabled', true);
  //  } else {
  //    $(this).closest('.filter-blocks-form').find('button[type="submit"]').prop('disabled', false);
  //  }
  //});

  $('body').on('submit', '.filter-blocks-form', function (e) {
    e.preventDefault();

    var program = $(this).find('select[name="programs"]');
    var brand = $(this).find('select[name="brands"]');
    var manufacturers = $(this).find('input[name="hideManufacturers"]');
    var brandVal = brand.val().toLowerCase();
    var programVal = program.val().toLowerCase();
    var hideManufacturers = manufacturers.val(); 

    var resultsContainer = $(this).closest('.filter-blocks').find('.filter-blocks--results');
    resultsContainer.html('<div style="width: 100%; padding:2rem 0; text-align:center;">Loading...</div>');
    getFilteredResults(brandVal, programVal, hideManufacturers, resultsContainer);
  });

  var getFilteredResults = function (brand, program, hideManufacturers, resultsContainer) {

    $.get(
      '/api/locations/get',
      {
        "program": program,
        "brand": brand,
        "hideManufacturers": hideManufacturers
      },
      function (resp) {
        var items = JSON.parse(resp);
        var templateItems = "";

        if (items.length <= 0) {
          resultsContainer.html('<div style="width: 100%; padding:2rem 0; text-align:center;">No Results</div>');
          return false;
        }

        for (var item in items) {
          if (!items.hasOwnProperty(item)) { continue; }

          var title = items[item].title;
          var description = items[item].description;
          var imageUrl = items[item].imageUrl;
          var url = items[item].locationUrl;

          templateItems += '<a href="' + url + '" style="background-image:url( \'' + imageUrl + '\' )" class="column column-block background-image" aria-label="Link to ' + title + '">' +
                            '<div class="info-overlay">' +
                              '<div class="info-content">' +
                                '<h3>' + title + '</h3>' +
                                '<p>' + description + '</p>' +
                              '</div>' +
                            '</div>' +
                          '</a>';
        }

        resultsContainer.html(templateItems);

      }
    );
  }
});