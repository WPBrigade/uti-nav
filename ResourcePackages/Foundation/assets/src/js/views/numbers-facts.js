﻿$(function () {
    var $statRows = $('.numbers-facts-stats.row');

    function unsetStatHeights() {
        $statRows.each(function () {
            $(this).find('.column').attr('style', '');
        });
    }

    function adjustStatHeights() {
        unsetStatHeights();
        var width = $(window).width();

        $statRows.each(function () {
            var max = 0;
            var $c = $(this).find('.column');
            $c.each(function () {
                if ($(this).height() > max) {
                    max = $(this).height();
                }
            });
            $c.height(max);
        });
    }

    $(window).resize(adjustStatHeights);
    $(document).ready(adjustStatHeights);
    /*
    var showMoreClicked = false;
    var $factsInner = $('.numbers-facts-inner');
    var $numbersFacts = $('.widget.numbers-facts');

    var check = function () {
        $numbersFacts.each(function () {
            var widget = $(this);
            var facts = widget.find('.numbers-facts-stats > .column');

            if ($(window).width() <= 640 && !showMoreClicked) {
                if ($factsInner.find('.show-more').length !== 0) {
                    return;
                }
                if (facts.length > 4) {
                    for (var i = 4; i < facts.length; i++) {
                        facts[i].className += " hidden";
                    }
                    widget.find('.numbers-facts-stats').parent().parent().after('<a class="show-more" href="#" aria-label="Show hidden stats">Show Me More <i class="fa fa-chevron-down" aria-hidden="true"></i></a>');
                }
            }
        });
    }

    $('body').on('click', '.widget.numbers-facts .show-more', function (e) {
        e.preventDefault();

        $_this = $(this);
        $_this.parent().find('.numbers-facts-stats > .column').removeClass('hidden');
        $_this.remove();

        showMoreClicked = true;
    });

    $(window).resize(function () {
        check();
    });

    check();
    */
});
