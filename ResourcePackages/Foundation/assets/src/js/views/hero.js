﻿$(function () {
    $('body').on('click', '.scroll-to-next', function () {
        var ele = $(this).closest(".widget").next();
        if (ele.length > 0) {
            $("html, body").animate({
                scrollTop: $(ele).offset().top
            }, 250);
        } else {
            $("html, body").animate({
                scrollTop: $(this).closest(".container").next().top
            }, 250);
        }

        return false;
    });
    $('body').on('click', '.launch-video', function (e) {
        e.preventDefault();
        var playerID = $(this).data('open');

        setTimeout(function () {
            var $player = $('#' + playerID).find('iframe');

            var url = $player[0].src;
            var arr = url.split('?');
            if (url.length > 0 && arr[1] !== '' && url.indexOf('autoplay=1') < 0) {
                $player[0].src += "&autoplay=1";
            }
        }, 150);
    });
    $('body').on('click', '.video-modal .close-button, .reveal-overlay', function (e) {
        var $video = ($(this).find('.video-modal').length > 0) ? $(this).find('.video-modal iframe') : $(this).closest('.video-modal').find('iframe');
        var url = $video.attr('src');

        $video.attr('src', '');
        $video.attr('src', url.replace(/([&\?]autoplay=1*$|autoplay=1&|[?&]autoplay=1(?=#))/, ''));

        $video.closest('.reveal').foundation('close');

        // $video.contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
        return false;
    });
    playerDiv = $('.video-background');
    if (playerDiv.length > 0) {
        var ismobile = (/iphone|ipod|android|blackberry|opera|mini|windows\sce|palm|smartphone|iemobile/i.test(navigator.userAgent.toLowerCase()));
        if (!ismobile && $(window).width() >= 768) {
            playerDiv.each(function () {
                var pl = $(this);
                var vID = pl.data('youtube-id');

                if (!pl.find('.video-iframe.loaded').length) {
                    pl.find('.video-iframe').YTPlayer({
                        fitToBackground: false,
                        videoId: vID,
                        controls: 0,
                        loop: 1,
                        modestbranding: 1,
                        rel: 0,
                        showinfo: 0,
                        callback: function () {
                            $('.video-background, .video-background iframe, .video-background .ytp-watermark.yt-uix-sessionlink').attr('tabindex', "-1");
                        }
                    });
                }
            });
        }
    }

    var isInternetExplorer = navigator.userAgent.match(/(Trident\/|MSIE )/);
    if (isInternetExplorer) {
        $('.widget.hero').css('background-size', 'cover');
    }
});
