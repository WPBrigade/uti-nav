﻿$(function () {
    $('.expander span').click(function (e) {
        var $disclaimer = $('.main-footer--disclaimer');

        if ($disclaimer.length === 0) {
            return false;
        }

        var inner = $disclaimer.hasClass('closed') ? '-' : '+';

        $(this).html(inner);
        $disclaimer.toggleClass('closed open');
    });

    $(document).ready(function () {
        $('.main-footer').find('img').each(function () {
            var data = $(this).data('src');
            if (typeof data !== 'undefined') {
                $(this).attr('src', data);
            }
        });
    });
});