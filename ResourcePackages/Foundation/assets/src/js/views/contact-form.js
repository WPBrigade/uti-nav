﻿$(function () {

    'use strict';

    var $diploma = $('#diploma');
    var $military = $('#military');
    var $form = $('form[name="uti-contact"]');
    var $ageInput = $('select[name="age"]');
    var $emailInput = $('#emailAddressInput');
    var $phoneInput = $('#phoneNumberInput');
    var $firstNameInput = $('#firstNameInput');
    var $lastNameInput = $('#lastNameInput');
    var $zipInput = $('#zipCodeInput');
    var $textFields = $form.find('input[type="text"]')
    var $selectFields = $form.find('select');

    $ageInput.change(function (event) {
        var selectedAge = parseInt($(this).val());

        if (selectedAge >= 18) {
            $military.attr('style', 'display:block');
        } else {
            $military.attr('style', 'display:none');
        }

        if (selectedAge >= 14 && selectedAge <= 19) {
            $diploma.attr('style', 'display:block');
        } else {
            $diploma.attr('style', 'display:none');
        }
    });

    $textFields.keyup(function (event) {
        if ($(this).val().length > 0) {
            $(this).prev().attr('style', 'visibility:visible;opacity:1');
        } else {
            $(this).prev().attr('style', '');
        }
    });

    $textFields.keypress(function (event) {
        $(this).removeClass('valid-error');
        $(this).parent().find('.error').remove();
    });

    $selectFields.change(function (event) {
        $(this).removeClass('valid-error');
        $(this).parent().find('.error').remove();
    });

    var addValidError = function ($element, message) {
        $element.addClass('valid-error');
        if ($element.parent().find('.error').length === 0) {
            $("<label class='error'>" + message + "</label>").insertAfter($element);
        }
    }

    $form.submit(function (event) {
        event.preventDefault();
        var nullFields = 0;

        var requiredFields = [
            $ageInput, $zipInput, $lastNameInput,
            $('#countryInput'), $firstNameInput, $('#program'),
            $emailInput, $phoneInput, $ageInput
        ];

        for (var r = 0; r < requiredFields.length; r++) {
            var $field = requiredFields[r];
            if ($field.val().trim().length === 0) {
                nullFields++;
                $field.addClass('valid-error');
                if ($field.parent().find('.error').length === 0) {
                    $("<label class='error'>This field is required.</label>").insertAfter($field);
                }
            }
        }

        if (!$emailInput.val().match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
            addValidError($emailInput, "Invalid email address.");
            nullFields++;
        }

        if (!$firstNameInput.val().match(/^[a-zA-Z -]+$/)) {
            addValidError($firstNameInput, "This field does not accept numerical values or special characters.");
            nullFields++;
        }

        if (!$lastNameInput.val().match(/^[a-zA-Z -]+$/)) {
            addValidError($lastNameInput, "This field does not accept numerical values or special characters.");
            nullFields++;
        }

        if ($zipInput.val().length < 5 && $zipInput.val().match(/^[0-9 -]+$/)) {
            addValidError($zipInput, "Zip code must consist of at least 5 numbers.");
            nullFields++;
        } else if (!$zipInput.val().match(/^\d{5}(?:[-\s]\d{4})?$/)) {
            addValidError($zipInput, "This field only accepts numerical values.");
            nullFields++;
        }

        if (!$phoneInput.val().match(/^[0-9 -]+$/)) {
            addValidError($phoneInput, "This field only accepts numerical values.");
            nullFields++;
        } 

        if (nullFields > 0) {
            return false;
        }

        var postUrl = $form.find('input[type="hidden"]').val();

        var postData = {
            program: $('#program').val(),
            ageInput: $ageInput.val(),
            emailAddressInput: $emailInput.val(),
            countryInput: $('#countryInput').val(),
            zipCodeInput: $zipInput.val(),
            lastNameInput: $('#lastNameInput').val(),
            firstNameInput: $('#firstNameInput').val(),
            diplomaQuestionInput: $('#education').val(),
            phoneNumberInput: $phoneInput.val(),
            militaryQuestionInput: $('input[name="militaryQuestionInput"]:checked').val()
        };

        $.ajax({
            type: "POST",
            url: postUrl,
            data: {
                model: postData
            },
            dataType: "json",
            success: function (res) {
                $form.html('<h3 class="text-center">Submission success!</h3>');
            },
            error: function (res) {
                alert('Something went wrong.');
            }
        });
    });
});