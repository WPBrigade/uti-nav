﻿$(function () {

    var $imageRows = $('.image-icon.row');

    function unsetParagraphHeights() {
        $imageRows.each(function () {
            $(this).find('p').attr('style', '');
        });
    }

    function adjustParagraphHeights() {
        var width = $(window).width();

        unsetParagraphHeights();

        if (width <= 639)
            return false;

        $imageRows.each(function () {
            var max = 0;
            var $p = $(this).find('p');
            $p.each(function () {
                if ($(this).height() > max) {
                    max = $(this).height();
                }
            });
            $p.height(max);
        });
    }

    $(window).resize(adjustParagraphHeights);
    $(document).ready(adjustParagraphHeights);

});