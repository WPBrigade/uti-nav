﻿$(function () {
    /*
	$('.widget.fw-blocks-container').each(function () {
		var widget = $(this);
		var blocksFive = widget.find('.row.large-up-5 > .column-block');
		var blocksFour = widget.find('.row.large-up-4 > .column-block');

		if (blocksFive.length > 5) {
			blocksFive.parent().after('<div class="show-more-container"><a class="show-more" href="#">Show Me More <i class="fa fa-chevron-down"></i></a></div>');
		}
		if (blocksFour.length > 8) {
		    blocksFour.parent().after('<div class="show-more-container"><a class="show-more" href="#">Show Me More <i class="fa fa-chevron-down"></i></a></div>');
		}
	});
    */

    $('.fw-blocks-container').each(function () {
        var $inner = $(this).find('.align-center-middle');
        var count = $inner.find('.column-block').length;

        if (typeof $inner.attr('class') === 'undefined') {
          return;
        }

        if ($inner.attr('class').indexOf('large-up') > -1) {
            var colNo = $inner.attr('class').split('large-up-')[1].split(' ')[0];

            for (var i = 0; i < (count % colNo); i++) {
                $inner.append('<div class="column column-block background-image" style="display:none"></div>')
            }
        }
    });

	$('body').on('click', '.widget.fw-blocks-container .show-more', function (e) {
		e.preventDefault();

		var blockList = $(this).parent().prev();
		var button = $(this).parent();

		if (!blockList.hasClass('show-blocks')) {
			blockList.addClass('show-blocks');
			button.remove();
		}
    });

    $(document).ready(function () {
        var $colBlocks = $('.widget.fw-blocks-container').find('.column-block');
        $colBlocks.each(function () {
            var data = $(this).data('src');
            if (typeof data !== 'undefined') {
                if (data.length > 0) {
                    $(this).css('background-image', ('url(' + data + ')'));
                }
            }
        });
    });
});
