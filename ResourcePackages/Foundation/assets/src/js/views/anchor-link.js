﻿$(function () {
    $('body').on('click', '.anchor-links .is-accordion-submenu-item a:not(.button-special)', function (e) {
        var _this = $(this);
        e.preventDefault();

        if (_this.attr('href').indexOf('/') > -1) {
            window.location.href = _this.attr('href');
            return false;
        }

        _this.closest('.menu.accordion-menu').foundation('hideAll');
        setTimeout(function () {
            var height = _this.closest('.is-accordion-submenu-item').height();
            var ele = $(_this.attr('href'));
            $("html, body").animate({
                scrollTop: $(ele).offset().top - height
            }, 250);
        }, 250);
    });

    if ($('.anchor-links').length > 0) {
        moveScroller();
    }

    $('#anchorTitle').click(function () {
        var width = $(window).width();
        if (width <= 639) {
            $('.anchor-links').toggleClass("expanded");
        }
    });
});

function moveScroller() {
	var anchor = $(".sticky-anchor");
	var scroller = $('.anchor-links');
	var sh = scroller.outerHeight();
	var sections = $('.widget');

	var move = function () {
		var st = $(window).scrollTop();
		var ot = anchor.offset().top;
		if (st > ot) {
			scroller.css({
				position: "fixed",
				top: "0",
				left: "0",
				right: "0"
			});
			anchor.css({
				height: scroller.outerHeight()
			});

			sections.each(function () {
				var top = $(this).offset().top - sh,
						bottom = top + $(this).outerHeight();

				if (st >= top && st <= bottom) {
					scroller.find('a').removeClass('active');
					sections.removeClass('active');
					$(this).addClass('active');
                    var _this = $(this);
                    scroller.find('a:not(.button-special)[href="#' + _this.attr('id') + '"]').addClass('active');
                    var activeMobileLink = scroller.find('.accordion-menu a:not(.button-special)[href="#' + _this.attr('id') + '"]');
                    var activeMobileHeader = activeMobileLink.length > 0 ? activeMobileLink.text() : "Overview";
					$('#anchorTitle').text(activeMobileHeader);
				}
			});
		} else {
			scroller.css({
				position: "relative",
				top: "",
				left: "",
				right: ""
			});
			anchor.css({
				height: ""
			});
		}
	};
	$(window).scroll(move);
	move();
}