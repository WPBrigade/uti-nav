﻿$(function () {
  var $pag = $('.search-results .result-list + .pagination');
  if ($pag.length > 0) {
    var searchUrl = parseURL(window.location.href);
    var pageNum = searchUrl.pageNum;
    var curPageNum = parseURL($('.pagination li.active a').attr('href')).pageNum;
    var prevPageNum = (pageNum > 2) ? "/" + (pageNum - 1) : "";
    var nextPageNum = (pageNum === 0) ? "/2" : "/" + (pageNum + 1);
    var lastPageNum = parseURL($('.pagination li:nth-last-child(1) a').attr('href')).pageNum;

    var prevDisabled = (curPageNum === 0 || curPageNum === 1) ? "disabled" : "";
    var nextDisabled = (curPageNum === lastPageNum) ? "disabled" : "";

    var prevUrl = '/search' + prevPageNum + searchUrl.search;
    var nextUrl = '/search' + nextPageNum + searchUrl.search;

    $pag.prepend('<li class="pagination-arrow prev-arrow"><a href="' + prevUrl + '" ' + prevDisabled + '><i class="fa fa-chevron-left" aria-hidden="true"></i><span class="show-for-sr">Previous Page</span></a></li>');
    $pag.append('<li class="pagination-arrow next-arrow"><a href="' + nextUrl + '" ' + nextDisabled + '><i class="fa fa-chevron-right" aria-hidden="true"></i><span class="show-for-sr">Next Page</span></a></li>');

    $('.prev-arrow a').click(function (e) {
        if (prevDisabled) {
            e.preventDefault();
            e.stopPropagation();
        }
    });

    $('.next-arrow a').click(function (e) {
        if (nextDisabled) {
            e.preventDefault();
            e.stopPropagation();
        }
    });

    function parseURL(url) {
      var parser = document.createElement('a'),
          searchObject = {},
          queries, split, i;
      // Let the browser do the work
      parser.href = url;
      // Convert query string to object
      queries = parser.search.replace(/^\?/, '').split('&');
      for (i = 0; i < queries.length; i++) {
        split = queries[i].split('=');
        searchObject[split[0]] = split[1];
      }
      return {
        protocol: parser.protocol,
        host: parser.host,
        hostname: parser.hostname,
        port: parser.port,
        pathname: parser.pathname,
        pageNum: (parseInt(parser.pathname.split('/').pop())) ? Number(parser.pathname.split('/').pop()) : 0,
        search: parser.search,
        searchObject: searchObject,
        hash: parser.hash
      };
    }
  }
});