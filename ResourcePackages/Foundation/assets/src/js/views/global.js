﻿$(function () {
    $('sup').click(function (e) {
        var targetId = $(this).data('anchor-id');

        if (typeof targetId === 'undefined') {
            return false;
        }

        var isFooter = (targetId === "footer-disclaimer");
        var $target = isFooter ? $('.main-footer--disclaimer') : $('#' + targetId);
        var targetOffset = isFooter ? ($target.offset().top - 50) : $target.offset().top;

        if ($target.length === 0) {
            return false;
        }

        $("html, body").animate({
            scrollTop: targetOffset
        }, 250);
    });

    /* Code for offsite links to open in a new tab */
    $.expr[':'].external = function (obj) {
        return !obj.href.match(/^mailto\:/)
            && (obj.hostname != location.hostname)
            && !obj.href.match(/^javascript\:/)
            && !obj.href.match(/^$/)
    };

    $('a:external').attr('target', '_blank');

    for (var links = document.links, i = 0, a; a = links[i]; i++) {
        if (a.host !== location.host) {
            a.target = '_blank';
        }
    }

    var links = document.links;
    for (var i = 0; i < links.length; i++) {
        if (links[i].href.match(/^tel\:/)) {
            links[i].removeAttribute('target');
        }
    }

    $('img[id^="batBeacon"]').attr('alt', 'marketing beacon');
});