$(function() {
    'use strict';

    if ($('.content-slider').length) {
        $('.content-slider .slides').each(function() {
        	var _this = $(this);
        	_this.on('init', function (event, slick) {
        			$(window).resize();
        		});

        	_this.slick({
        	    accessibility: false,
            	dots: false,
            	appendArrows: $(this).closest('.content-slider').find('.pagination-container'),
            	prevArrow: '<button type="button" class="slider-prev"><i class="fa fa-chevron-left"></i></button>',
            	nextArrow: '<button type="button" class="slider-next"><i class="fa fa-chevron-right"></i></button>',
              adaptiveHeight: true,
              lazyLoad: 'progressive',
              slidesToShow: 1,
							speed: 0
          });
        	// before slide change update the slide number
        	_this.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
            var slideNum = nextSlide + 1;
            _this.closest('.content-slider').find('.current-slide-no').text(slideNum);
            checkPagination();
          });
        	_this.on('afterChange', function (event, slick, currentSlide) {
        		checkPagination();
          });
        	var label = _this.attr('aria-label');
            _this.find('.slick-track').attr('aria-label', label);
            _this.attr('aria-label', '').removeAttr('aria-label');
            _this.find('.slick-slide, .slick-dots li').removeAttr('role');
        });
				
        function checkPagination() {
        	var ww = $(window).width();
        	if (ww < 1024) {
        		$('.content-slider').each(function () {
        			var imgHeight = $(this).find('.slick-active .slide-media').height();
        			$(this).find('.pagination-wrapper').css({ 'top': imgHeight });
        		});
        	}
        }

        checkPagination();

        var resizeTimer;
        $(window).on('resize', function (e) {
        	clearTimeout(resizeTimer);
        	resizeTimer = setTimeout(checkPagination(), 200);
        });
    }
});