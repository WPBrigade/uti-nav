﻿$(function () {
    'use strict';

    if ($('.filter-cards').length > 0) {
    	$('.filter-cards').each(function () {
            var $_this = $(this);
    		var $storyType = $_this.find('select[name="story-type"]');
    		var $program = $_this.find('select[name="program"]');
    		var $brand = $_this.find('select[name="brand"]');
    		var $location = $_this.find('select[name="location"]');
    		var $filterForm = $_this.find('.filter-cards--form');
    		var $filterResults = $_this.find('.filter-cards--results');

    		$filterForm.on('submit', function (e) {
    			e.preventDefault();
    			var brandVal = $brand.val().toLowerCase();
    			var programVal = $program.val().toLowerCase();
    			var locationVal = $location.val().toLowerCase();
    			var storyTypeVal = $storyType.val().toLowerCase();
    			getFilteredCardResults(brandVal, programVal, locationVal, storyTypeVal, $filterResults);
    		});
    	});

        var rebindCloseEvents = function () {
          $('.reveal-overlay.filter .close-button').click(function (e) {
            e.preventDefault();
            e.stopPropagation();

            var $parent = $(this).parent();
            var $video = $parent.find('iframe');
            var url = $video[0].src;
            var urlString = url.split('?')[0];
            var newUrl = urlString + '?color=null&modestbranding=1&rel=0';

            $video.attr('src', newUrl);
            $parent.css('display', 'none');
            $parent.parent().css('display', 'none');
          });
        };

        var getStoryTemplate = function (card, cards) {
            var type = '', templateItem = '';
            var name = cards[card].name;
            var quote = cards[card].quote;
            var imageUrl = cards[card].imageUrl;
            var url = cards[card].locationUrl;
            var videoId = cards[card].videoId;
            var showContent = cards[card].showContent;
            var subHeadingOne = cards[card].subHeadingOne;
            var subHeadingTwo = cards[card].subHeadingTwo;

            if (showContent) {
                var image = (typeof imageUrl !== "undefined" && imageUrl !== null && imageUrl.length > 0) ? '<figure><img src=' + imageUrl + '" alt="see more about ' + name + '" /></figure>' : '';
                templateItem = '<article class="column ' + type + '" aria-label="' + type + ' story from ' + name + '">' +
                    '<div class="filter-card">' +
                    image +
                    '<div class="filter-card--content">' +
                    '<h3>' + name + '</h3>' +
                    '<p>"' + quote + '"</p>' +
                    '<a href="' + url + '" class="button">Call to Action</a>' +
                    '</div>' +
                    '</div>' +
                    '</article>';
            } else {
                var videoMarkup = '';

                if (videoId.trim().length > 0) {
                    videoMarkup += '<div class="sc-video-thumb" style="background-image:url(https://img.youtube.com/vi/' + videoId + '/0.jpg)">' +
                        '<a href="#" aria-haspopup="true" class="launch-sc-video" data-open="story-video-' + videoId + '" aria-controls="story-video-' + videoId + '">' +
                        '<span class="show-for-sr">Play the video</span>' +
                        '<i class="fa fa-play" aria-hidden="true"></i>' +
                        '</a>' +
                        '</div>';
                    var bottomContainer = '<div class="reveal-overlay filter" style="display:none"><div style="display: none; top: 254px" class="large reveal video-modal" id="story-video-' + videoId + '" data-reveal data-reset-on-close="true" role="dialog" aria-hidden="true" data-yeti-box="story-video-' + videoId + '" data-resize="story-video-' + videoId + '" tab-index="-1">' +
                        '<div class="flex-video widescreen">' +
                        '<iframe width="420" height="315" src="//www.youtube-nocookie.com/embed/' + videoId + '?color=null&modestbranding=1&rel=0" title="youtube video for youtube ID ' + videoId + '" frameborder="0" allowfullscreen></iframe>' +
                        '</div>' +
                        '<button class="close-button" data-close aria-label="Close reveal" type="button">' +
                        '<span aria-hidden="true">&times;</span>' +
                        '</button>' +
                        '</div></div>';
                    $('#PublicWrapper').after(bottomContainer);
                    rebindCloseEvents();
                }

                var shOne = (subHeadingOne.trim().length > 0) ? ('<p>' + subHeadingOne + '</p>') : '';
                var shTwo = (subHeadingTwo.trim().length > 0) ? ('<p>' + subHeadingTwo + '</p>') : '';

                templateItem = '<article class="column ' + type + '" aria-label="' + type + ' story from ' + name + '">' +
                    '<div class="filter-card">' +
                    videoMarkup +
                    '<div class="filter-card--content">' +
                    '<h3>' + name + '</h3>' +
                    shOne + 
                    shTwo +
                    '</div>' +
                    '</div>' +
                    '</article>'; 
            }

            return templateItem;
        };

    	var getFilteredCardResults = function (brand, program, location, storyType, $filterResults) {
    		$.get(
						'/api/stories/get',
						{
							"program": program,
							"brand": brand,
							"location": location,
							"storyType": storyType
						},
						function (resp) {
							showNewCards(JSON.parse(resp), $filterResults);
						}
				);
    	}
    	var showNewCards = function (cards, $filterResults) {
    		var templateItems = "";

    		$filterResults.html('<div style="width: 100%; padding: 2rem; text-align: center;">Loading...</div>');
    		$filterResults.removeClass('hide');
    		removeButton($filterResults);

    		if (cards.length > 0) {
    			for (var card in cards) {
    				if (!cards.hasOwnProperty(card)) { continue; }
                    templateItems += getStoryTemplate(card, cards);
    			}

    			$filterResults.html(templateItems);

    			if (cards.length > 6) {
    				showButton($filterResults);
    			}
    		} else {
    			$filterResults.html('<div style="width: 100%; padding: 2rem; text-align: center;">No Results</div>');
    		}
    	}

    	var showButton = function ($filterResults) {
    		var template = '<a class="show-more-button">Load More Stories <i class="fa fa-chevron-down"></i></a>';
    		$filterResults.after(template)
    	}

    	var removeButton = function ($filterResults) {
    		$filterResults.parent().find('.show-more-button').remove();
    	}
    }


    $('body').on('click', '.filter-cards .show-more-button', function (e) {
    	e.preventDefault();
    	$(this).closest('.filter-cards').find('.filter-cards--results').removeClass('hide-stories');
    	$(this).remove();
    });
});
