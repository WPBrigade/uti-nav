﻿// To include distance, use https://developers.google.com/maps/documentation/distance-matrix/start
var initMap = function () {
    if ($('.map-list').length > 0) {
        var mapLists = document.getElementsByClassName("map-list");
        for (var i = 0; i < mapLists.length; i++) {
            var delay = 100;
            var _this = mapLists[i];
            var map = _this.getElementsByClassName('map-list--map');
            var markers = [];
            var locations = JSON.parse(map[0].getAttribute('data-locations'));
            var listItems = _this.getElementsByClassName('map-list--list--li');

            // var latlng = new google.maps.LatLng(33.448376, -112.074036);

            var infowindow = new google.maps.InfoWindow();
            var mapOptions = {
                zoom: 5,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                fullscreenControl: false,
                streetViewControl: false
            };

            var geocoder = new google.maps.Geocoder();
            var map = new google.maps.Map(map[0], mapOptions);
            var bounds = new google.maps.LatLngBounds();

            function geocodeAddress(address, next) {
                geocoder.geocode({ address: address }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var currentLocIndex = locations.indexOf(address);
                        var p = results[0].geometry.location;
                        var lat = p.lat();
                        var lng = p.lng();
                        var desc = $(listItems).eq(currentLocIndex).find('.map-list--list--link').attr('data-description');
                        var title = $(listItems).eq(currentLocIndex).find('.map-list--list--link').attr('data-title');
                        createMarker(address, lat, lng, desc, title, currentLocIndex);
                    } else {
                        if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                            nextAddress--;
                            delay++;
                        } else {
                        }
                    }
                    next();
                });
            }
            function createMarker(address, lat, lng, desc, title, index) {
                var markerNum = index + 1;
                var urlAdd = title.split(' ').join('+') + "+" + address.split(' ').join('+');
                var contentString = '<div class="mapContent">' +
                                        // '<span>Distance from Campus</span>' +
                                        '<h3>' + title + '</h3>' +
                                        '<div class="bodyContent">' +
                                            '<p>' + desc + '</p>' +
                                            '<a href="https://maps.google.com?q=' + urlAdd + '" target="_blank">Open in Google Maps</a>' +
                                        '</div>' +
                                    '</div>';
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(lat, lng),
                    icon: "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=" + markerNum + "|1779BA|FFFFFF",
                    map: map,
                });

                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.setContent(contentString);
                    infowindow.open(map, marker);
                });

                google.maps.event.addListener(infowindow, 'domready', function () {

                  // Reference to the DIV which receives the contents of the infowindow using jQuery
                  var iwOuter = $('.gm-style-iw');

                  /* The DIV we want to change is above the .gm-style-iw DIV.
                   * So, we use jQuery and create a iwBackground variable,
                   * and took advantage of the existing reference to .gm-style-iw for the previous DIV with .prev().
                   */
                  var iwBackground = iwOuter.prev();

                  // Remove the background shadow DIV
                  iwBackground.children(':nth-child(2)').css({ 'display': 'none' });

                  // Remove the white background DIV
                  iwBackground.children(':nth-child(4)').css({ 'display': 'none' });

                  iwBackground.children(':nth-child(1)').attr('style', function (i, s) { return s + 'display: none !important;' });
                  iwBackground.children(':nth-child(3)').attr('style', function (i, s) { return s + 'margin-top: -5px !important; box-shadow: none !important; background: rgba(0,56,82,.9) !important;' });
                  iwBackground.children(':nth-child(3)').children().attr('style', function (i, s) { return s + 'display: none !important;' });
                });

                bounds.extend(marker.position);
                markers.push(marker);
            }

            //var locations = [
            //    'Phoenix, AZ',
            //    'Scottsdale, AZ',
            //    'Chandler, AZ'
            //];

            // Bypass the geolocation limitations by setting a timeout between locations
            var nextAddress = -1;
            function theNext() {
                if (nextAddress < locations.length) {
                    setTimeout(function () {
                        geocodeAddress(locations[nextAddress], theNext);
                    }, delay);
                    nextAddress++;
                } else {
                    map.fitBounds(bounds);
                }
            }

            theNext();

        }

        $('body').on('click', '.map-list--list--link', function (e) {
            e.preventDefault();

            var itemIndex = $(this).closest('.map-list--list--li').index();
            google.maps.event.trigger(markers[itemIndex], 'click');
        });
    }
}

// Reload
var resizeGMap = function () {
    setTimeout(function () {
        initMap();
    }, 250);
}

$(function () {
    // reload on window resize
    var resizeTimer;
    $(window).on('resize', function (e) {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function () {
            resizeGMap();
        }, 250);
    });
});
